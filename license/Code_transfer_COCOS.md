Authorisation to use and apply:

**COCOS**

at the _______________________________________________ (insert association name)

___

The COCOS routines are developed at the Swiss Plasma Center, Ecole Polytechnique Fédérale de Lausanne (SPC/EPFL), Switzerland. COCOS provides functionalities to obtain and check the relevant parameters and transformations related to the coordinate conventions used. It interfaces with the IDS data model.

The undersigned has received a copy of COCOS, including its source code, under
the additional conditions that:

1. The code does not change its name even if modified.

2. The undersigned agrees with the contributor license agreement (CLA), see
   [CLA_individual_COCOS.md](CLA_individual_COCOS.md)/[CLA_organization_COCOS.md](CLA_organization_COCOS.md) (v2.2) for details,
   also in the git repository license subfolder. Modifications of the codes
   that are developed and made available to the SPC are granting the right to
   EPFL to make these modifications available on an “as is” basis to all other
   holders of an agreed usage of COCOS.

3. Usage of these routines and of the COCOS coordinate conventions should
   appropriately reference the original publication:

     - [O. Sauter and S. Y Medvedev, "Tokamak Coordinate Conventions: COCOS", Comput. Phys. Commun. 184 (2013) 293](https://crppwww.epfl.ch/~sauter/cocos/Sauter_COCOS_Tokamak_Coordinate_Conventions.pdf)

4. COCOS can be used by the partners of the above association on the
   association related clusters under the above conditions. The partners can
   obtain the sources to be installed at their home institutes by just signing
   this code transfer document ([printable pdf version](https://crppwww.epfl.ch/~sauter/COCOS/Code_transfer_COCOS.pdf)).



5. COCOS nor their progeny may be transferred or made available to other
   research groups without the written authorisation from the SPC.

Responsible person

- Name: __________________________________

- email: __________________________________

- Place and Date: __________________________________

- Signature: __________________________________


___

SPC-EPFL<br/>
Station 13<br/>
1015 Lausanne (Switzerland)

http://spc.epfl.ch/COCOS
https://gitlab.epfl.ch/spc/cocos
