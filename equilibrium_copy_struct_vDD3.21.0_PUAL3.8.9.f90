 
module equilibr331_copy_struct



 
use utilitie115_copy_struct


interface ids_copy
 
   module procedure ids_copy_struct_equilibr3272 
   module procedure ids_copy_struct_equilibr1593 
   module procedure ids_copy_struct_equilibr2480 
   module procedure ids_copy_struct_equilibr1294 
   module procedure ids_copy_struct_equilibr3708 
   module procedure ids_copy_struct_equilibr2773 
   module procedure ids_copy_struct_equlibri2136 
   module procedure ids_copy_struct_equilibr3153 
   module procedure ids_copy_struct_equilibr2857 
   module procedure ids_copy_struct_equilibr1877 
   module procedure ids_copy_struct_equilibr3124 
   module procedure ids_copy_struct_equilibr1634 
   module procedure ids_copy_struct_equilibr1538 
   module procedure ids_copy_struct_equilibr1539 
   module procedure ids_copy_struct_equilibr732 
   module procedure ids_copy_struct_equilibr1370 
   module procedure ids_copy_struct_equilibr1480 
   module procedure ids_copy_struct_equilibr331

end interface ids_copy


contains


 
subroutine ids_copy_struct_equilibr3272(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_profiles_1d_rz1d_dynamic_aos) :: struct_in, struct_out

      
! Copy %r
if (associated(struct_in%r)) then
   allocate(struct_out%r&
      (size(struct_in%r,1)))
   struct_out%r = &
   struct_in%r
endif

					
! Copy %z
if (associated(struct_in%z)) then
   allocate(struct_out%z&
      (size(struct_in%z,1)))
   struct_out%z = &
   struct_in%z
endif

					

return
end subroutine ids_copy_struct_equilibr3272

 
subroutine ids_copy_struct_equilibr1593(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_convergence) :: struct_in, struct_out

      
! Copy %iterations_n
if (struct_in%iterations_n/=ids_int_invalid)  then
   struct_out%iterations_n = &
   struct_in%iterations_n
endif

					

return
end subroutine ids_copy_struct_equilibr1593

 
subroutine ids_copy_struct_equilibr2480(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_boundary_separatrix) :: struct_in, struct_out

      
! Copy %type
if (struct_in%type/=ids_int_invalid)  then
   struct_out%type = &
   struct_in%type
endif

					 
      call ids_copy(struct_in%outline, struct_out%outline)

        

			
! Copy %psi
if (struct_in%psi.NE.ids_real_invalid) then
   struct_out%psi = &
   struct_in%psi
endif

					 
      call ids_copy(struct_in%geometric_axis, struct_out%geometric_axis)

        

			
! Copy %minor_radius
if (struct_in%minor_radius.NE.ids_real_invalid) then
   struct_out%minor_radius = &
   struct_in%minor_radius
endif

					
! Copy %elongation
if (struct_in%elongation.NE.ids_real_invalid) then
   struct_out%elongation = &
   struct_in%elongation
endif

					
! Copy %elongation_upper
if (struct_in%elongation_upper.NE.ids_real_invalid) then
   struct_out%elongation_upper = &
   struct_in%elongation_upper
endif

					
! Copy %elongation_lower
if (struct_in%elongation_lower.NE.ids_real_invalid) then
   struct_out%elongation_lower = &
   struct_in%elongation_lower
endif

					
! Copy %triangularity
if (struct_in%triangularity.NE.ids_real_invalid) then
   struct_out%triangularity = &
   struct_in%triangularity
endif

					
! Copy %triangularity_upper
if (struct_in%triangularity_upper.NE.ids_real_invalid) then
   struct_out%triangularity_upper = &
   struct_in%triangularity_upper
endif

					
! Copy %triangularity_lower
if (struct_in%triangularity_lower.NE.ids_real_invalid) then
   struct_out%triangularity_lower = &
   struct_in%triangularity_lower
endif

					
! Copy %squareness_upper_inner
if (struct_in%squareness_upper_inner.NE.ids_real_invalid) then
   struct_out%squareness_upper_inner = &
   struct_in%squareness_upper_inner
endif

					
! Copy %squareness_upper_outer
if (struct_in%squareness_upper_outer.NE.ids_real_invalid) then
   struct_out%squareness_upper_outer = &
   struct_in%squareness_upper_outer
endif

					
! Copy %squareness_lower_inner
if (struct_in%squareness_lower_inner.NE.ids_real_invalid) then
   struct_out%squareness_lower_inner = &
   struct_in%squareness_lower_inner
endif

					
! Copy %squareness_lower_outer
if (struct_in%squareness_lower_outer.NE.ids_real_invalid) then
   struct_out%squareness_lower_outer = &
   struct_in%squareness_lower_outer
endif

					  
! Copy %x_point
if (associated(struct_in%x_point)) then  
   allocate(struct_out%x_point(size(struct_in%x_point)))
   do i1 = 1,size(struct_in%x_point)
      call ids_copy(struct_in%x_point(i1), struct_out%x_point(i1))


   enddo
endif
           
! Copy %strike_point
if (associated(struct_in%strike_point)) then  
   allocate(struct_out%strike_point(size(struct_in%strike_point)))
   do i1 = 1,size(struct_in%strike_point)
      call ids_copy(struct_in%strike_point(i1), struct_out%strike_point(i1))


   enddo
endif
          
      call ids_copy(struct_in%active_limiter_point, struct_out%active_limiter_point)

        

			

return
end subroutine ids_copy_struct_equilibr2480

 
subroutine ids_copy_struct_equilibr1294(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_boundary) :: struct_in, struct_out

      
! Copy %type
if (struct_in%type/=ids_int_invalid)  then
   struct_out%type = &
   struct_in%type
endif

					 
      call ids_copy(struct_in%outline, struct_out%outline)

        

			 
      call ids_copy(struct_in%lcfs, struct_out%lcfs)

        

			
! Copy %psi_norm
if (struct_in%psi_norm.NE.ids_real_invalid) then
   struct_out%psi_norm = &
   struct_in%psi_norm
endif

					
! Copy %b_flux_pol_norm
if (struct_in%b_flux_pol_norm.NE.ids_real_invalid) then
   struct_out%b_flux_pol_norm = &
   struct_in%b_flux_pol_norm
endif

					
! Copy %psi
if (struct_in%psi.NE.ids_real_invalid) then
   struct_out%psi = &
   struct_in%psi
endif

					 
      call ids_copy(struct_in%geometric_axis, struct_out%geometric_axis)

        

			
! Copy %minor_radius
if (struct_in%minor_radius.NE.ids_real_invalid) then
   struct_out%minor_radius = &
   struct_in%minor_radius
endif

					
! Copy %elongation
if (struct_in%elongation.NE.ids_real_invalid) then
   struct_out%elongation = &
   struct_in%elongation
endif

					
! Copy %elongation_upper
if (struct_in%elongation_upper.NE.ids_real_invalid) then
   struct_out%elongation_upper = &
   struct_in%elongation_upper
endif

					
! Copy %elongation_lower
if (struct_in%elongation_lower.NE.ids_real_invalid) then
   struct_out%elongation_lower = &
   struct_in%elongation_lower
endif

					
! Copy %triangularity
if (struct_in%triangularity.NE.ids_real_invalid) then
   struct_out%triangularity = &
   struct_in%triangularity
endif

					
! Copy %triangularity_upper
if (struct_in%triangularity_upper.NE.ids_real_invalid) then
   struct_out%triangularity_upper = &
   struct_in%triangularity_upper
endif

					
! Copy %triangularity_lower
if (struct_in%triangularity_lower.NE.ids_real_invalid) then
   struct_out%triangularity_lower = &
   struct_in%triangularity_lower
endif

					
! Copy %squareness_upper_inner
if (struct_in%squareness_upper_inner.NE.ids_real_invalid) then
   struct_out%squareness_upper_inner = &
   struct_in%squareness_upper_inner
endif

					
! Copy %squareness_upper_outer
if (struct_in%squareness_upper_outer.NE.ids_real_invalid) then
   struct_out%squareness_upper_outer = &
   struct_in%squareness_upper_outer
endif

					
! Copy %squareness_lower_inner
if (struct_in%squareness_lower_inner.NE.ids_real_invalid) then
   struct_out%squareness_lower_inner = &
   struct_in%squareness_lower_inner
endif

					
! Copy %squareness_lower_outer
if (struct_in%squareness_lower_outer.NE.ids_real_invalid) then
   struct_out%squareness_lower_outer = &
   struct_in%squareness_lower_outer
endif

					  
! Copy %x_point
if (associated(struct_in%x_point)) then  
   allocate(struct_out%x_point(size(struct_in%x_point)))
   do i1 = 1,size(struct_in%x_point)
      call ids_copy(struct_in%x_point(i1), struct_out%x_point(i1))


   enddo
endif
           
! Copy %strike_point
if (associated(struct_in%strike_point)) then  
   allocate(struct_out%strike_point(size(struct_in%strike_point)))
   do i1 = 1,size(struct_in%strike_point)
      call ids_copy(struct_in%strike_point(i1), struct_out%strike_point(i1))


   enddo
endif
          
      call ids_copy(struct_in%active_limiter_point, struct_out%active_limiter_point)

        

			

return
end subroutine ids_copy_struct_equilibr1294

 
subroutine ids_copy_struct_equilibr3708(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_global_quantities_magnetic_axis) :: struct_in, struct_out

      
! Copy %r
if (struct_in%r.NE.ids_real_invalid) then
   struct_out%r = &
   struct_in%r
endif

					
! Copy %z
if (struct_in%z.NE.ids_real_invalid) then
   struct_out%z = &
   struct_in%z
endif

					
! Copy %b_tor
if (struct_in%b_tor.NE.ids_real_invalid) then
   struct_out%b_tor = &
   struct_in%b_tor
endif

					
! Copy %b_field_tor
if (struct_in%b_field_tor.NE.ids_real_invalid) then
   struct_out%b_field_tor = &
   struct_in%b_field_tor
endif

					

return
end subroutine ids_copy_struct_equilibr3708

 
subroutine ids_copy_struct_equilibr2773(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_global_quantities_qmin) :: struct_in, struct_out

      
! Copy %value
if (struct_in%value.NE.ids_real_invalid) then
   struct_out%value = &
   struct_in%value
endif

					
! Copy %rho_tor_norm
if (struct_in%rho_tor_norm.NE.ids_real_invalid) then
   struct_out%rho_tor_norm = &
   struct_in%rho_tor_norm
endif

					

return
end subroutine ids_copy_struct_equilibr2773

 
subroutine ids_copy_struct_equlibri2136(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equlibrium_global_quantities) :: struct_in, struct_out

      
! Copy %beta_pol
if (struct_in%beta_pol.NE.ids_real_invalid) then
   struct_out%beta_pol = &
   struct_in%beta_pol
endif

					
! Copy %beta_tor
if (struct_in%beta_tor.NE.ids_real_invalid) then
   struct_out%beta_tor = &
   struct_in%beta_tor
endif

					
! Copy %beta_normal
if (struct_in%beta_normal.NE.ids_real_invalid) then
   struct_out%beta_normal = &
   struct_in%beta_normal
endif

					
! Copy %ip
if (struct_in%ip.NE.ids_real_invalid) then
   struct_out%ip = &
   struct_in%ip
endif

					
! Copy %li_3
if (struct_in%li_3.NE.ids_real_invalid) then
   struct_out%li_3 = &
   struct_in%li_3
endif

					
! Copy %volume
if (struct_in%volume.NE.ids_real_invalid) then
   struct_out%volume = &
   struct_in%volume
endif

					
! Copy %area
if (struct_in%area.NE.ids_real_invalid) then
   struct_out%area = &
   struct_in%area
endif

					
! Copy %surface
if (struct_in%surface.NE.ids_real_invalid) then
   struct_out%surface = &
   struct_in%surface
endif

					
! Copy %length_pol
if (struct_in%length_pol.NE.ids_real_invalid) then
   struct_out%length_pol = &
   struct_in%length_pol
endif

					
! Copy %psi_axis
if (struct_in%psi_axis.NE.ids_real_invalid) then
   struct_out%psi_axis = &
   struct_in%psi_axis
endif

					
! Copy %psi_boundary
if (struct_in%psi_boundary.NE.ids_real_invalid) then
   struct_out%psi_boundary = &
   struct_in%psi_boundary
endif

					 
      call ids_copy(struct_in%magnetic_axis, struct_out%magnetic_axis)

        

			
! Copy %q_axis
if (struct_in%q_axis.NE.ids_real_invalid) then
   struct_out%q_axis = &
   struct_in%q_axis
endif

					
! Copy %q_95
if (struct_in%q_95.NE.ids_real_invalid) then
   struct_out%q_95 = &
   struct_in%q_95
endif

					 
      call ids_copy(struct_in%q_min, struct_out%q_min)

        

			
! Copy %energy_mhd
if (struct_in%energy_mhd.NE.ids_real_invalid) then
   struct_out%energy_mhd = &
   struct_in%energy_mhd
endif

					
! Copy %w_mhd
if (struct_in%w_mhd.NE.ids_real_invalid) then
   struct_out%w_mhd = &
   struct_in%w_mhd
endif

					

return
end subroutine ids_copy_struct_equlibri2136

 
subroutine ids_copy_struct_equilibr3153(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_constraints_pure_position) :: struct_in, struct_out

       
      call ids_copy(struct_in%position_measured, struct_out%position_measured)

        

			
! Copy %source
if (associated(struct_in%source)) then
   allocate(struct_out%source&
      (size(struct_in%source,1)))
   struct_out%source = &
   struct_in%source
endif

					
! Copy %time_measurement
if (struct_in%time_measurement.NE.ids_real_invalid) then
   struct_out%time_measurement = &
   struct_in%time_measurement
endif

					
! Copy %exact
if (struct_in%exact/=ids_int_invalid)  then
   struct_out%exact = &
   struct_in%exact
endif

					
! Copy %weight
if (struct_in%weight.NE.ids_real_invalid) then
   struct_out%weight = &
   struct_in%weight
endif

					 
      call ids_copy(struct_in%position_reconstructed, struct_out%position_reconstructed)

        

			
! Copy %chi_squared_r
if (struct_in%chi_squared_r.NE.ids_real_invalid) then
   struct_out%chi_squared_r = &
   struct_in%chi_squared_r
endif

					
! Copy %chi_squared_z
if (struct_in%chi_squared_z.NE.ids_real_invalid) then
   struct_out%chi_squared_z = &
   struct_in%chi_squared_z
endif

					

return
end subroutine ids_copy_struct_equilibr3153

 
subroutine ids_copy_struct_equilibr2857(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_constraints_0D_position) :: struct_in, struct_out

      
! Copy %measured
if (struct_in%measured.NE.ids_real_invalid) then
   struct_out%measured = &
   struct_in%measured
endif

					 
      call ids_copy(struct_in%position, struct_out%position)

        

			
! Copy %source
if (associated(struct_in%source)) then
   allocate(struct_out%source&
      (size(struct_in%source,1)))
   struct_out%source = &
   struct_in%source
endif

					
! Copy %time_measurement
if (struct_in%time_measurement.NE.ids_real_invalid) then
   struct_out%time_measurement = &
   struct_in%time_measurement
endif

					
! Copy %exact
if (struct_in%exact/=ids_int_invalid)  then
   struct_out%exact = &
   struct_in%exact
endif

					
! Copy %weight
if (struct_in%weight.NE.ids_real_invalid) then
   struct_out%weight = &
   struct_in%weight
endif

					
! Copy %reconstructed
if (struct_in%reconstructed.NE.ids_real_invalid) then
   struct_out%reconstructed = &
   struct_in%reconstructed
endif

					
! Copy %chi_squared
if (struct_in%chi_squared.NE.ids_real_invalid) then
   struct_out%chi_squared = &
   struct_in%chi_squared
endif

					

return
end subroutine ids_copy_struct_equilibr2857

 
subroutine ids_copy_struct_equilibr1877(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_constraints_0D) :: struct_in, struct_out

      
! Copy %measured
if (struct_in%measured.NE.ids_real_invalid) then
   struct_out%measured = &
   struct_in%measured
endif

					
! Copy %source
if (associated(struct_in%source)) then
   allocate(struct_out%source&
      (size(struct_in%source,1)))
   struct_out%source = &
   struct_in%source
endif

					
! Copy %time_measurement
if (struct_in%time_measurement.NE.ids_real_invalid) then
   struct_out%time_measurement = &
   struct_in%time_measurement
endif

					
! Copy %exact
if (struct_in%exact/=ids_int_invalid)  then
   struct_out%exact = &
   struct_in%exact
endif

					
! Copy %weight
if (struct_in%weight.NE.ids_real_invalid) then
   struct_out%weight = &
   struct_in%weight
endif

					
! Copy %reconstructed
if (struct_in%reconstructed.NE.ids_real_invalid) then
   struct_out%reconstructed = &
   struct_in%reconstructed
endif

					
! Copy %chi_squared
if (struct_in%chi_squared.NE.ids_real_invalid) then
   struct_out%chi_squared = &
   struct_in%chi_squared
endif

					

return
end subroutine ids_copy_struct_equilibr1877

 
subroutine ids_copy_struct_equilibr3124(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_constraints_magnetisation) :: struct_in, struct_out

       
      call ids_copy(struct_in%magnetisation_r, struct_out%magnetisation_r)

        

			 
      call ids_copy(struct_in%magnetisation_z, struct_out%magnetisation_z)

        

			

return
end subroutine ids_copy_struct_equilibr3124

 
subroutine ids_copy_struct_equilibr1634(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_constraints) :: struct_in, struct_out

       
      call ids_copy(struct_in%b_field_tor_vacuum_r, struct_out%b_field_tor_vacuum_r)

        

			  
! Copy %bpol_probe
if (associated(struct_in%bpol_probe)) then  
   allocate(struct_out%bpol_probe(size(struct_in%bpol_probe)))
   do i1 = 1,size(struct_in%bpol_probe)
      call ids_copy(struct_in%bpol_probe(i1), struct_out%bpol_probe(i1))


   enddo
endif
          
      call ids_copy(struct_in%diamagnetic_flux, struct_out%diamagnetic_flux)

        

			  
! Copy %faraday_angle
if (associated(struct_in%faraday_angle)) then  
   allocate(struct_out%faraday_angle(size(struct_in%faraday_angle)))
   do i1 = 1,size(struct_in%faraday_angle)
      call ids_copy(struct_in%faraday_angle(i1), struct_out%faraday_angle(i1))


   enddo
endif
           
! Copy %mse_polarisation_angle
if (associated(struct_in%mse_polarisation_angle)) then  
   allocate(struct_out%mse_polarisation_angle(size(struct_in%mse_polarisation_angle)))
   do i1 = 1,size(struct_in%mse_polarisation_angle)
      call ids_copy(struct_in%mse_polarisation_angle(i1), struct_out%mse_polarisation_angle(i1))


   enddo
endif
           
! Copy %flux_loop
if (associated(struct_in%flux_loop)) then  
   allocate(struct_out%flux_loop(size(struct_in%flux_loop)))
   do i1 = 1,size(struct_in%flux_loop)
      call ids_copy(struct_in%flux_loop(i1), struct_out%flux_loop(i1))


   enddo
endif
          
      call ids_copy(struct_in%ip, struct_out%ip)

        

			  
! Copy %iron_core_segment
if (associated(struct_in%iron_core_segment)) then  
   allocate(struct_out%iron_core_segment(size(struct_in%iron_core_segment)))
   do i1 = 1,size(struct_in%iron_core_segment)
      call ids_copy(struct_in%iron_core_segment(i1), struct_out%iron_core_segment(i1))


   enddo
endif
           
! Copy %n_e
if (associated(struct_in%n_e)) then  
   allocate(struct_out%n_e(size(struct_in%n_e)))
   do i1 = 1,size(struct_in%n_e)
      call ids_copy(struct_in%n_e(i1), struct_out%n_e(i1))


   enddo
endif
           
! Copy %n_e_line
if (associated(struct_in%n_e_line)) then  
   allocate(struct_out%n_e_line(size(struct_in%n_e_line)))
   do i1 = 1,size(struct_in%n_e_line)
      call ids_copy(struct_in%n_e_line(i1), struct_out%n_e_line(i1))


   enddo
endif
           
! Copy %pf_current
if (associated(struct_in%pf_current)) then  
   allocate(struct_out%pf_current(size(struct_in%pf_current)))
   do i1 = 1,size(struct_in%pf_current)
      call ids_copy(struct_in%pf_current(i1), struct_out%pf_current(i1))


   enddo
endif
           
! Copy %pressure
if (associated(struct_in%pressure)) then  
   allocate(struct_out%pressure(size(struct_in%pressure)))
   do i1 = 1,size(struct_in%pressure)
      call ids_copy(struct_in%pressure(i1), struct_out%pressure(i1))


   enddo
endif
           
! Copy %q
if (associated(struct_in%q)) then  
   allocate(struct_out%q(size(struct_in%q)))
   do i1 = 1,size(struct_in%q)
      call ids_copy(struct_in%q(i1), struct_out%q(i1))


   enddo
endif
           
! Copy %x_point
if (associated(struct_in%x_point)) then  
   allocate(struct_out%x_point(size(struct_in%x_point)))
   do i1 = 1,size(struct_in%x_point)
      call ids_copy(struct_in%x_point(i1), struct_out%x_point(i1))


   enddo
endif
         

return
end subroutine ids_copy_struct_equilibr1634

 
subroutine ids_copy_struct_equilibr1538(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_profiles_1d) :: struct_in, struct_out

      
! Copy %psi
if (associated(struct_in%psi)) then
   allocate(struct_out%psi&
      (size(struct_in%psi,1)))
   struct_out%psi = &
   struct_in%psi
endif

					
! Copy %phi
if (associated(struct_in%phi)) then
   allocate(struct_out%phi&
      (size(struct_in%phi,1)))
   struct_out%phi = &
   struct_in%phi
endif

					
! Copy %pressure
if (associated(struct_in%pressure)) then
   allocate(struct_out%pressure&
      (size(struct_in%pressure,1)))
   struct_out%pressure = &
   struct_in%pressure
endif

					
! Copy %f
if (associated(struct_in%f)) then
   allocate(struct_out%f&
      (size(struct_in%f,1)))
   struct_out%f = &
   struct_in%f
endif

					
! Copy %dpressure_dpsi
if (associated(struct_in%dpressure_dpsi)) then
   allocate(struct_out%dpressure_dpsi&
      (size(struct_in%dpressure_dpsi,1)))
   struct_out%dpressure_dpsi = &
   struct_in%dpressure_dpsi
endif

					
! Copy %f_df_dpsi
if (associated(struct_in%f_df_dpsi)) then
   allocate(struct_out%f_df_dpsi&
      (size(struct_in%f_df_dpsi,1)))
   struct_out%f_df_dpsi = &
   struct_in%f_df_dpsi
endif

					
! Copy %j_tor
if (associated(struct_in%j_tor)) then
   allocate(struct_out%j_tor&
      (size(struct_in%j_tor,1)))
   struct_out%j_tor = &
   struct_in%j_tor
endif

					
! Copy %j_parallel
if (associated(struct_in%j_parallel)) then
   allocate(struct_out%j_parallel&
      (size(struct_in%j_parallel,1)))
   struct_out%j_parallel = &
   struct_in%j_parallel
endif

					
! Copy %q
if (associated(struct_in%q)) then
   allocate(struct_out%q&
      (size(struct_in%q,1)))
   struct_out%q = &
   struct_in%q
endif

					
! Copy %magnetic_shear
if (associated(struct_in%magnetic_shear)) then
   allocate(struct_out%magnetic_shear&
      (size(struct_in%magnetic_shear,1)))
   struct_out%magnetic_shear = &
   struct_in%magnetic_shear
endif

					
! Copy %r_inboard
if (associated(struct_in%r_inboard)) then
   allocate(struct_out%r_inboard&
      (size(struct_in%r_inboard,1)))
   struct_out%r_inboard = &
   struct_in%r_inboard
endif

					
! Copy %r_outboard
if (associated(struct_in%r_outboard)) then
   allocate(struct_out%r_outboard&
      (size(struct_in%r_outboard,1)))
   struct_out%r_outboard = &
   struct_in%r_outboard
endif

					
! Copy %rho_tor
if (associated(struct_in%rho_tor)) then
   allocate(struct_out%rho_tor&
      (size(struct_in%rho_tor,1)))
   struct_out%rho_tor = &
   struct_in%rho_tor
endif

					
! Copy %rho_tor_norm
if (associated(struct_in%rho_tor_norm)) then
   allocate(struct_out%rho_tor_norm&
      (size(struct_in%rho_tor_norm,1)))
   struct_out%rho_tor_norm = &
   struct_in%rho_tor_norm
endif

					
! Copy %dpsi_drho_tor
if (associated(struct_in%dpsi_drho_tor)) then
   allocate(struct_out%dpsi_drho_tor&
      (size(struct_in%dpsi_drho_tor,1)))
   struct_out%dpsi_drho_tor = &
   struct_in%dpsi_drho_tor
endif

					 
      call ids_copy(struct_in%geometric_axis, struct_out%geometric_axis)

        

			
! Copy %elongation
if (associated(struct_in%elongation)) then
   allocate(struct_out%elongation&
      (size(struct_in%elongation,1)))
   struct_out%elongation = &
   struct_in%elongation
endif

					
! Copy %triangularity_upper
if (associated(struct_in%triangularity_upper)) then
   allocate(struct_out%triangularity_upper&
      (size(struct_in%triangularity_upper,1)))
   struct_out%triangularity_upper = &
   struct_in%triangularity_upper
endif

					
! Copy %triangularity_lower
if (associated(struct_in%triangularity_lower)) then
   allocate(struct_out%triangularity_lower&
      (size(struct_in%triangularity_lower,1)))
   struct_out%triangularity_lower = &
   struct_in%triangularity_lower
endif

					
! Copy %squareness_upper_inner
if (associated(struct_in%squareness_upper_inner)) then
   allocate(struct_out%squareness_upper_inner&
      (size(struct_in%squareness_upper_inner,1)))
   struct_out%squareness_upper_inner = &
   struct_in%squareness_upper_inner
endif

					
! Copy %squareness_upper_outer
if (associated(struct_in%squareness_upper_outer)) then
   allocate(struct_out%squareness_upper_outer&
      (size(struct_in%squareness_upper_outer,1)))
   struct_out%squareness_upper_outer = &
   struct_in%squareness_upper_outer
endif

					
! Copy %squareness_lower_inner
if (associated(struct_in%squareness_lower_inner)) then
   allocate(struct_out%squareness_lower_inner&
      (size(struct_in%squareness_lower_inner,1)))
   struct_out%squareness_lower_inner = &
   struct_in%squareness_lower_inner
endif

					
! Copy %squareness_lower_outer
if (associated(struct_in%squareness_lower_outer)) then
   allocate(struct_out%squareness_lower_outer&
      (size(struct_in%squareness_lower_outer,1)))
   struct_out%squareness_lower_outer = &
   struct_in%squareness_lower_outer
endif

					
! Copy %volume
if (associated(struct_in%volume)) then
   allocate(struct_out%volume&
      (size(struct_in%volume,1)))
   struct_out%volume = &
   struct_in%volume
endif

					
! Copy %rho_volume_norm
if (associated(struct_in%rho_volume_norm)) then
   allocate(struct_out%rho_volume_norm&
      (size(struct_in%rho_volume_norm,1)))
   struct_out%rho_volume_norm = &
   struct_in%rho_volume_norm
endif

					
! Copy %dvolume_dpsi
if (associated(struct_in%dvolume_dpsi)) then
   allocate(struct_out%dvolume_dpsi&
      (size(struct_in%dvolume_dpsi,1)))
   struct_out%dvolume_dpsi = &
   struct_in%dvolume_dpsi
endif

					
! Copy %dvolume_drho_tor
if (associated(struct_in%dvolume_drho_tor)) then
   allocate(struct_out%dvolume_drho_tor&
      (size(struct_in%dvolume_drho_tor,1)))
   struct_out%dvolume_drho_tor = &
   struct_in%dvolume_drho_tor
endif

					
! Copy %area
if (associated(struct_in%area)) then
   allocate(struct_out%area&
      (size(struct_in%area,1)))
   struct_out%area = &
   struct_in%area
endif

					
! Copy %darea_dpsi
if (associated(struct_in%darea_dpsi)) then
   allocate(struct_out%darea_dpsi&
      (size(struct_in%darea_dpsi,1)))
   struct_out%darea_dpsi = &
   struct_in%darea_dpsi
endif

					
! Copy %darea_drho_tor
if (associated(struct_in%darea_drho_tor)) then
   allocate(struct_out%darea_drho_tor&
      (size(struct_in%darea_drho_tor,1)))
   struct_out%darea_drho_tor = &
   struct_in%darea_drho_tor
endif

					
! Copy %surface
if (associated(struct_in%surface)) then
   allocate(struct_out%surface&
      (size(struct_in%surface,1)))
   struct_out%surface = &
   struct_in%surface
endif

					
! Copy %trapped_fraction
if (associated(struct_in%trapped_fraction)) then
   allocate(struct_out%trapped_fraction&
      (size(struct_in%trapped_fraction,1)))
   struct_out%trapped_fraction = &
   struct_in%trapped_fraction
endif

					
! Copy %gm1
if (associated(struct_in%gm1)) then
   allocate(struct_out%gm1&
      (size(struct_in%gm1,1)))
   struct_out%gm1 = &
   struct_in%gm1
endif

					
! Copy %gm2
if (associated(struct_in%gm2)) then
   allocate(struct_out%gm2&
      (size(struct_in%gm2,1)))
   struct_out%gm2 = &
   struct_in%gm2
endif

					
! Copy %gm3
if (associated(struct_in%gm3)) then
   allocate(struct_out%gm3&
      (size(struct_in%gm3,1)))
   struct_out%gm3 = &
   struct_in%gm3
endif

					
! Copy %gm4
if (associated(struct_in%gm4)) then
   allocate(struct_out%gm4&
      (size(struct_in%gm4,1)))
   struct_out%gm4 = &
   struct_in%gm4
endif

					
! Copy %gm5
if (associated(struct_in%gm5)) then
   allocate(struct_out%gm5&
      (size(struct_in%gm5,1)))
   struct_out%gm5 = &
   struct_in%gm5
endif

					
! Copy %gm6
if (associated(struct_in%gm6)) then
   allocate(struct_out%gm6&
      (size(struct_in%gm6,1)))
   struct_out%gm6 = &
   struct_in%gm6
endif

					
! Copy %gm7
if (associated(struct_in%gm7)) then
   allocate(struct_out%gm7&
      (size(struct_in%gm7,1)))
   struct_out%gm7 = &
   struct_in%gm7
endif

					
! Copy %gm8
if (associated(struct_in%gm8)) then
   allocate(struct_out%gm8&
      (size(struct_in%gm8,1)))
   struct_out%gm8 = &
   struct_in%gm8
endif

					
! Copy %gm9
if (associated(struct_in%gm9)) then
   allocate(struct_out%gm9&
      (size(struct_in%gm9,1)))
   struct_out%gm9 = &
   struct_in%gm9
endif

					
! Copy %b_average
if (associated(struct_in%b_average)) then
   allocate(struct_out%b_average&
      (size(struct_in%b_average,1)))
   struct_out%b_average = &
   struct_in%b_average
endif

					
! Copy %b_field_average
if (associated(struct_in%b_field_average)) then
   allocate(struct_out%b_field_average&
      (size(struct_in%b_field_average,1)))
   struct_out%b_field_average = &
   struct_in%b_field_average
endif

					
! Copy %b_min
if (associated(struct_in%b_min)) then
   allocate(struct_out%b_min&
      (size(struct_in%b_min,1)))
   struct_out%b_min = &
   struct_in%b_min
endif

					
! Copy %b_field_min
if (associated(struct_in%b_field_min)) then
   allocate(struct_out%b_field_min&
      (size(struct_in%b_field_min,1)))
   struct_out%b_field_min = &
   struct_in%b_field_min
endif

					
! Copy %b_max
if (associated(struct_in%b_max)) then
   allocate(struct_out%b_max&
      (size(struct_in%b_max,1)))
   struct_out%b_max = &
   struct_in%b_max
endif

					
! Copy %b_field_max
if (associated(struct_in%b_field_max)) then
   allocate(struct_out%b_field_max&
      (size(struct_in%b_field_max,1)))
   struct_out%b_field_max = &
   struct_in%b_field_max
endif

					
! Copy %beta_pol
if (associated(struct_in%beta_pol)) then
   allocate(struct_out%beta_pol&
      (size(struct_in%beta_pol,1)))
   struct_out%beta_pol = &
   struct_in%beta_pol
endif

					
! Copy %mass_density
if (associated(struct_in%mass_density)) then
   allocate(struct_out%mass_density&
      (size(struct_in%mass_density,1)))
   struct_out%mass_density = &
   struct_in%mass_density
endif

					

return
end subroutine ids_copy_struct_equilibr1538

 
subroutine ids_copy_struct_equilibr1539(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_profiles_2d) :: struct_in, struct_out

       
      call ids_copy(struct_in%grid_type, struct_out%grid_type)

        

			 
      call ids_copy(struct_in%grid, struct_out%grid)

        

			
! Copy %r
if (associated(struct_in%r)) then
   allocate(struct_out%r&
      (size(struct_in%r,1), &
      size(struct_in%r,2)))
   struct_out%r = &
   struct_in%r
endif

					
! Copy %z
if (associated(struct_in%z)) then
   allocate(struct_out%z&
      (size(struct_in%z,1), &
      size(struct_in%z,2)))
   struct_out%z = &
   struct_in%z
endif

					
! Copy %psi
if (associated(struct_in%psi)) then
   allocate(struct_out%psi&
      (size(struct_in%psi,1), &
      size(struct_in%psi,2)))
   struct_out%psi = &
   struct_in%psi
endif

					
! Copy %theta
if (associated(struct_in%theta)) then
   allocate(struct_out%theta&
      (size(struct_in%theta,1), &
      size(struct_in%theta,2)))
   struct_out%theta = &
   struct_in%theta
endif

					
! Copy %phi
if (associated(struct_in%phi)) then
   allocate(struct_out%phi&
      (size(struct_in%phi,1), &
      size(struct_in%phi,2)))
   struct_out%phi = &
   struct_in%phi
endif

					
! Copy %j_tor
if (associated(struct_in%j_tor)) then
   allocate(struct_out%j_tor&
      (size(struct_in%j_tor,1), &
      size(struct_in%j_tor,2)))
   struct_out%j_tor = &
   struct_in%j_tor
endif

					
! Copy %j_parallel
if (associated(struct_in%j_parallel)) then
   allocate(struct_out%j_parallel&
      (size(struct_in%j_parallel,1), &
      size(struct_in%j_parallel,2)))
   struct_out%j_parallel = &
   struct_in%j_parallel
endif

					
! Copy %b_r
if (associated(struct_in%b_r)) then
   allocate(struct_out%b_r&
      (size(struct_in%b_r,1), &
      size(struct_in%b_r,2)))
   struct_out%b_r = &
   struct_in%b_r
endif

					
! Copy %b_field_r
if (associated(struct_in%b_field_r)) then
   allocate(struct_out%b_field_r&
      (size(struct_in%b_field_r,1), &
      size(struct_in%b_field_r,2)))
   struct_out%b_field_r = &
   struct_in%b_field_r
endif

					
! Copy %b_z
if (associated(struct_in%b_z)) then
   allocate(struct_out%b_z&
      (size(struct_in%b_z,1), &
      size(struct_in%b_z,2)))
   struct_out%b_z = &
   struct_in%b_z
endif

					
! Copy %b_field_z
if (associated(struct_in%b_field_z)) then
   allocate(struct_out%b_field_z&
      (size(struct_in%b_field_z,1), &
      size(struct_in%b_field_z,2)))
   struct_out%b_field_z = &
   struct_in%b_field_z
endif

					
! Copy %b_tor
if (associated(struct_in%b_tor)) then
   allocate(struct_out%b_tor&
      (size(struct_in%b_tor,1), &
      size(struct_in%b_tor,2)))
   struct_out%b_tor = &
   struct_in%b_tor
endif

					
! Copy %b_field_tor
if (associated(struct_in%b_field_tor)) then
   allocate(struct_out%b_field_tor&
      (size(struct_in%b_field_tor,1), &
      size(struct_in%b_field_tor,2)))
   struct_out%b_field_tor = &
   struct_in%b_field_tor
endif

					

return
end subroutine ids_copy_struct_equilibr1539

 
subroutine ids_copy_struct_equilibr732(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_ggd) :: struct_in, struct_out

       
      call ids_copy(struct_in%grid, struct_out%grid)

        

			  
! Copy %r
if (associated(struct_in%r)) then  
   allocate(struct_out%r(size(struct_in%r)))
   do i1 = 1,size(struct_in%r)
      call ids_copy(struct_in%r(i1), struct_out%r(i1))


   enddo
endif
           
! Copy %z
if (associated(struct_in%z)) then  
   allocate(struct_out%z(size(struct_in%z)))
   do i1 = 1,size(struct_in%z)
      call ids_copy(struct_in%z(i1), struct_out%z(i1))


   enddo
endif
           
! Copy %psi
if (associated(struct_in%psi)) then  
   allocate(struct_out%psi(size(struct_in%psi)))
   do i1 = 1,size(struct_in%psi)
      call ids_copy(struct_in%psi(i1), struct_out%psi(i1))


   enddo
endif
           
! Copy %phi
if (associated(struct_in%phi)) then  
   allocate(struct_out%phi(size(struct_in%phi)))
   do i1 = 1,size(struct_in%phi)
      call ids_copy(struct_in%phi(i1), struct_out%phi(i1))


   enddo
endif
           
! Copy %theta
if (associated(struct_in%theta)) then  
   allocate(struct_out%theta(size(struct_in%theta)))
   do i1 = 1,size(struct_in%theta)
      call ids_copy(struct_in%theta(i1), struct_out%theta(i1))


   enddo
endif
           
! Copy %j_tor
if (associated(struct_in%j_tor)) then  
   allocate(struct_out%j_tor(size(struct_in%j_tor)))
   do i1 = 1,size(struct_in%j_tor)
      call ids_copy(struct_in%j_tor(i1), struct_out%j_tor(i1))


   enddo
endif
           
! Copy %j_parallel
if (associated(struct_in%j_parallel)) then  
   allocate(struct_out%j_parallel(size(struct_in%j_parallel)))
   do i1 = 1,size(struct_in%j_parallel)
      call ids_copy(struct_in%j_parallel(i1), struct_out%j_parallel(i1))


   enddo
endif
           
! Copy %b_field_r
if (associated(struct_in%b_field_r)) then  
   allocate(struct_out%b_field_r(size(struct_in%b_field_r)))
   do i1 = 1,size(struct_in%b_field_r)
      call ids_copy(struct_in%b_field_r(i1), struct_out%b_field_r(i1))


   enddo
endif
           
! Copy %b_field_z
if (associated(struct_in%b_field_z)) then  
   allocate(struct_out%b_field_z(size(struct_in%b_field_z)))
   do i1 = 1,size(struct_in%b_field_z)
      call ids_copy(struct_in%b_field_z(i1), struct_out%b_field_z(i1))


   enddo
endif
           
! Copy %b_field_tor
if (associated(struct_in%b_field_tor)) then  
   allocate(struct_out%b_field_tor(size(struct_in%b_field_tor)))
   do i1 = 1,size(struct_in%b_field_tor)
      call ids_copy(struct_in%b_field_tor(i1), struct_out%b_field_tor(i1))


   enddo
endif
         

return
end subroutine ids_copy_struct_equilibr732

 
subroutine ids_copy_struct_equilibr1370(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_ggd_array) :: struct_in, struct_out

        
! Copy %grid
if (associated(struct_in%grid)) then  
   allocate(struct_out%grid(size(struct_in%grid)))
   do i1 = 1,size(struct_in%grid)
      call ids_copy(struct_in%grid(i1), struct_out%grid(i1))


   enddo
endif
         
! Copy %time
if (struct_in%time.NE.ids_real_invalid) then
   struct_out%time = &
   struct_in%time
endif

					

return
end subroutine ids_copy_struct_equilibr1370

 
subroutine ids_copy_struct_equilibr1480(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium_time_slice) :: struct_in, struct_out

       
      call ids_copy(struct_in%boundary, struct_out%boundary)

        

			 
      call ids_copy(struct_in%boundary_separatrix, struct_out%boundary_separatrix)

        

			 
      call ids_copy(struct_in%constraints, struct_out%constraints)

        

			 
      call ids_copy(struct_in%global_quantities, struct_out%global_quantities)

        

			 
      call ids_copy(struct_in%profiles_1d, struct_out%profiles_1d)

        

			  
! Copy %profiles_2d
if (associated(struct_in%profiles_2d)) then  
   allocate(struct_out%profiles_2d(size(struct_in%profiles_2d)))
   do i1 = 1,size(struct_in%profiles_2d)
      call ids_copy(struct_in%profiles_2d(i1), struct_out%profiles_2d(i1))


   enddo
endif
           
! Copy %ggd
if (associated(struct_in%ggd)) then  
   allocate(struct_out%ggd(size(struct_in%ggd)))
   do i1 = 1,size(struct_in%ggd)
      call ids_copy(struct_in%ggd(i1), struct_out%ggd(i1))


   enddo
endif
          
      call ids_copy(struct_in%coordinate_system, struct_out%coordinate_system)

        

			 
      call ids_copy(struct_in%convergence, struct_out%convergence)

        

			
! Copy %time
if (struct_in%time.NE.ids_real_invalid) then
   struct_out%time = &
   struct_in%time
endif

					

return
end subroutine ids_copy_struct_equilibr1480

 
subroutine ids_copy_struct_equilibr331(struct_in,  struct_out)
! Copies all fields of struct_in to struct_out
! Assumes that struct_in is a single instance of a given structure

use ids_schemas
implicit none

integer(ids_int) :: itime, lentime, lenstring, istring
integer(ids_int) :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium) :: struct_in, struct_out

       
      call ids_copy(struct_in%ids_properties, struct_out%ids_properties)
			 
      call ids_copy(struct_in%vacuum_toroidal_field, struct_out%vacuum_toroidal_field)

        

			  
! Copy %grids_ggd
if (associated(struct_in%grids_ggd)) then  
   allocate(struct_out%grids_ggd(size(struct_in%grids_ggd)))
   do i1 = 1,size(struct_in%grids_ggd)
      call ids_copy(struct_in%grids_ggd(i1), struct_out%grids_ggd(i1))


   enddo
endif
           
! Copy %time_slice
if (associated(struct_in%time_slice)) then  
   allocate(struct_out%time_slice(size(struct_in%time_slice)))
   do i1 = 1,size(struct_in%time_slice)
      call ids_copy(struct_in%time_slice(i1), struct_out%time_slice(i1))


   enddo
endif
          
      call ids_copy(struct_in%code, struct_out%code)
			 
! Copy time
if (associated(struct_in%time)) then
   allocate(struct_out%time&
      (size(struct_in%time,1)))
   struct_out%time = &
   struct_in%time
endif

			

return
end subroutine ids_copy_struct_equilibr331


end module
