program test_fortranworkflow_itm

  use itm_types

  use euITM_schemas
  use copy_structures
  use euITM_routines

  IMPLICIT NONE

  interface
     subroutine COCOStransform(equilibrium_in, equilibrium_out, COCOS_in, COCOS_out, IPsign_out, B0sign_out)
       !
       use itm_types
       
       use euITM_schemas
       use copy_structures
       type (type_equilibrium), pointer  ::  equilibrium_in(:) ! should not use intent(in) or out with pointers
       type (type_equilibrium), pointer  ::  equilibrium_out(:)
       integer, optional, intent(IN) :: COCOS_in
       integer, optional, intent(IN) :: COCOS_out
       integer, optional, intent(IN) :: IPsign_out
       integer, optional, intent(IN) :: B0sign_out
     end subroutine COCOStransform
  end interface

  type(type_equilibrium), pointer  :: equil_in_itm(:), equil_out_itm(:), equil_out2_itm(:)

  integer :: shot_in=181, shot_out=181, run_in=301, run_out=311, idx, it
  character*5 :: euitmtree

  euitmtree = 'euitm'
  call euitm_open(euitmtree,shot_in,run_in,idx)
  call euitm_get(idx,"equilibrium",equil_in_itm)
  call euitm_close(idx)
  it=1
  ! print *,'equil_in_itm%euitm_properties%cocos= ',equil_in_itm%euitm_properties%cocos
  print *,'size(equil_in_itm)= ',size(equil_in_itm)
  if (associated(equil_in_itm(it)%datainfo%source)) print *,'equil_in_itm(it)%datainfo%source= ',equil_in_itm(it)%datainfo%source
  if (associated(equil_in_itm(it)%codeparam%codeversion)) print *,'equil_in_itm(it)%codeparam%codeversion= ',equil_in_itm(it)%codeparam%codeversion
  print *,'equil_in_itm(1)%coord_sys%grid_type= ',equil_in_itm(1)%coord_sys%grid_type
  print *,'equil_in_itm(it)%datainfo%cocos= ',equil_in_itm(it)%datainfo%cocos
  !
  call COCOStransform(equil_in_itm,equil_out_itm,equil_in_itm(it)%datainfo%cocos,2)
  call COCOStransform(equil_out_itm,equil_out2_itm,2,17)
  call COCOStransform(equil_out2_itm,equil_out_itm,17,11)
  print *,'equil_out_itm(it)%datainfo%cocos= ',equil_out_itm(it)%datainfo%cocos
  print *,'equil_out_itm(it)%time= ',equil_out_itm(it)%time
  if (associated(equil_out_itm(it)%codeparam%codename)) print *,'equil_out_itm(it)%codeparam%codename= ',equil_out_itm(it)%codeparam%codename
  if (associated(equil_out_itm(it)%codeparam%codeversion)) print *,'equil_out_itm(it)%codeparam%codeversion= ',equil_out_itm(it)%codeparam%codeversion

  call euitm_create(euitmtree,shot_out,run_out,shot_out,0,idx)
  call euitm_put(idx,"Equilibrium",equil_out_itm)
  call euitm_close(idx)

end program test_fortranworkflow_itm
