#!/bin/tcsh -fe

#if ($IMAS_VERSION == '3.6.1') then
#if ($IMAS_VERSION == '3.7.4') then
if ($IMAS_VERSION == '3.15.1') then
  # module load imas/3.6.1/ual/3.3.11
  # export IMAS_MAJOR_VERSION=3
  # imasdb test
  # export UAL=$IMAS_HOME/core/imas/$IMAS_VERSION/ual/$UAL_VERSION

  # module load fc2k

  make clean
  make cocos_transform_imas
  echo $UAL_VERSION
  echo " "
  echo "Can execute: fc2k fc2k_imas/cocostransform.xml" #  -kepler -docfile doc/cocostransform.txt
else
  echo "error IMAS model version not expected."
endif

# then can test with workflow in ./workflow_test/chease_cocostransform_workflow.xml using shot=12345, run=1 to create run=13 via a cocos transform to cocos=2 and  chease with cocos_in=2
