subroutine COCOStransform(equilibrium_in, equilibrium_out, COCOS_in, COCOS_out, IPsign_out, B0sign_out)
  !
  ! Transform input equilibrium CPO with COCOS_in (from input or from CPO .cocos index if exists) into output equil_CPO with COCOS_out
  !
  ! Can ask for a specific sign of Ip and/or B0 in output as well
  !
  ! It follows the general transformation rules specified in Appendix C of COCOS paper 
  ! (by O. Sauter and S. Yu. Medvevdev, "Tokamak coordinate conventions: COCOS", Comput. Physics Commun. 184 (2013) 293)
  !
  ! without the normalization factors since assume CPO_in and CPO_out are in SI units
  !
  ! Uses cocos_module which contains cocos function (with new theta_sign_clockwise output) 
  ! and COCOS_Values_Coefficients which contains transformation coefficients from cocos_in to cocos_out 
  ! including new fact_dtheta for change of sign of direction of theta
  !
  use ids_schemas
  !use equilibrium_ids_module
  use ids_routines
  use cocos_module

  IMPLICIT NONE
  integer, parameter :: DP = ids_real
  REAL(DP), PARAMETER :: TWOPI=6.283185307179586476925286766559005768394_DP
  INTEGER,  PARAMETER :: itm_int_invalid = ids_int_invalid
  ! REAL(DP), PARAMETER :: itm_DP_invalid = -9.0D40

  type (ids_equilibrium)  ::  equilibrium_in ! should not use intent(in) or out with pointers
  type (ids_equilibrium)  ::  equilibrium_out
  type (ids_equilibrium)  ::  equilibrium_in2
  type (ids_equilibrium)  ::  equilibrium_out2
  integer, optional, intent(IN) :: COCOS_in
  integer, optional, intent(IN) :: COCOS_out
  integer, optional, intent(IN) :: IPsign_out
  integer, optional, intent(IN) :: B0sign_out
  !
  integer :: nb_times, nb_1d, nb_2d_prof, nb_b0, i, it, i_gridtype, j, is_homogeneous_time
  integer :: icocos_in, iexp_Bp_in,isigma_Bp_in,isigma_RphiZ_in,isigma_rhothetaphi_in,isign_q_pos_in,isign_pprime_pos_in,itheta_sign_clockwise_in
  integer :: icocos_out, iexp_Bp_out,isigma_Bp_out,isigma_RphiZ_out,isigma_rhothetaphi_out,isign_q_pos_out,isign_pprime_pos_out
  integer :: iIPsign_out, iB0sign_out
  real(DP) :: z_one, sigma_Ip_in, sigma_B0_in, pedge_rel, sigma_Ip_out, sigma_IP_eff, sigma_B0_eff
  real(DP) :: sigma_Bp_eff, exp_Bp_eff, sigma_rhothetaphi_eff, sigma_RphiZ_eff, fact_q, fact_psi, &
    & fact_dim1_eff, fact_dim2_eff, fact_dim3_eff, fact_dpsi, fact_dtheta

  !
  z_one = 1._DP
  !
  icocos_in = 11
  if (present(COCOS_in)) THEN
    if (COCOS_in .GT. 0) icocos_in = COCOS_in
  end if
  icocos_out = icocos_in
  if (present(COCOS_out)) THEN
    if (COCOS_out .GT. 0) icocos_out = COCOS_out
  end if
  ! Ipsign_out undefined means use input value transformed to new COCOS
  iIPsign_out = itm_int_invalid
  if (present(IPsign_out)) THEN
    if ( (IPsign_out .EQ. -1) .OR. (IPsign_out .EQ. +1) ) iIPsign_out = IPsign_out
  end if
  iB0sign_out = itm_int_invalid
  if (present(B0sign_out)) THEN
    if ( (B0sign_out .EQ. -1) .OR. (B0sign_out .EQ. +1) ) iB0sign_out = B0sign_out
  end if
  !
  ! Check for COCOS consistency (cf Sec. V of paper)
  ! need the expected signs for cocos_in
  call COCOS(icocos_in,iexp_Bp_in,isigma_Bp_in,isigma_RphiZ_in,isigma_rhothetaphi_in,isign_q_pos_in,isign_pprime_pos_in,itheta_sign_clockwise_in)
  !
  ! check ids_properties/homogeneous_time to know which "time" to use (note only for time_slice(:)/time)
  is_homogeneous_time = equilibrium_in%ids_properties%homogeneous_time
  nb_times = 1
  if (associated(equilibrium_in%time_slice)) then
    if (is_homogeneous_time .eq. 1) then
      nb_times = size(equilibrium_in%time)
    else
      nb_times = size(equilibrium_in%time_slice)
    end if
  else
    ! time_slice not associated, return
    return
  end if
  if (nb_times .lt. 1) then
    ! problem with size of input
    return
  end if
  !
  ! check COCOS consistency for 1st time_slice, assume Ip and B0 do not change sign over time for the check
  it = 1
  nb_1d = size(equilibrium_in%time_slice(it)%profiles_1d%q)
  sigma_Ip_in = sign(z_one,equilibrium_in%time_slice(it)%global_quantities%ip)
  sigma_B0_in = sign(z_one,equilibrium_in%vacuum_toroidal_field%b0(it))
  if (nb_1d .gt. 0) then
    if (sign(z_one,equilibrium_in%time_slice(it)%profiles_1d%q(nb_1d))*isigma_rhothetaphi_in*sigma_Ip_in*sigma_B0_in .le. 0._DP) THEN
      write(*,*) 'WARNING: sign(q) is not consistent with COCOS_in=',icocos_in,' value'
      write(*,*) 'qedge = ',equilibrium_in%time_slice(it)%profiles_1d%q(nb_1d)
      write(*,*) 'sig_rhothetaphi*sign(Ip)*sign(B0) = ',isigma_rhothetaphi_in, ' * ',sigma_Ip_in,' * ',sigma_B0_in,' = ', &
        & isigma_rhothetaphi_in*sigma_Ip_in*sigma_B0_in
    end if
    if (sign(z_one,equilibrium_in%time_slice(it)%profiles_1d%f(nb_1d))*sigma_B0_in .LE. 0) THEN
      write(*,*) 'WARNING: Signs of F and B0 are not consistent'
    end if
  end if
  IF (sign(z_one,equilibrium_in%time_slice(it)%global_quantities%psi_boundary-equilibrium_in%time_slice(it)%global_quantities%psi_axis) * &
    & isigma_Bp_in*sigma_Ip_in .LE. 0._DP) THEN
    IF (sign(z_one,equilibrium_in%time_slice(it)%global_quantities%psi_boundary-equilibrium_in%time_slice(it)%global_quantities%psi_axis) &
      & .LE. 0._DP) THEN
      write(*,*) 'WARNING: psi should be increasing with : sign(Ip)=',sigma_Ip_in,' and sigma_Bp=',sigma_Ip_in,' for COCOS=',icocos_in
    ELSE
      write(*,*) 'WARNING: psi should be decreasing with : sign(Ip)=',sigma_Ip_in,' and sigma_Bp=',sigma_Ip_in,' for COCOS=',icocos_in
    END if
  ELSE
    ! check sign of pprime
    IF (associated(equilibrium_in%time_slice(it)%profiles_1d%dpressure_dpsi)) THEN
      pedge_rel=0._DP
      DO i=2,nb_1d
        pedge_rel = pedge_rel + equilibrium_in%time_slice(it)%profiles_1d%dpressure_dpsi(i) * &
          & (equilibrium_in%time_slice(it)%profiles_1d%psi(i)-equilibrium_in%time_slice(it)%profiles_1d%psi(i-1))
      END DO
      IF (pedge_rel .GE. DP) THEN
        write(*,*) 'WARNING: pprime has wrong sign'
      END IF
    END IF
  END IF
  !
  ! Now start changing equilibrium_out
  ! Copy equilibrium_in to _out and then transform relevant quantities according to desired sign(Ip), sign(B0)
  !
  call ids_copy(equilibrium_in,equilibrium_out)
  !
  ! ids_properties
  !
  !  equilibrium_out%ids_properties%cocos = icocos_out ! should be able to put in some metadata
  !
  ! vacuum_toroidal_field
  ! assume size(b0) same as time_slice, but check first
  !
  nb_b0 = size(equilibrium_out%vacuum_toroidal_field%b0)
!!$  if (nb_b0 .ne. nb_times) then
!!$    ! transform B0 according to values at 1st time-step
!!$    sigma_B0_in = sign(z_one,equilibrium_in%vacuum_toroidal_field%b0(1))
!!$    ! sign(B0) in output:
!!$    IF (iB0sign_out .LE. -2) THEN
!!$      sigma_B0_eff = REAL(isigma_RphiZ_in * isigma_RphiZ_out,DP)
!!$    ELSE
!!$      sigma_B0_eff = sigma_B0_in * REAL(iB0sign_out,DP)
!!$    END IF
!!$    equilibrium_out%vacuum_toroidal_field%b0 = sigma_B0_eff * equilibrium_out%vacuum_toroidal_field%b0
!!$  else
!!$    ! will transform with it do loop below
!!$  end if
  !
  do it=1,nb_times
    !
    sigma_Ip_in = sign(z_one,equilibrium_in%time_slice(it)%global_quantities%ip)
    sigma_B0_in = sign(z_one,equilibrium_in%vacuum_toroidal_field%b0(it))
    !
    ! get COCOS related values and coefficients for transformation
    call COCOS_values_coefficients(iCOCOS_in, iCOCOS_out, sigma_Ip_in, sigma_B0_in, &
      & sigma_Ip_eff, sigma_B0_eff, sigma_Bp_eff, sigma_rhothetaphi_eff, sigma_RphiZ_eff, exp_Bp_eff, &
      & fact_psi, fact_q, fact_dpsi, fact_dtheta,  iIpsign_out, iB0sign_out)
    !
    ! vacuum_toroidal_field
    if (nb_b0 .eq. nb_times) then
      equilibrium_out%vacuum_toroidal_field%b0(it) = sigma_B0_eff * equilibrium_out%vacuum_toroidal_field%b0(it)
    else
      equilibrium_out%vacuum_toroidal_field%b0(1) = sigma_B0_eff * equilibrium_out%vacuum_toroidal_field%b0(1)
    end if
    !
    ! boundary: nothing to change
    !
    ! In global_quantities:
    !
    equilibrium_out%time_slice(it)%global_quantities%ip = sigma_Ip_eff * equilibrium_out%time_slice(it)%global_quantities%ip
    equilibrium_out%time_slice(it)%global_quantities%psi_axis = fact_psi * equilibrium_out%time_slice(it)%global_quantities%psi_axis
    equilibrium_out%time_slice(it)%global_quantities%psi_boundary = fact_psi * equilibrium_out%time_slice(it)%global_quantities%psi_boundary
    ! obsolescent section begin
    ! b_tor obsolescent but need to keep for backward compatibility
    equilibrium_out%time_slice(it)%global_quantities%magnetic_axis%b_tor = sigma_B0_eff * &
      & equilibrium_out%time_slice(it)%global_quantities%magnetic_axis%b_tor
    ! obsolescent section end
    equilibrium_out%time_slice(it)%global_quantities%magnetic_axis%b_field_tor = sigma_B0_eff * &
      & equilibrium_out%time_slice(it)%global_quantities%magnetic_axis%b_field_tor
    equilibrium_out%time_slice(it)%global_quantities%q_axis = fact_q * equilibrium_out%time_slice(it)%global_quantities%q_axis
    equilibrium_out%time_slice(it)%global_quantities%q_95 = fact_q * equilibrium_out%time_slice(it)%global_quantities%q_95
    equilibrium_out%time_slice(it)%global_quantities%q_min%value = fact_q * equilibrium_out%time_slice(it)%global_quantities%q_min%value
    !
    ! profiles_1d:
    !
    equilibrium_out%time_slice(it)%profiles_1d%psi = fact_psi * equilibrium_out%time_slice(it)%profiles_1d%psi
    equilibrium_out%time_slice(it)%profiles_1d%phi = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_1d%phi
    equilibrium_out%time_slice(it)%profiles_1d%f = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_1d%f
    equilibrium_out%time_slice(it)%profiles_1d%dpressure_dpsi = sigma_Ip_eff * sigma_Bp_eff / TWOPI**exp_Bp_eff * &
      & equilibrium_out%time_slice(it)%profiles_1d%dpressure_dpsi
    equilibrium_out%time_slice(it)%profiles_1d%f_df_dpsi = sigma_Ip_eff * sigma_Bp_eff / TWOPI**exp_Bp_eff * &
      & equilibrium_out%time_slice(it)%profiles_1d%f_df_dpsi
    equilibrium_out%time_slice(it)%profiles_1d%j_tor = sigma_Ip_eff * equilibrium_out%time_slice(it)%profiles_1d%j_tor
    equilibrium_out%time_slice(it)%profiles_1d%j_parallel = sigma_Ip_eff * equilibrium_out%time_slice(it)%profiles_1d%j_parallel
    equilibrium_out%time_slice(it)%profiles_1d%q = fact_q * equilibrium_out%time_slice(it)%profiles_1d%q
    equilibrium_out%time_slice(it)%profiles_1d%dpsi_drho_tor = fact_psi * equilibrium_out%time_slice(it)%profiles_1d%dpsi_drho_tor
    equilibrium_out%time_slice(it)%profiles_1d%dvolume_dpsi = equilibrium_out%time_slice(it)%profiles_1d%dvolume_dpsi / fact_psi
    ! no change for equilibrium_out%time_slice(it)%profiles_1d%dvolume_drho_tor
    equilibrium_out%time_slice(it)%profiles_1d%darea_dpsi = equilibrium_out%time_slice(it)%profiles_1d%darea_dpsi / fact_psi
    ! obsolescent section begin
    ! b_average, b_min, b_max obsolescent
    equilibrium_out%time_slice(it)%profiles_1d%b_average = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_1d%b_average
    equilibrium_out%time_slice(it)%profiles_1d%b_min = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_1d%b_min
    equilibrium_out%time_slice(it)%profiles_1d%b_max = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_1d%b_max
    ! obsolescent section end
    equilibrium_out%time_slice(it)%profiles_1d%b_field_average = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_1d%b_field_average
    equilibrium_out%time_slice(it)%profiles_1d%b_field_min = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_1d%b_field_min
    equilibrium_out%time_slice(it)%profiles_1d%b_field_max = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_1d%b_field_max
    !
    ! profiles_2d(j):
    !
    nb_2d_prof = 0
    if ( associated(equilibrium_out%time_slice(it)%profiles_2d) ) then
      nb_2d_prof = size(equilibrium_out%time_slice(it)%profiles_2d)
    end if
    if (nb_2d_prof .gt. 0) then
      do j=1,nb_2d_prof
        if ( (equilibrium_out%time_slice(it)%profiles_2d(j)%grid_type%index .ge. 11) .and. &
          & (equilibrium_out%time_slice(it)%profiles_2d(j)%grid_type%index .le. 19)) then
          ! dim1 is psi related thus need to transform it
          equilibrium_out%time_slice(it)%profiles_2d(j)%grid%dim1 = fact_psi * equilibrium_out%time_slice(it)%profiles_2d(j)%grid%dim1
        end if
        ! dim2 is always angle-like if >1 and <90 (from table by O. Sauter)
        ! however angle always from 0 to +2pi, it is the direction of positive theta which changes with cocos
        equilibrium_out%time_slice(it)%profiles_2d(j)%psi = fact_psi * equilibrium_out%time_slice(it)%profiles_2d(j)%psi
        equilibrium_out%time_slice(it)%profiles_2d(j)%theta = sigma_RphiZ_eff * sigma_rhothetaphi_eff * &
          & equilibrium_out%time_slice(it)%profiles_2d(j)%theta
        equilibrium_out%time_slice(it)%profiles_2d(j)%phi = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_2d(j)%phi
        equilibrium_out%time_slice(it)%profiles_2d(j)%j_tor = sigma_Ip_eff * equilibrium_out%time_slice(it)%profiles_2d(j)%j_tor
        equilibrium_out%time_slice(it)%profiles_2d(j)%j_parallel = sigma_Ip_eff * equilibrium_out%time_slice(it)%profiles_2d(j)%j_parallel
        ! if sign(Ip_out(it)) follows from sign(Ip_in), then sigma_Ip_eff=sigma_RphiZ_eff and BR_out(it)=BR_in as it should
        ! if sign(Ip) is changed on demand from inputs, then BR should follow
        ! obsolescent section begin
        ! b_r, b_z obsolescent
        equilibrium_out%time_slice(it)%profiles_2d(j)%b_r = sigma_RphiZ_eff * sigma_Ip_eff * equilibrium_out%time_slice(it)%profiles_2d(j)%b_r
        equilibrium_out%time_slice(it)%profiles_2d(j)%b_z = sigma_RphiZ_eff * sigma_Ip_eff * equilibrium_out%time_slice(it)%profiles_2d(j)%b_z
        equilibrium_out%time_slice(it)%profiles_2d(j)%b_tor = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_2d(j)%b_tor
        ! obsolescent section end
        equilibrium_out%time_slice(it)%profiles_2d(j)%b_field_r = sigma_RphiZ_eff * sigma_Ip_eff * equilibrium_out%time_slice(it)%profiles_2d(j)%b_field_r
        equilibrium_out%time_slice(it)%profiles_2d(j)%b_field_z = sigma_RphiZ_eff * sigma_Ip_eff * equilibrium_out%time_slice(it)%profiles_2d(j)%b_field_z
        equilibrium_out%time_slice(it)%profiles_2d(j)%b_field_tor = sigma_B0_eff * equilibrium_out%time_slice(it)%profiles_2d(j)%b_field_tor
      end do
    end if
    !
    ! coordinate_system: use same table for grid index as for profiles_2d
    ! if index in 11-19, dim1 is "psi" proportional, thus needs to be factorized
    ! if index >=2 and <=89, dim2 is an angle-like, thus depends on RphiZ and rhothetaphi
    ! 
    fact_dim1_eff = 1._DP
    if ( (equilibrium_out%time_slice(it)%coordinate_system%grid_type%index .ge. 11) .and. &
      & (equilibrium_out%time_slice(it)%coordinate_system%grid_type%index .le. 19)) then
      fact_dim1_eff = fact_psi
    else
      ! assume dim1 is R-like or rho-like, so no factor to add
    end if
    fact_dim2_eff = 1._DP
    if ( (equilibrium_out%time_slice(it)%coordinate_system%grid_type%index .ge. 2) .and. &
      & (equilibrium_out%time_slice(it)%coordinate_system%grid_type%index .le. 89)) then
      ! angle-like theta thus depends on rhothetaphi and effective phi
      fact_dim2_eff = sigma_RphiZ_eff * sigma_rhothetaphi_eff
    else
      ! assume dim2 is Z-like, so no factor to add
    end if
    ! Assume dim3 is always phi-like, thus sign depends on RZphi sign
    fact_dim3_eff = sigma_RphiZ_eff
    equilibrium_out%time_slice(it)%coordinate_system%grid%dim1 = fact_dim1_eff * equilibrium_out%time_slice(it)%coordinate_system%grid%dim1
    ! det(g_ij) always contains i^2, j^2 terms, thus sign dependencies of individual dim_i disappear, only psi factor might remain (dim1) from 1/sqrt(.) thus abs(fact)
    equilibrium_out%time_slice(it)%coordinate_system%jacobian = equilibrium_out%time_slice(it)%coordinate_system%jacobian / abs(fact_dim1_eff)
    equilibrium_out%time_slice(it)%coordinate_system%g11_contravariant = fact_dim1_eff**2 * &
      & equilibrium_out%time_slice(it)%coordinate_system%g11_contravariant
    equilibrium_out%time_slice(it)%coordinate_system%g12_contravariant = fact_dim1_eff * fact_dim2_eff * &
      & equilibrium_out%time_slice(it)%coordinate_system%g12_contravariant
    equilibrium_out%time_slice(it)%coordinate_system%g13_contravariant = fact_dim1_eff * fact_dim3_eff * &
      & equilibrium_out%time_slice(it)%coordinate_system%g13_contravariant
    equilibrium_out%time_slice(it)%coordinate_system%g23_contravariant = fact_dim2_eff * fact_dim3_eff * &
      & equilibrium_out%time_slice(it)%coordinate_system%g23_contravariant
    !
  END do
  !
  return
  !
end subroutine COCOStransform
