#!/bin/tcsh -fe

if ($ITM_ENVIRONMENT_LOADED == 'yes') then
  # source $ITMSCRIPTDIR/ITMv1 kepler test 4.10b.10_R2.2.0
 
  # source $ITMSCRIPTDIR/ITMv1 kepler test 4.10b_rc
    # module switch fc2k/rc
    # module switch scripts/R2.2
  make clean
  make cocos_transform
  echo $UAL
  echo " "
  echo "Can execute: fc2k -kepler -docfile doc/cocostransform.txt fc2k_itm/cocostransform.xml"
else
  echo "error ITM environment not available. Cant use makefile option 'ITM_ENVIRONMENT_LOADED=yes'"
endif

# then can test with workflow in ./workflow_test/chease_cocostransform_workflow.xml using shot=12345, run=1 to create run=13 via a cocos transform to cocos=2 and  chease with cocos_in=2
