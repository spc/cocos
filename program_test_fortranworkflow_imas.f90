program test_fortranworkflow_imas

  use ids_schemas                       ! module containing the equilibrium type definitions
  use ids_routines
  use ids_utilities

  IMPLICIT NONE

  interface
     subroutine COCOStransform(equilibrium_in, equilibrium_out, COCOS_in, COCOS_out, IPsign_out, B0sign_out)
       !
       use ids_schemas
       ! use equilibrium_ids_module
       use ids_routines
       type (ids_equilibrium)  ::  equilibrium_in ! should not use intent(in) or out with pointers
       type (ids_equilibrium)  ::  equilibrium_out
       integer, optional, intent(IN) :: COCOS_in
       integer, optional, intent(IN) :: COCOS_out
       integer, optional, intent(IN) :: IPsign_out
       integer, optional, intent(IN) :: B0sign_out
     end subroutine COCOStransform
  end interface

  type(ids_equilibrium)  :: equil_in_ids, equil_out_ids, equil_out2_ids
  type (ids_Parameters_Input) :: param_input_ids
  character(len = 132), allocatable :: parameters(:)
  target :: parameters

  integer :: shot_in=9992, shot_out=9992, run_in=99, run_out=999, idx, equil_in_ids_ids_properties_cocos
  character*5 :: idstree
  character*11  :: signal_name ='equilibrium'

  idstree = 'ids'
  call imas_open(idstree,shot_in,run_in,idx)
  call ids_get(idx,"equilibrium",equil_in_ids)
  call imas_close(idx)
  ! print *,'equil_in_ids%ids_properties%cocos= ',equil_in_ids%ids_properties%cocos
  print *,'size(equil_in_ids%time)= ',size(equil_in_ids%time)
  print *,'size(equil_in_ids%time_slice%time)= ',size(equil_in_ids%time_slice%time)
  print *,'size(equil_in_ids%code%name)= ',size(equil_in_ids%code%name)
  print *,'equil_in_ids%code%name(1)= ',equil_in_ids%code%name(1)
  print *,'size(equil_in_ids%code%version)= ',size(equil_in_ids%code%version)
  print *,'equil_in_ids%code%version(1)= ',equil_in_ids%code%version(1)
  print *,'equil_in_ids%time_slice(1)%coordinate_system%grid_type%name= ',equil_in_ids%time_slice(1)%coordinate_system%grid_type%name
  print *,'equil_in_ids%time_slice(1)%coordinate_system%grid_type%description= ',equil_in_ids%time_slice(1)%coordinate_system%grid_type%description
  ! cocos not in ids anymore thus has to assume ITER cocos (11)
  equil_in_ids_ids_properties_cocos = 11
  call COCOStransform(equil_in_ids,equil_out_ids,equil_in_ids_ids_properties_cocos,2)
  call COCOStransform(equil_out_ids,equil_out2_ids,2,17)
  call COCOStransform(equil_out2_ids,equil_out_ids,17,11)
  print *,'equil_in_ids_ids_properties_cocos= ',equil_in_ids_ids_properties_cocos
  print *,'equil_out_ids%ids_properties%homogeneous_time= ',equil_out_ids%ids_properties%homogeneous_time
  print *,'equil_out_ids%time= ',equil_out_ids%time
  print *,'equil_out_ids%time_slice(1)%time= ',equil_out_ids%time_slice(1)%time
  print *,'equil_out_ids%code%name= ',equil_out_ids%code%name(1)
  print *,'equil_out_ids%code%version= ',equil_out_ids%code%version(1)

  call imas_create(idstree,shot_out,run_out,shot_out,0,idx)
  call ids_put(idx,"Equilibrium",equil_out_ids)
  call imas_close(idx)

end program test_fortranworkflow_imas
