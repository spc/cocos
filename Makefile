LIBNAME1= cocos_transform
LIBNAME1_nonITM= cocos_transform_nonITM
PROG_test_ITM= testforworkflow_itm

LIBNAME1_IMAS= cocos_transform_imas
PROG_test_IMAS= testforworkflow_imas
PROG_test_IMAS_copy= testequilcopy_imas

SRCS = cocos_transform.f90
SRCS_test_ITM = program_test_fortranworkflow_itm.f90
SRCS_IMAS = cocos_transform_imas.f90
SRCS_test_IMAS = program_test_fortranworkflow_imas.f90
SRCS_test_IMAS_copy = program_test_equilcopy.f90

MODS = itm_types.f90 cocos_module.f90
MODS_nonITM = itm_types.f90 cocos_module.f90 copy_structures.f90
MODS_test_ITM = 
MODS_IMAS = itm_types.f90 cocos_module.f90
MODS_test_IMAS = 
MODS_test_IMAS_copy = 

OBJS_LIBS = $(MODS:.f90=.o) $(SRCS:.f90=.o)
OBJS_LIBS_nonITM = $(MODS_nonITM:.f90=.o) $(SRCS:.f90=.o)
OBJS_LIBS_test_ITM = $(MODS_test_ITM:.f90=.o) $(SRCS_test_ITM:.f90=.o)
OBJS_LIBS_IMAS = $(MODS_IMAS:.f90=.o) $(SRCS_IMAS:.f90=.o)
OBJS_LIBS_test_IMAS = $(MODS_test_IMAS:.f90=.o) $(SRCS_test_IMAS:.f90=.o)
OBJS_LIBS_test_IMAS_copy = $(MODS_test_IMAS_copy:.f90=.o) $(SRCS_test_IMAS_copy:.f90=.o)

.PRECIOUS:	$(SRCS) $(MODS) $(MODS_nonITM) $(SRCS_IMAS) $(MODS_IMAS)

include Makefile.define_FLAGS

all: $(LIBNAME1)

clean:
	rm -f *.o *.mod  *.a

.SUFFIXES: $(SUFFIXES) .f90

.f90.o:
	$(F90) $(F90FLAGS) -c $<

.f90.mod:
	@touch $*.mod

.mod.o:
	$(F90) -c $(F90FLAGS) $*.f90

$(LIBNAME1): $(OBJS_LIBS)
	@mkdir -p $(LOCAL_LIB_DIR)
	ar -rv $@.a $(OBJS_LIBS)
	mv $@.a $(LOCAL_LIB_DIR)/lib$@.a
	@echo "now can compile with: $(F90) -o file_a_out $(F90FLAGS) file.f90 -L$(PWD)/$(LOCAL_LIB_DIR) -l$@"

$(LIBNAME1_nonITM): $(OBJS_LIBS_nonITM)
	ar -rv $@.a $(OBJS_LIBS_nonITM)
	mv $@.a lib$@.a
	@echo "now can compile with: $(F90) -o file_a_out file.f90 -L$(PWD) -l$@"

$(LIBNAME1_IMAS): $(OBJS_LIBS_IMAS)
	@mkdir -p $(LOCAL_LIB_DIR)
	ar -rv lib$@.a $(OBJS_LIBS_IMAS)
	mv lib$@.a $(LOCAL_LIB_DIR)/lib$(LIBNAME1).a
	@echo "now can compile with: $(F90) -o file_a_out $(F90FLAGS) file.f90 -L$(PWD)/$(LOCAL_LIB_DIR) -l$(LIBNAME1)"

$(PROG_test_ITM): $(OBJS_LIBS_test_ITM)
	$(F90) -o $@.exe $(LDFLAGS) $(LIBS) $(OBJS_LIBS_test_ITM) -L$(PWD)/$(LOCAL_LIB_DIR) -l$(LIBNAME1) $(LIBSend)

$(PROG_test_IMAS): $(OBJS_LIBS_test_IMAS)
	$(F90) -o $@.exe $(LDFLAGS) $(LIBS) $(OBJS_LIBS_test_IMAS) -L$(PWD)/$(LOCAL_LIB_DIR) -l$(LIBNAME1) $(LIBSend) 

$(PROG_test_IMAS_copy): $(OBJS_LIBS_test_IMAS_copy)
	$(F90) -o $@.exe $(LDFLAGS) $(LIBS) $(OBJS_LIBS_test_IMAS_copy) -L$(PWD)/$(LOCAL_LIB_DIR) -l$(LIBNAME1) $(LIBSend)

cocos_module.o: itm_types.o
	$(F90) $(F90FLAGS) -c cocos_module.f90
	@mkdir -p $(LOCAL_LIB_DIR)
	mv cocos_module.mod $(LOCAL_LIB_DIR)

cocos_transform.o:	itm_types.o cocos_module.o
copy_structures.o:	itm_types.o
cocos_transform_imas.o:	itm_types.o cocos_module.o
program_test_fortranworkflow_itm.o:	$(LIBNAME1)
program_test_fortranworkflow_imas.o:	$(LIBNAME1_IMAS)
