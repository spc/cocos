program test_equilcopy

  use ids_schemas                       ! module containing the equilibrium type definitions
  use ids_routines
  use ids_utilities

  IMPLICIT NONE

  type(ids_equilibrium)  :: equil_in_ids, equil_out_ids

  integer :: shot_in=9992, shot_out=9992, run_in=999, run_out=9991, idx
  character*5 :: idstree

  idstree = 'ids'
  call imas_open(idstree,shot_in,run_in,idx)
  call ids_get(idx,"equilibrium",equil_in_ids)
  call imas_close(idx)
  ! print *,'equil_in_ids%ids_properties%cocos= ',equil_in_ids%ids_properties%cocos
  call ids_copy(equil_in_ids,equil_out_ids)  
  print *,'equil_out_ids%ids_properties%comment= ',equil_out_ids%ids_properties%comment

end program test_equilcopy
