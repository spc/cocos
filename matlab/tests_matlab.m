function tests_matlab(test_case)
try
   fprintf('\n Running test file: %s\n',mfilename('fullpath'));
   fprintf('     Time: %s\n',datestr(now));

   
   matver = ver('matlab');
   if str2double(matver.Version)>=8.5
     passed = run_cocos_tests(test_case); % call to your test script here, with optional test_case input
     exit_code = int32(~passed); % convert to bash shell convention
   else
     warning('skipping tests since this matlab version is not fully compatible with the test framework');
     exit_code = int32(0);
   end
catch ME
   disp(getReport(ME))
   exit_code = 1;
end
exit(exit_code);
