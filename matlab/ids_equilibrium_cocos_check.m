function [ids_equilibrium_out,cocos_struct,stat_struct]=ids_equilibrium_cocos_check(ids_equilibrium_in,cocos_in,ipsign_in,b0sign_in,varargin);
%
% [ids_equilibrium_out,cocos_struct,stat_struct]=ids_equilibrium_cocos_check(ids_equilibrium_in,cocos_in,ipsign_in,b0sign_in,varargin);
%
% Required inputs:
% ids_equilibrium_in: input ids equilibrium, mandatory
% cocos_in: cocos to check ids_in with
%
% Optional inputs:
% ipsign_in: sign(ip) of ids_in if not available within ids_in or to use for the check
% b0sign_in: sign(b0) of ids_in if not available within ids_in or to use for the check
%

try
  ids_equilibrium_out = ids_gen('equilibrium');
catch
  ids_equilibrium_out = ids_equilibrium_in;
end
cocos_struct = struct([]);
stat_struct.global_status = 0;

if nargin<2
  disp('the first 2 inputs are mandatory')
  return
end
if isempty(ids_equilibrium_in) || ~isstruct(ids_equilibrium_in)
  disp('expects a equilibrium ids as 1st input')
  return
end
if isempty(cocos_in) || cocos_in<1 || cocos_in>18 || (cocos_in>8 & cocos_in<11)
  disp('cocos_in should be in [1,8] or [11,18]')
  return
end

if ~exist('ipsign_in') || isempty(ipsign_in)
  ipsign_in = 0;
end
if ipsign_in~=-1 && ipsign_in~=0 && ipsign_in~=+1
  disp('ipsign_in should be +1, -1, 0 or empty')
  return
end

if ~exist('b0sign_in') || isempty(b0sign_in)
  b0sign_in = 0;
end
if b0sign_in~=-1 && b0sign_in~=0 && b0sign_in~=+1
  disp('b0sign_in should be +1, -1, 0 or empty')
  return
end

ids_equilibrium_out = ids_equilibrium_in;

% assume sign of ip abd b0 same as 1st time point
itime=max(1,floor(numel(ids_equilibrium_out.time_slice)/2));
b0_in = ids_equilibrium_in.vacuum_toroidal_field.b0(itime);
ip_in = ids_equilibrium_in.time_slice{itime}.global_quantities.ip;
sigma_b0_in = sign(b0_in);
sigma_ip_in = sign(ip_in);
if isempty(b0_in)
  disp('ids_core_profiles_in.vacuum_toroidal_field.b0 is empty')
  if b0sign_in~=0
    sigma_b0_in = b0sign_in;
  else
    disp('assume B0 positive')
    sigma_b0_in = 1;
  end
end
if nargin>=4 && b0sign_in~=0
  sigma_b0_in = b0sign_in;
elseif b0sign_in == 0
  b0sign_in = sigma_b0_in;
end

if isempty(ip_in)
  disp('ids_core_profiles_in.global_quantities.ip is empty')
  if ipsign_in~=0
    sigma_ip_in = ipsign_in;
  else
    disp('assume Ip positive')
    sigma_ip_in = 1;
  end
end
if nargin>=4 && ipsign_in~=0
  sigma_ip_in = ipsign_in;
elseif ipsign_in == 0
  ipsign_in = sigma_ip_in;
end

[cocos_struct]=cocos(cocos_in);

% vacuum_toroidal_field
if ~isempty(ids_equilibrium_out.vacuum_toroidal_field.b0) && (sign(ids_equilibrium_out.vacuum_toroidal_field.b0(1))~=sigma_b0_in)
  stat_struct.global_status = stat_struct.global_status + 1;
  stat_struct.vacuum_toroidal_field.b0 = ['wrong sign, should be: ' num2str(sigma_b0_in)];
end

% time_slice: check 1st time_slice, assume rest is similar
% 1st may have 0 Ip, last as well, take middle point
%for i=1:length(ids_equilibrium_out.time_slice)
%
% boundary: nothing to do (assume "_norm" means normalized in absolute value sense)
%
% global_quantities:
main_field = 'global_quantities';
fields_check_ip_like={'ip'};
for i=1:length(fields_check_ip_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_ip_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(aa_tmp(1))~=sigma_ip_in)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_check_ip_like{i}) = ['wrong sign, should be: ' num2str(sigma_ip_in)];
  end
end
% psi like can only check "psi-like" - psi_axis versus expected "dpsi" sign, not psi as such
fields_check_psi_like={'psi_boundary'};
fields_structname_psi_like={'psi_boundary'};
psi_axis_ref = ids_equilibrium_out.time_slice{itime}.(main_field).psi_axis;
for i=1:length(fields_check_psi_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_psi_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(aa_tmp(1)-psi_axis_ref)~=-sigma_ip_in*cocos_struct.sign_pprime_pos)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_structname_psi_like{i}) = ['wrong sign with respect to psi_axis_ref=' num2str(psi_axis_ref) ', should be: ' num2str(-sigma_ip_in*cocos_struct.sign_pprime_pos)];
  end
end
% q like
fields_check_q_like={'q_axis', 'q_95', 'q_min.value'};
fields_structname_like={'q_axis', 'q_95', 'q_min_value'};
for i=1:length(fields_check_q_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_q_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(aa_tmp(1))~=sigma_ip_in*sigma_b0_in*cocos_struct.sign_q_pos)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_structname_like{i}) = ['wrong sign, should be: ' num2str(sigma_ip_in*sigma_b0_in*cocos_struct.sign_q_pos)];
  end
end
% b like
fields_check_b0_like={'magnetic_axis.b_tor', 'magnetic_axis.b_field_tor'};
fields_structname_like={'magnetic_axis_b_tor', 'magnetic_axis_b_field_tor'};
for i=1:length(fields_check_b0_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_b0_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(aa_tmp(1))~=sigma_b0_in)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_structname_like{i}) = ['wrong sign, should be: ' num2str(sigma_b0_in)];
  end
end

% for profiles might need to use (end) value or mean(value) of array
main_field = 'profiles_1d';
% psi like can only check "psi-like" - psi_axis versus expected "dpsi" sign, not psi as such
fields_check_psi_like={'psi'};
fields_structname_psi_like={'psi'};
psi_axis_ref = ids_equilibrium_out.time_slice{itime}.(main_field).psi(1);
for i=1:length(fields_check_psi_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_psi_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(aa_tmp(end)-psi_axis_ref)~=-sigma_ip_in*cocos_struct.sign_pprime_pos)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_structname_psi_like{i}) = ['wrong sign with respect to psi_axis_ref=' num2str(psi_axis_ref) ', should be: ' num2str(-sigma_ip_in*cocos_struct.sign_pprime_pos)];
  end
end
% dpsi like (just sign not absolute value)
fields_check_dodpsi_like={'dpressure_dpsi', 'dpsi_drho_tor', 'dvolume_dpsi', 'darea_dpsi'};
fields_check_dodpsi_like_extra_factor=[-1, +1, +1, +1];
for i=1:length(fields_check_dodpsi_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_dodpsi_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(mean(aa_tmp))~=-sigma_ip_in*cocos_struct.sign_pprime_pos.*fields_check_dodpsi_like_extra_factor(i))
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_check_dodpsi_like{i}) = ['wrong sign, should be: ' num2str(-sigma_ip_in*cocos_struct.sign_pprime_pos.*fields_check_dodpsi_like_extra_factor(i))];
  end
end
% b like
fields_check_b0_like={'phi', 'f'};
for i=1:length(fields_check_b0_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_b0_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(mean(aa_tmp))~=sigma_b0_in)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_check_b0_like{i}) = ['wrong sign, should be: ' num2str(sigma_b0_in)];
  end
end
% ip like
fields_check_ip_like={'j_tor', 'j_parallel'};
for i=1:length(fields_check_ip_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_ip_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(aa_tmp(1))~=sigma_ip_in)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_check_ip_like{i}) = ['wrong sign, should be: ' num2str(sigma_ip_in)];
  end
end
% q like
fields_check_q_like={'q'};
for i=1:length(fields_check_q_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_q_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(aa_tmp(1))~=sigma_ip_in*sigma_b0_in*cocos_struct.sign_q_pos)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_check_q_like{i}) = ['wrong sign, should be: ' num2str(sigma_ip_in*sigma_b0_in*cocos_struct.sign_q_pos)];
  end
end
% specific sign like
fields_check_specific_like={'b_average', 'b_min', 'b_max', 'b_field_average', 'b_field_min', 'b_field_max'};
fields_check_expected_sign=[+1        +1      +1         +1           +1             +1];
for i=1:length(fields_check_specific_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_specific_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(aa_tmp(1))~=fields_check_expected_sign(i))
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_check_specific_like{i}) = ['wrong sign, should be: ' num2str(fields_check_expected_sign(i))];
  end
end

% check of 2pi factor, do it on prfile_1d level only:
% Eq.(9) of COCOS paper: q = sigma_Bp*sigma_rhothetaphi / (2pi)^(1-exp_Bp) * dPhi/dpsi
% thus mean(q)~(phi(end)-phi(1))/(psi_end)-psi(1) / (2pi)^(1-exp_Bp)
mean_q_expBp = cocos_struct.sigma_rhothetaphi.*cocos_struct.sigma_Bp.*(ids_equilibrium_out.time_slice{itime}.profiles_1d.phi(end)-ids_equilibrium_out.time_slice{itime}.profiles_1d.phi(1)) ...
         / (ids_equilibrium_out.time_slice{itime}.profiles_1d.psi(end)-ids_equilibrium_out.time_slice{itime}.profiles_1d.psi(1)) ...
         / (2.*pi).^(1-cocos_struct.exp_Bp);
mean_q_other_expBp = cocos_struct.sigma_rhothetaphi.*cocos_struct.sigma_Bp.*(ids_equilibrium_out.time_slice{itime}.profiles_1d.phi(end)-ids_equilibrium_out.time_slice{itime}.profiles_1d.phi(1)) ...
    / (ids_equilibrium_out.time_slice{itime}.profiles_1d.psi(end)-ids_equilibrium_out.time_slice{itime}.profiles_1d.psi(1)) ...
    / (2.*pi).^(cocos_struct.exp_Bp);
q_mean = mean(ids_equilibrium_out.time_slice{itime}.profiles_1d.q);
if abs(mean_q_expBp-q_mean) > abs(mean_q_other_expBp-q_mean)
  % mean_q_expBp not closer to q_mean, thus probably 2pi wrong
  stat_struct.global_status = stat_struct.global_status + 1;
  stat_struct.(main_field).twopi_q_dphi_dpsi = ['wrong value of q average from 1/(2pi)^(1-expBp)*dphi/dpsi = ' ...
                    num2str(mean_q_expBp) ', should be within [' num2str(min(ids_equilibrium_out.time_slice{itime}.profiles_1d.q)) ',' ...
                   num2str(max(ids_equilibrium_out.time_slice{itime}.profiles_1d.q)) '], with other expBp q av = ' num2str(mean_q_other_expBp)];
end

% profiles_2d
main_field = 'profiles_2d';
for jprof2d=1:length(ids_equilibrium_out.time_slice{itime}.profiles_2d)
  grid_index = ids_equilibrium_out.time_slice{itime}.profiles_2d{jprof2d}.grid_type.index;
  if grid_index>=11 && grid_index<=19
    % psi like can only check "psi-like" - psi_axis versus expected "dpsi" sign, not psi as such
    fields_check_psi_like={'grid.dim1'};
    fields_structname_psi_like={'grid_dim1'};
    psi_axis_ref = ids_equilibrium_out.time_slice{itime}.global_quantities.psi_axis;
    for i=1:length(fields_check_psi_like)
      eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '{jprof2d}.' fields_check_psi_like{i} ';'])
      status_aa_tmp = ids_value_isvalid(aa_tmp);
      if status_aa_tmp==0 && (sign(aa_tmp(end)-psi_axis_ref)~=-sigma_ip_in*cocos_struct.sign_pprime_pos)
        stat_struct.global_status = stat_struct.global_status + 1;
        stat_struct.(main_field).(fields_structname_psi_like{i}) = ['wrong sign with respect to psi_axis_ref=' num2str(psi_axis_ref) ', should be: ' num2str(-sigma_ip_in*cocos_struct.sign_pprime_pos)];
      end
    end
  end
  % psi like can only check "psi-like" - psi_axis versus expected "dpsi" sign, not psi as such
  fields_check_psi_like={'psi'};
  fields_structname_psi_like={'psi'};
  psi_axis_ref = ids_equilibrium_out.time_slice{itime}.global_quantities.psi_axis;
  for i=1:length(fields_check_psi_like)
    eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '{jprof2d}.' fields_check_psi_like{i} ';'])
    status_aa_tmp = ids_value_isvalid(aa_tmp);
    if status_aa_tmp==0 && (sign(mean(aa_tmp(:))-psi_axis_ref)~=-sigma_ip_in*cocos_struct.sign_pprime_pos)
      stat_struct.global_status = stat_struct.global_status + 1;
      stat_struct.(main_field).(fields_structname_psi_like{i}) = ['wrong sign with respect to psi_axis_ref=' num2str(psi_axis_ref) ', should be: ' num2str(-sigma_ip_in*cocos_struct.sign_pprime_pos)];
    end
  end
  % dim2 is always angle-like if >1 and <90 (from table by O. Sauter)
  % however angle always from 0 to +2pi, it is the direction of positive theta which changes with cocos
  if grid_index>=2 && grid_index<=89
    % theta like
    fields_check_psi_like={'grid.dim2'};
    fields_structname_psi_like={'grid_dim2'};
    for i=1:length(fields_check_psi_like)
      eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '{jprof2d}.' fields_check_psi_like{i} ';'])
      status_aa_tmp = ids_value_isvalid(aa_tmp);
      if status_aa_tmp==0 && (sign(aa_tmp(2)-aa_tmp(1))~=-cocos_struct.sign_theta_clockwise)
        stat_struct.global_status = stat_struct.global_status + 1;
        stat_struct.(main_field).(fields_structname_psi_like{i}) = ['wrong sign delta value, should be: ' num2str(-cocos_struct.sign_theta_clockwise)];
      end
    end
  end
  % theta like
  fields_check_psi_like={'theta'};
  fields_structname_psi_like={'theta'};
  for i=1:length(fields_check_psi_like)
    eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '{jprof2d}.' fields_check_psi_like{i} ';'])
    status_aa_tmp = ids_value_isvalid(aa_tmp);
    if status_aa_tmp==0 && sign(diff(aa_tmp(end,end-1:end)))~=-cocos_struct.sign_theta_clockwise
      stat_struct.global_status = stat_struct.global_status + 1;
      stat_struct.(main_field).(fields_structname_psi_like{i}) = ...
          ['wrong sign delta value, should be: ' num2str(-cocos_struct.sign_theta_clockwise)];
    end
  end
  % b like
  fields_check_b0_like={'phi', 'b_tor', 'b_field_tor'};
  for i=1:length(fields_check_b0_like)
    eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '{jprof2d}.' fields_check_b0_like{i} ';'])
    status_aa_tmp = ids_value_isvalid(aa_tmp);
    if status_aa_tmp==0 && (sign(mean(aa_tmp(:)))~=sigma_b0_in) && any(aa_tmp(:))
      stat_struct.global_status = stat_struct.global_status + 1;
      stat_struct.(main_field).(fields_check_b0_like{i}) = ['wrong sign, should be: ' num2str(sigma_b0_in)];
    end
  end
  % ip like
  fields_check_ip_like={'j_tor', 'j_parallel'};
  for i=1:length(fields_check_ip_like)
    eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '{jprof2d}.' fields_check_ip_like{i} ';'])
    status_aa_tmp = ids_value_isvalid(aa_tmp);
    if status_aa_tmp==0 && (sign(mean(aa_tmp(:)))~=sigma_ip_in) && any(aa_tmp(:))
      stat_struct.global_status = stat_struct.global_status + 1;
        stat_struct.(main_field).(fields_check_ip_like{i}) = ['wrong sign, should be: ' num2str(sigma_ip_in)];
    end
  end
end
% coordinate_system
main_field = 'coordinate_system';
grid_index = ids_equilibrium_out.time_slice{itime}.coordinate_system.grid_type.index;
fact_dim_eff(1) = 1.;
if grid_index>=11 && grid_index<=19
  % psi like can only check "psi-like" - psi_axis versus expected "dpsi" sign, not psi as such
  fields_check_psi_like={'grid.dim1'};
  fields_structname_psi_like={'grid_dim1'};
  psi_axis_ref = ids_equilibrium_out.time_slice{itime}.global_quantities.psi_axis;
  fact_dim_eff(1) = -sigma_ip_in*cocos_struct.sign_pprime_pos;
  for i=1:length(fields_check_psi_like)
    eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_psi_like{i} ';'])
    status_aa_tmp = ids_value_isvalid(aa_tmp);
    if status_aa_tmp==0 && (sign(aa_tmp(end)-psi_axis_ref)~=fact_dim_eff(1))
      stat_struct.global_status = stat_struct.global_status + 1;
      stat_struct.(main_field).(fields_structname_psi_like{i}) = ['wrong sign with respect to psi_axis_ref=' num2str(psi_axis_ref) ', should be: ' num2str(fact_dim_eff(1))];
    end
  end
end
fact_dim_eff(2) = 1.;
if grid_index>=2 && grid_index<=89
  % theta like
  fields_check_psi_like={'grid.dim2'};
  fields_structname_psi_like={'grid_dim2'};
  fact_dim_eff(2) = -cocos_struct.sign_theta_clockwise;
  for i=1:length(fields_check_psi_like)
    eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_psi_like{i} ';'])
    status_aa_tmp = ids_value_isvalid(aa_tmp);
    if status_aa_tmp==0 && (sign(aa_tmp(2)-aa_tmp(1))~=fact_dim_eff(2))
      stat_struct.global_status = stat_struct.global_status + 1;
      stat_struct.(main_field).(fields_structname_psi_like{i}) = ['wrong sign delta value, should be: ' num2str(fact_dim_eff(2))];
    end
  end
end
% specific sign like
fields_check_specific_like={'jacobian', 'g11_contravariant', 'g22_contravariant', 'g33_contravariant', 'g11_covariant', 'g22_covariant', 'g33_covariant'};
fields_check_expected_sign=[+1        ,         +1,              +1,                   +1               +1               +1                +1];
for i=1:length(fields_check_specific_like)
  eval(['aa_tmp = ids_equilibrium_out.time_slice{itime}.' main_field '.' fields_check_specific_like{i} ';'])
  status_aa_tmp = ids_value_isvalid(aa_tmp);
  if status_aa_tmp==0 && (sign(mean(aa_tmp(:)))~=fields_check_expected_sign(i))
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_check_specific_like{i}) = ['wrong sign, should be: ' num2str(fields_check_expected_sign(i))];
  end
end
