function [passed,results] = run_cocos_tests(test_case)
% Template test runner for toolbox tests

% F. Felici, EPFL federico.felici@epfl.ch

if nargin==0 || isempty(test_case)
  test_case = 'basic'; % default
end
test_case = lower(test_case);

%% Import some classes we need
import matlab.unittest.selectors.HasTag;
import matlab.unittest.constraints.ContainsSubstring;
import matlab.unittest.selectors.HasName;

%% populate suite
% add path with tests
addpath(genpath(fullfile(fileparts(mfilename('fullpath')),'tests')));

suite_all = matlab.unittest.TestSuite.fromClass(?tests_cocos);

switch test_case
  case 'all'
    suite = suite_all; % run all
  case 'basic'
    s = ~HasTag('slow');
    suite = suite_all.selectIf(s);
  otherwise
    error('unknown test_case %s',test_case)
end

%% run it
fprintf('Start test case: %s\n%s\n\n',test_case,datestr(now));
results = run(suite);
disp(table(results));
fprintf('\nTotal test duration: %5.2fs\n',sum(table(results).Duration))

if all([results.Passed])
  fprintf('\nPassed all tests\n')
  passed = true;
elseif any([results.Failed])
  fprintf('\nSome tests Failed\n')
  disp(table(results([results.Failed])))
  passed = false;
elseif any([results.Incomplete])
  fprintf('\nSome tests Incomplete\n')
  disp(table(results([results.Incomplete])));
  passed = true; % pass tests even if some were skipped
else
  % the conditions above should cover all cases, otherwise
  error('something is very wrong - please check')
end

end
