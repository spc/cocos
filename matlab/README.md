# testproject

Test project to
* Test testing procedure and deployments
* Contain template files for starting new toolboxes

In ./matlab subfolder:

./test_script matlab basic # tests basic tests

# Standard usage
* Now any ids can be transformed using the generic call:

  [ids_structure,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_structure,'ids_name',cocos_in,cocos_out,ipsign_out,b0sign_out,ipsign_in,b0sign_in,error_bar,nverbose);

# Most recent test on 3.39 version (at ITER-IO), working with a call like:
~sautero/public/matlab/cocos_matlab/ids_check_cocos equilibrium sautero test  3 98 9999 0 3 1 1 11
(as shell script)
or
\>> ss=ids_check_cocos('equilibrium','sautero','test','3',97,9999,0,3,-1,-1,11)
(in matlab. Returning 0 if ok or list of nodes with problems (with nverbose=3))

# Note on 3.39 files in $IMAS_PREFIX/share/doc/imas/cocos:
       grid_type_transformations_symbolic_table.csv  ids_cocos_transformations_symbolic_table.csv
 the grid file has "phi_like" while the ids_...symbolic table contains only tor_angle_like cases, so I had to
* either change phi_like as tor_angle_like in grid file (now done in git 2023.11.23 version)
* or add a dummy_def with phi_like in ids.. file (now commented as example in ids...3.39 version)
