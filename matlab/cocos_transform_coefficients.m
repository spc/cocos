function [cocoscoeff,cocoscoeffsym]=cocos_transform_coefficients(cocos_in, cocos_out, label_transformation, transformation_expression, ip_in, b0_in, ipsign_out, b0sign_out);
%
% [cocoscoeff,cocoscoeffsym]=cocos_transform_coefficients(cocos_in,cocos_out,label_transformation,transformation_expression, ...
%                                                       ip_in,b0_in,ipsign_out,b0sign_out);
%
% (follows subroutine cocos_values_coefficients in cocos_module.f90)
%
% cocoscoeffsym is the symbolic expressions to compute the coefficients, in order to keep 2pi's in particular up to evaluation and not lose digits
%           (at this stage only fact_psi and fact_dodpsi are related, thus evaluate all others directly)
%
% computes the coefficients/factors for transforming various relevant quantities like psi, r*bphi, q, toroidal direction, etc
% from cocos_in to cocos_out, given ip_in/b0_in with/without a desired sign of ip/b0 in the resulting cocos_out coordinate convention
%
% Inputs:
%  cocos_in: coordinate convention of input data (0 or empty if not known, then output sign of ip and b0 are required)
%  cocos_out: coordinate convention of output data
%  label_transformation: additional fields to compute its transformation
%  transformation_expression: transformation expression to compute the additional label_transformation:
%              eval(['cocoscoeff.(label_transformation{i}) = ' transformation_expression{i} ';'])
%
% Optional inputs:
%  ip_in: input plasma current (value or sign), just the sign is used (default=+1)
%  b0_in: input toroidal magnetic field (value or sign), just the sign is used (default=+1)
%  ipsign_out: specific sign of Ip in output if desired, otherwise follows coordinate transformations from ip_in
%  b0sign_out: specific sign of B0 in output if desired, otherwise follows coordinate transformations from b0_in
%
% output structure cocoscoeff. :
%  .sigma_ip_eff:            Value with which to multiply ip_in such as to obtain consistent ip_out
%  .sigma_b0_eff:            Value with which to multiply b0_in such as to obtain consistent b0_out
%  .sigma_bp_eff:            +-1 tells if there is a change in sign of psi convention in between the 2 systems
%  .sigma_rhothetaphi_eff:   +-1 tells if there is a change in direction of rhothetaphi in between the 2 systems
%  .sigma_rphiz_eff:         +-1 tells if there is a change in direction of phi in between the 2 systems
%  .exp_bp_eff:              0, +1, -1: exponent of 2pi used when transforming psi related quantities
%     factors to use to transform psi, q, delta_psi (like d/dpsi quantities) and theta direction
%  .fact_psi:                = sigma_ip_eff * sigma_bp_eff * twopi.^exp_bp_eff
%  .fact_dodpsi:             = sigma_ip_eff * sigma_bp_eff / twopi.^exp_bp_eff (factor for d/dpsi terms) (=1/fact_psi)
%  .fact_q:                  = sigma_ip_eff * sigma_b0_eff * sigma_rhothetaphi_eff
%  .fact_dtheta:             = sigma_rphiz_eff * sigma_rhothetaphi_eff
%
% If cocos_in is not given in a vlid way, then many transformation coefficients will be empty, mainly the sign of Ip nd b0 can be used
%

% default structure
cocoscoeff.cocos_in = [];
cocoscoeff.cocos_out = [];
cocoscoeff.ip_in= [];
cocoscoeff.b0_in = [];
cocoscoeff.sigma_ip_in = [];
cocoscoeff.sigma_b0_in = [];
cocoscoeff.ipsign_out = [];
cocoscoeff.b0sign_out = [];
cocoscoeff.sigma_ip_eff = [];
cocoscoeff.sigma_b0_eff = [];
cocoscoeff.sigma_bp_eff = [];
cocoscoeff.sigma_rhothetaphi_eff = [];
cocoscoeff.sigma_rphiz_eff = [];
cocoscoeff.exp_bp_eff = [];
cocoscoeff.fact_psi = [];
cocoscoeff.fact_q = [];
cocoscoeff.fact_dodpsi = [];
cocoscoeff.fact_dtheta = [];
field_to_copy = fieldnames(cocoscoeff);
for i=1:length(field_to_copy)
  cocoscoeffsym.(field_to_copy{i}) = '';
end

% check inputs
if nargin < 2
  error(['requires at least 2 inputs, nargin = ', num2str(nargin)]);
  return
end

if isempty(cocos_in) || cocos_in<1 || cocos_in>18 || (cocos_in>8 & cocos_in<11)
  if ~isempty(cocos_in) && cocos_in~=0; warning(['cocos_in = ' num2str(cocos_in) ' not valid thus set to 0']); end
  cocos_in = 0;
end
cocoscoeff.cocos_in = cocos_in;
if isempty(cocos_out) || cocos_out<1 || cocos_out>18 || (cocos_out>8 & cocos_out<11)
  disp('cocos_out should be in [1,8] or [11,18]')
  return
end
if ~exist('ip_in') || isempty(ip_in)
  ip_in =+1;
end
if ~exist('b0_in') || isempty(b0_in)
  b0_in =+1;
end
cocoscoeff.cocos_out = cocos_out;
cocoscoeff.ip_in= ip_in;
cocoscoeff.b0_in = b0_in;
cocoscoeff.sigma_ip_in = sign(cocoscoeff.ip_in);
cocoscoeff.sigma_b0_in = sign(cocoscoeff.b0_in);

if cocos_in == 0
  % in this case we enforce the output to be consistent with cocos_out only and to match the ipsign_out and b0sign_out
  % thus ipsign_out and b0sign_out are required
  if nargin < 6 || (isempty(ipsign_out) | ipsign_out==0) || (isempty(b0sign_out) | b0sign_out==0)
    error(['requires at least 6 inputs if cocos_in not provided or valid, nargin = ', num2str(nargin)]);
    return
  end
end

if ~exist('ipsign_out') || isempty(ipsign_out)
  ipsign_out = 0;
end
if ipsign_out~=-1 && ipsign_out~=0 && ipsign_out~=+1
  disp('ipsign_out should be +1, -1, 0 or empty')
  return
end
if ~exist('b0sign_out') || isempty(b0sign_out)
  b0sign_out = 0;
end
if b0sign_out~=-1 && b0sign_out~=0 && b0sign_out~=+1
  disp('b0sign_out should be +1, -1, 0 or empty')
  return
end
cocoscoeff.ipsign_out = ipsign_out;
cocoscoeff.b0sign_out = b0sign_out;

cocos_in_struct=cocos(cocos_in);
cocos_out_struct=cocos(cocos_out);

% define effective variables: sigma_ip_eff, si1gma_b0_eff, sigma_bp_eff, exp_bp_eff as in appendix c
% if cocos_in not defined set to empty to make sure it's not used by mistake
if ~isempty(cocos_in_struct.sigma_RphiZ)
  cocoscoeff.sigma_rphiz_eff  = cocos_out_struct.sigma_RphiZ * cocos_in_struct.sigma_RphiZ;
else
  cocoscoeff.sigma_rphiz_eff  = [];
end
%
% sign(ip) in output:
if ~isempty(cocoscoeff.sigma_rphiz_eff)
  if ipsign_out == 0
    % sign folllowing transformation
    cocoscoeff.sigma_ip_eff = cocoscoeff.sigma_rphiz_eff;
  else
    cocoscoeff.sigma_ip_eff = cocoscoeff.sigma_ip_in * cocoscoeff.ipsign_out;
  end
else
  cocoscoeff.sigma_ip_eff = cocoscoeff.sigma_ip_in * cocoscoeff.ipsign_out;
end
cocoscoeff.sigma_ip_out = cocoscoeff.sigma_ip_in * cocoscoeff.sigma_ip_eff;
%
% sign(b0) in output:
if ~isempty(cocoscoeff.sigma_rphiz_eff)
  if b0sign_out == 0
    % sign folllowing transformation
    cocoscoeff.sigma_b0_eff = cocoscoeff.sigma_rphiz_eff;
  else
    cocoscoeff.sigma_b0_eff = cocoscoeff.sigma_b0_in * cocoscoeff.b0sign_out;
  end
else
  cocoscoeff.sigma_b0_eff = cocoscoeff.sigma_b0_in * cocoscoeff.b0sign_out;
end
cocoscoeff.sigma_b0_out = cocoscoeff.sigma_b0_in * cocoscoeff.sigma_b0_eff;
%
if ~isempty(cocos_in_struct.sigma_Bp)
  cocoscoeff.sigma_bp_eff = cocos_out_struct.sigma_Bp * cocos_in_struct.sigma_Bp;
else
  cocoscoeff.sigma_bp_eff = [];
end
if ~isempty(cocos_in_struct.exp_Bp)
  cocoscoeff.exp_bp_eff = cocos_out_struct.exp_Bp - cocos_in_struct.exp_Bp;
else
  cocoscoeff.exp_bp_eff = [];
end
if ~isempty(cocos_in_struct.sigma_rhothetaphi)
  cocoscoeff.sigma_rhothetaphi_eff  = cocos_out_struct.sigma_rhothetaphi * cocos_in_struct.sigma_rhothetaphi;
else
  cocoscoeff.sigma_rhothetaphi_eff  = [];
end
%
% Note that sign(sigma_RphiZ*sigma_rhothetaphi) gives theta in clockwise or counter-clockwise respectively
% Thus sigma_RphiZ_eff*sigma_rhothetaphi_eff negative if the direction of theta has changed from cocos_in to _out
%
twopi = 2.* pi;
cocoscoeff.fact_psi = cocoscoeff.sigma_ip_eff * cocoscoeff.sigma_bp_eff * twopi.^cocoscoeff.exp_bp_eff;
cocoscoeff.fact_dodpsi  = cocoscoeff.sigma_ip_eff * cocoscoeff.sigma_bp_eff / twopi.^cocoscoeff.exp_bp_eff;
cocoscoeff.fact_q = cocoscoeff.sigma_ip_eff * cocoscoeff.sigma_b0_eff * cocoscoeff.sigma_rhothetaphi_eff;
cocoscoeff.fact_dtheta  = cocoscoeff.sigma_rphiz_eff * cocoscoeff.sigma_rhothetaphi_eff;

% construct cocoscoeffsym
field_to_copy = fieldnames(rmfield(cocoscoeff,{'fact_psi','fact_dodpsi'}));
for i=1:length(field_to_copy)
  cocoscoeffsym.(field_to_copy{i}) = num2str(cocoscoeff.(field_to_copy{i}));
end
cocoscoeffsym.fact_psi = [num2str(cocoscoeff.sigma_ip_eff * cocoscoeff.sigma_bp_eff) '*(2*pi).^' num2str(cocoscoeff.exp_bp_eff)];
cocoscoeffsym.fact_dodpsi = [num2str(cocoscoeff.sigma_ip_eff * cocoscoeff.sigma_bp_eff) '/(2*pi).^' num2str(cocoscoeff.exp_bp_eff)];

[apath]=fileparts(which('cocos_transform_coefficients'));
grid_type_table_filename = fullfile(apath,'grid_type_transformations_symbolic_table.csv');
if exist(grid_type_table_filename,'file')
  [grid_type_index_cell,grid_type_name,gt_label,gt_label_transformation_dim1,gt_label_transformation_dim2,gt_label_transformation_dim3]=textread(grid_type_table_filename,'%s%s%s%s%s%s','delimiter',';','headerlines',1);
else
  warning(['table of transformation not available in file: ' grid_type_table_filename])
  return
end

if exist('label_transformation') && exist('transformation_expression') && ~isempty(label_transformation) && length(label_transformation)==length(transformation_expression)
  % first define basic transformations
  for i=1:length(label_transformation)
    if length(label_transformation{i}) < 13 || ~strcmp(label_transformation{i}(1:10),'grid_type_')
      if any(findstr(transformation_expression{i},'.'))
        transformation_expression_eff = strrep(transformation_expression{i},'.','cocoscoeffsym.');
      else
        transformation_expression_eff = transformation_expression{i};
      end
      cocoscoeffsym.(label_transformation{i}) = eval(transformation_expression_eff);
    end
  end

  % then define grid_type using basics
  for i=1:length(label_transformation)
    if length(label_transformation{i}) >= 13 && strcmp(label_transformation{i}(1:10),'grid_type_')
      switch label_transformation{i}
       case 'grid_type_dim1_like'
        for i_gt=1:length(grid_type_index_cell)
          cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]) = cocoscoeffsym.(gt_label_transformation_dim1{i_gt});
        end
       case 'grid_type_dim2_like'
        for i_gt=1:length(grid_type_index_cell)
          cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]) = cocoscoeffsym.(gt_label_transformation_dim2{i_gt});
        end
       case 'grid_type_dim3_like'
        for i_gt=1:length(grid_type_index_cell)
          cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]) = cocoscoeffsym.(gt_label_transformation_dim3{i_gt});
        end
       case 'grid_type_dim1_dim1_like'
        for i_gt=1:length(grid_type_index_cell)
          cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]) = ...
              [cocoscoeffsym.(gt_label_transformation_dim1{i_gt}) '*' cocoscoeffsym.(gt_label_transformation_dim1{i_gt})];
        end
       case 'grid_type_dim1_dim2_like'
        for i_gt=1:length(grid_type_index_cell)
          cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]) = ...
              [cocoscoeffsym.(gt_label_transformation_dim1{i_gt}) '*' cocoscoeffsym.(gt_label_transformation_dim2{i_gt})];
        end
       case 'grid_type_dim1_dim3_like'
        for i_gt=1:length(grid_type_index_cell)
          cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]) = ...
              [cocoscoeffsym.(gt_label_transformation_dim1{i_gt}) '*' cocoscoeffsym.(gt_label_transformation_dim3{i_gt})];
        end
       case 'grid_type_dim2_dim2_like'
        for i_gt=1:length(grid_type_index_cell)
          cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]) = ...
              [cocoscoeffsym.(gt_label_transformation_dim2{i_gt}) '*' cocoscoeffsym.(gt_label_transformation_dim2{i_gt})];
        end
       case 'grid_type_dim2_dim3_like'
        for i_gt=1:length(grid_type_index_cell)
          cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]) = ...
              [cocoscoeffsym.(gt_label_transformation_dim2{i_gt}) '*' cocoscoeffsym.(gt_label_transformation_dim3{i_gt})];
        end
       case 'grid_type_dim3_dim3_like'
        for i_gt=1:length(grid_type_index_cell)
          cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]) = ...
              [cocoscoeffsym.(gt_label_transformation_dim3{i_gt}) '*' cocoscoeffsym.(gt_label_transformation_dim3{i_gt})];
        end
       case 'grid_type_tensor_contravariant_like'
        for il=1:3
          gt_label_transformation_dim_il = eval(['gt_label_transformation_dim' num2str(il)]);
          for jl=1:3
            gt_label_transformation_dim_jl = eval(['gt_label_transformation_dim' num2str(jl)]);
            for i_gt=1:length(grid_type_index_cell)
              try
                cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]){il,jl} = ...
                    [cocoscoeffsym.(gt_label_transformation_dim_il{i_gt}) '*' cocoscoeffsym.(gt_label_transformation_dim_jl{i_gt})];
              catch
                keyboard
              end
            end
          end
        end
       case 'grid_type_tensor_covariant_like'
        for il=1:3
          gt_label_transformation_dim_il = eval(['gt_label_transformation_dim' num2str(il)]);
          for jl=1:3
            gt_label_transformation_dim_jl = eval(['gt_label_transformation_dim' num2str(jl)]);
            for i_gt=1:length(grid_type_index_cell)
              try
                cocoscoeffsym.([label_transformation{i} '_' grid_type_index_cell{i_gt}]){il,jl} = ...
                    ['(' cocoscoeffsym.(gt_label_transformation_dim_il{i_gt}) ')^-1*(' cocoscoeffsym.(gt_label_transformation_dim_jl{i_gt}) ')^-1'];
              catch
                keyboard
              end
            end
          end
        end
       otherwise
        error([char(10) '**********************************' char(10) ...
               'label_transformation = ' label_transformation{i} ', unexpected ' ...
               char(10) '**********************************' char(10)])
      end
    end
  end
end


% $$$ cocoscoeffsym.ip_like = [num2str(cocoscoeff.sigma_ip_eff) '*'];
% $$$ cocoscoeffsym.b0_like = [num2str(cocoscoeff.sigma_b0_eff) '*'];
% $$$ cocoscoeffsym.phi_like = [num2str(cocoscoeff.sigma_rphiz_eff) '*'];
% $$$ cocoscoeffsym.q_like = [num2str(cocoscoeff.fact_q) '*'];
% $$$ cocoscoeffsym.psi_like = [num2str(cocoscoeff.fact_psi) '*'];
% $$$ cocoscoeffsym.bp_like = [num2str(cocoscoeff.fact_dtheta*cocoscoeff.sigma_rphiz_eff*cocoscoeff.sigma_ip_eff) '*'];
% $$$ cocoscoeffsym.dodpsi_like = [num2str(cocoscoeff.fact_dodpsi) '*'];
% $$$ cocoscoeffsym.pol_angle_like = [num2str(cocoscoeff.fact_dtheta) '*'];
% $$$ cocoscoeffsym.tor_angle_like = [num2str(cocoscoeff.sigma_rphiz_eff) '*'];
% $$$ cocoscoeffsym.one_like = ['1*'];
