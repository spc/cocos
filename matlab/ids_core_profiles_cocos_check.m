function [ids_core_profiles_out,cocos_struct,stat_struct]=ids_core_profiles_cocos_check(ids_core_profiles_in,cocos_in,ipsign_in,b0sign_in,varargin);
%
% [ids_core_profiles_out,cocos_struct,stat_struct]=ids_core_profiles_cocos_check(ids_core_profiles_in,cocos_in,ipsign_in,b0sign_in,varargin);
%
% Required inputs:
% ids_core_profiles_in: input ids core_profiles, mandatory
% cocos_in: cocos to check ids_in with
% 
% Optional inputs:
% ipsign_in: sign(ip) of ids_in if not available within ids_in or to use for the check
% b0sign_in: sign(b0) of ids_in if not available within ids_in or to use for the check
% 

ids_core_profiles_out = ids_gen('core_profiles');
cocos_struct = struct([]);
stat_struct.global_status = 0;

if nargin<2
  disp('the first 2 inputs are mandatory')
  return
end
if isempty(ids_core_profiles_in) || ~isstruct(ids_core_profiles_in)
  disp('expects a core_profiles ids as 1st input')
  return
end
if isempty(cocos_in) || cocos_in<1 || cocos_in>18 || (cocos_in>8 & cocos_in<11)
  disp('cocos_in should be in [1,8] or [11,18]')
  return
end

if ~exist('ipsign_in') || isempty(ipsign_in)
  ipsign_in = 0;
end
if ipsign_in~=-1 && ipsign_in~=0 && ipsign_in~=+1
  disp('ipsign_in should be +1, -1, 0 or empty')
  return
end

if ~exist('b0sign_in') || isempty(b0sign_in)
  b0sign_in = 0;
end
if b0sign_in~=-1 && b0sign_in~=0 && b0sign_in~=+1
  disp('b0sign_in should be +1, -1, 0 or empty')
  return
end

ids_core_profiles_out = ids_core_profiles_in;

% assume sign of ip abd b0 same as 1st time point
b0_in = ids_core_profiles_in.vacuum_toroidal_field.b0(1);
ip_in = ids_core_profiles_in.global_quantities.ip(1);
sigma_b0_in = sign(b0_in);
sigma_ip_in = sign(ip_in);
if isempty(b0_in) 
  disp('ids_core_profiles_in.vacuum_toroidal_field.b0 is empty')
  if b0sign_in~=0
    sigma_b0_in = b0sign_in;
  else
    disp('assume B0 positive')
    sigma_b0_in = 1;
  end
end
if nargin>=4 && b0sign_in~=0
  sigma_b0_in = b0sign_in;
end

if isempty(ip_in)
  disp('ids_core_profiles_in.global_quantities.ip is empty')
  if ipsign_in~=0
    sigma_ip_in = ipsign_in;
  else
    disp('assume Ip positive')
    sigma_ip_in = 1;
  end 
end
if nargin>=4 && ipsign_in~=0
  sigma_ip_in = ipsign_in;
end
  
[cocos_struct]=cocos(cocos_in);

% vacuum_toroidal_field
if ~isempty(ids_core_profiles_out.vacuum_toroidal_field.b0) && (sign(ids_core_profiles_out.vacuum_toroidal_field.b0(1))~=sigma_b0_in)
  stat_struct.global_status = stat_struct.global_status + 1;
  stat_struct.vacuum_toroidal_field.b0 = ['wrong sign, should be: ' num2str(sigma_b0_in)];
end

% global_quantities:
main_field = 'global_quantities';
fields_check_ip_like={'ip', 'current_non_inductive', 'current_bootstrap', 'v_loop'};
for i=1:length(fields_check_ip_like)
  eval(['aa_tmp = ids_core_profiles_out.' main_field '.' fields_check_ip_like{i} ';'])
  if ~isempty(aa_tmp) && (sign(aa_tmp(1))~=sigma_ip_in)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_check_ip_like{i}) = ['wrong sign, should be: ' num2str(sigma_ip_in)];
  end
end

% profiles_1d: check only 1st element
%for i=1:length(ids_core_profiles_out.profiles_1d)
iprof1d=1;
main_field = 'profiles_1d';
fields_check_ip_like={'j_total', 'current_parallel_inside', 'j_tor', 'j_ohmic', 'j_non_inductive', 'j_bootstrap'};
for i=1:length(fields_check_ip_like)
  eval(['aa_tmp = ids_core_profiles_out.' main_field '{iprof1d}.' fields_check_ip_like{i} ';'])
  if ~isempty(aa_tmp) && (sign(aa_tmp(1))~=sigma_ip_in)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_check_ip_like{i}) = ['wrong sign, should be: ' num2str(sigma_ip_in)];
  end
end
% psi like can only check "psi-like" - psi_axis versus expected "dpsi" sign, not psi as such
fields_check_psi_like={'grid.psi'};
fields_structname_psi_like={'grid_psi'};
psi_axis_ref = ids_core_profiles_out.profiles_1d{iprof1d}.grid.psi(1);
for i=1:length(fields_check_psi_like)
  eval(['aa_tmp = ids_core_profiles_out.' main_field '{iprof1d}.' fields_check_psi_like{i} ';'])
  if ~isempty(aa_tmp) && (sign(aa_tmp(1)-psi_axis_ref)~=-sigma_ip_in*cocos_struct.sign_pprime_pos)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_structname_psi_like{i}) = ['wrong sign with respect to psi_axis_ref=' num2str(psi_axis_ref) ', should be: ' num2str(-sigma_ip_in*cocos_struct.sign_pprime_pos)];
  end
end
% q like
fields_check_q_like={'q'};
for i=1:length(fields_check_q_like)
  eval(['aa_tmp = ids_core_profiles_out.' main_field '{iprof1d}.' fields_check_q_like{i} ';'])
  if ~isempty(aa_tmp) && (sign(aa_tmp(1))~=sigma_ip_in*sigma_b0_in*cocos_struct.sign_q_pos)
    stat_struct.global_status = stat_struct.global_status + 1;
    stat_struct.(main_field).(fields_check_q_like{i}) = ['wrong sign, should be: ' num2str(sigma_ip_in*sigma_b0_in*cocos_struct.sign_q_pos)];
  end
end

% check of 2pi factor, do it on prfile_1d level only:
% Eq.(9) of COCOS paper: q = sigma_Bp*sigma_rhothetaphi / (2pi)^(1-exp_Bp) * dPhi/dpsi
% thus mean(q)~(phi(end)-phi(1))/(psi_end)-psi(1) / (2pi)^(1-exp_Bp)
mean_q_expBp = (ids_core_profiles_out.profiles_1d{iprof1d}.grid.rho_tor(end).^2-ids_core_profiles_out.profiles_1d{iprof1d}.grid.rho_tor(1).^2) ...
         .* pi .* ids_core_profiles_out.vacuum_toroidal_field.b0(1) / (ids_core_profiles_out.profiles_1d{iprof1d}.grid.psi(end)-ids_core_profiles_out.profiles_1d{iprof1d}.grid.psi(1)) ...
         / (2.*pi).^(1-cocos_struct.exp_Bp);
mean_q_other_expBp = (ids_core_profiles_out.profiles_1d{iprof1d}.grid.rho_tor(end).^2-ids_core_profiles_out.profiles_1d{iprof1d}.grid.rho_tor(1).^2) ...
    .* pi .* ids_core_profiles_out.vacuum_toroidal_field.b0(1) / (ids_core_profiles_out.profiles_1d{iprof1d}.grid.psi(end)-ids_core_profiles_out.profiles_1d{iprof1d}.grid.psi(1)) ...
    / (2.*pi).^(cocos_struct.exp_Bp);
q_mean = mean(ids_core_profiles_out.profiles_1d{iprof1d}.q);
if abs(mean_q_expBp-q_mean) > abs(mean_q_other_expBp-q_mean)
  % mean_q_expBp not closer to q_mean, thus probably 2pi wrong
  stat_struct.global_status = stat_struct.global_status + 1;
  stat_struct.(main_field).twopi_q_dphi_dpsi = ['wrong value of q average from 1/(2pi)^(1-expBp)*dphi/dpsi = ' ...
                    num2str(mean_q_expBp) ', should be within [' num2str(min(ids_core_profiles_out.profiles_1d{iprof1d}.q)) ',' ...
                    num2str(max(ids_core_profiles_out.profiles_1d{iprof1d}.q)) '], with other expBp q av = ' num2str(mean_q_other_expBp)];
end

