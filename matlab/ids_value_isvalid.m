function [status]=ids_value_isvalid(ids_value,varargin);
%
% [status,invalid_kind]=ids_value_isvalid(ids_value,varargin);
%
% check if input ids_value is different from empty, -9.0000e+40 and -999999999
%
% Returned status:
%
% if ids_value is:
%    not provided: status = 9999
%    -9.e+40:    status = 1 (default float)
%    -999999999: status = 2 (default integer)
%    empty:      status = 3
%    isnan:      status = 4 (NaN)dbs
%    isinf:      status = 5 (Inf)
%    otherwise:  status = 0 (value is finite, cannot decide if float or integer at this stage)
% 
status = 9999;
if exist('ids_value')
  if isempty(ids_value)
    status = 3;
  elseif any(isnan(ids_value))
    status = 4;
  elseif any(isinf(ids_value))
    status = 5;
  elseif any(abs(ids_value+9.0e40) < 1.e-6)
    status = 1;
  elseif any(abs(ids_value+999999999) < 1.e-6)
    status = 2;
  else
    status = 0;
  end
end
