function [ids_out,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(ids_in,ids_type,cocos_in,cocos_out,ipsign_out,b0sign_out,ipsign_in,b0sign_in,varargin);
%
% [ids_out,cocoscoeff]=ids_generic_cocos_nodes_transformation(ids_in,ids_type,cocos_in,cocos_out,ipsign_out,b0sign_out,ipsign_in,b0sign_in,varargin);
%
% ids_out = ids_generic_cocos_transform; % returns the list of ids for which a cocos_transform is available
%
% Required inputs:
% ids_in: input ids, mandatory except if no args provided, returns a structure with the list of available ids types
% ids_type: name of ids like 'equilibrium', 'core_profiles', ...
% cocos_in: cocos of input ids, if not known set to 0 or empty and then provide cocos_out and ipsign_out and b0sign_out
% cocos_out: required cocos for ids_out. Needs to be a relevant option: 1-8 or 11-18
%
% Optional inputs:
% ipsign_out: desired sign of plasma current in ids_out (if a specific sign is aimed for)
%             0 (or empty or not provided) means no specific sign required,
%               thus it will follow from the cocos_in to cocos_out primary transformation and ipsign_in is not required
%       -1 or +1: ask for this sign of Ip in output, then requires ipsign_in to know if additional transformation
%                    is required after the standard cocos transformation (=> secondary transformation)
% b0sign_out: desired sign of toroidal magnetic field in ids_out (if a specific sign is aimed for)
%             0 (or empty or not provided) means no specific sign required,
%               thus it will follow from the cocos_in to cocos_out primary transformation and b0sign_in is not required
%       -1 or +1: ask for this sign of B0 in output, then requires b0sign_in to know if additional transformation
%                    is required after the standard cocos transformation ( => secondary transformation)
% ipsign_in: sign of Ip in present cocos_in equilirium, required if ipsign_out is +1 or -1
% b0sign_in: sign of B0 in present cocos_in equilirium, required if b0sign_out is +1 or -1
%
% varargin{1}: error_bar option: if 'added' changes _error_upper and _error_lower accordingly
% varargin{2}: nverbose (default=1)
%
seconds_0 = tic;
i=-1;

ids_type_available_cocostransform = {'equilibrium', 'magnetics', 'pf_active','wall', 'tf','core_profiles','core_sources','ec_launchers','nbi','pf_passive','summary'};
cocoscoeff = struct([]);

% Check inputs

if nargin<=1
  if nargin==1; warning('needs an ids_type within the list returned in ids_out.ids_type_available_cocostransform'); end
  ids_out.ids_type_available_cocostransform = ids_type_available_cocostransform;
  return
end

if nargin<4
  warning('the first 4 inputs are mandatory')
  return
end
if isempty(ids_in) || ~isstruct(ids_in)
  warning('expects a valid ids as 1st input')
  return
end
eval([ids_type '= ids_in;']);
eval(['ids_out = ' ids_type ';']);
% check ids_type
ij=strmatch(ids_type,ids_type_available_cocostransform,'exact');
if isempty(ij)
  warning(['ids_type: ' ids_type ' not available yet ask O. Sauter and check list in ids_out.ids_type_available_cocostransform'])
  ids_out.ids_type_available_cocostransform = ids_type_available_cocostransform;
  return
end
if isempty(cocos_in) || cocos_in<1 || cocos_in>18 || (cocos_in>8 & cocos_in<11)
  warning('cocos_in should be in [1,8] or [11,18]')
  return
end
if isempty(cocos_out) || cocos_out<1 || cocos_out>18 || (cocos_out>8 & cocos_out<11)
  warning('cocos_out should be in [1,8] or [11,18]')
  return
end

if ~exist('ipsign_out') || isempty(ipsign_out)
  ipsign_out = 0;
end
if ipsign_out~=-1 && ipsign_out~=0 && ipsign_out~=+1
  warning('ipsign_out should be +1, -1, 0 or empty')
  return
end

if ~exist('b0sign_out') || isempty(b0sign_out)
  b0sign_out = 0;
end
if b0sign_out~=-1 && b0sign_out~=0 && b0sign_out~=+1
  warning('b0sign_out should be +1, -1, 0 or empty')
  return
end
if abs(ipsign_out)==1 && (~exist('ipsign_in') || abs(ipsign_in)~=1)
  warning('ipsign_in should be provided (+1 or -1) if a specific ipsign_out is desired')
  return
end
if abs(b0sign_out)==1 && (~exist('b0sign_in') || abs(b0sign_in)~=1)
  warning('b0sign_in should be provided (+1 or -1) if a specific b0sign_out is desired')
  return
end
if ~exist('ipsign_in')
  ipsign_in = 10;
end
if ~exist('b0sign_in')
  b0sign_in = 10;
end

if exist('varargin') && length(varargin)>=1 && ~isempty(varargin{1}) && ischar(varargin{1})
  error_bar = varargin{1};
else
  error_bar = 'na';
end
error_bar_added = [strcmp(error_bar,'added')];

if exist('varargin') && length(varargin)>=2 && ~isempty(varargin{2}) && isnumeric(varargin{2})
  nverbose = varargin{2};
else
  nverbose = 1;
end

% prepare nodes to transform
[apath]=fileparts(which('cocos_transform_coefficients'));
table_filename = fullfile(apath,'ids_cocos_transformations_symbolic_table.csv');
if exist(table_filename,'file')
  % with commentstyle no need for 'headerlines',2 so can use matlab comment in middle
  [leaf_name,label_transformation,transformation_expression,leaf_name_matlab,length_i,length_j]=textread(table_filename,'%s%s%s%s%s%s', ...
          'delimiter',';','commentstyle','matlab');
else
  warning(['table of transformation not available in file: ' table_filename])
  return
end

% find valid leaf_names for this ids
ids_type_len = length(ids_type);
j_leaf_tocheck = [];
is_grid_type = [];
index_grid_type_leaf{1} = '';
icount = 0;
for ijk=1:length(leaf_name)
  if length(leaf_name{ijk})>ids_type_len && strcmp([ids_type '.'],leaf_name{ijk}(1:ids_type_len+1)) % make sure full name is an ids, add dot
    icount = icount + 1;
    j_leaf_tocheck(icount) = ijk;
    if length(label_transformation{ijk}) > 12
      is_grid_type(icount) = strcmp(label_transformation{ijk}(1:10),{'grid_type_'});
      if is_grid_type(icount)
        is_grid_type(icount) = is_grid_type(icount) + strcmp(label_transformation{ijk},{'grid_type_tensor_contravariant_like'});
        is_grid_type(icount) = is_grid_type(icount) + 2*strcmp(label_transformation{ijk},{'grid_type_tensor_covariant_like'});
        % find location of grid_type.index
        i_parent = findstr(leaf_name_matlab{ijk},'.');
        if ~isempty(i_parent) && isnumeric(i_parent)
          if length(i_parent>2) && strcmp(leaf_name_matlab{ijk}(i_parent(end-1)+1:i_parent(end)-1),'grid')
            i_parent = i_parent(end-1);
          else
            i_parent = i_parent(end);
          end
          index_grid_type_leaf{icount} = [leaf_name_matlab{ijk}(1:i_parent) 'grid_type.index'];
        else
          warning('should never arrive here...?');
          keyboard
          index_grid_type_leaf{icount} = '';
        end
      else
        index_grid_type_leaf{icount} = '';
      end
    else
      is_grid_type(icount) = 0;
      index_grid_type_leaf{icount} = '';
    end
  end
end
[cocoscoeff,cocoscoeffsym]=cocos_transform_coefficients(cocos_in, cocos_out, label_transformation, transformation_expression, ...
          ipsign_in, b0sign_in, ipsign_out, b0sign_out);
j_leaf_not_ok = [];
j_leaf_exist_default_float = [];
j_leaf_exist_default_integer = [];
j_leaf_exist_is_empty = [];
j_leaf_exist_nonnumeric = [];

seconds_1=toc(seconds_0);

% Scan through non grid_type nodes first
ijk_not_grid = find(~is_grid_type);
for ii=1:length(ijk_not_grid)
  ijk = ijk_not_grid(ii);
  try
    eval(['length_i_eff = length(' length_i{j_leaf_tocheck(ijk)} ');']);
  catch
    fprintf('\n*******************************\nNo %s found for COCOS check/transform \n*******************************\n\n',length_i{j_leaf_tocheck(ijk)});
    length_i_eff = 0;
  end
  for i=1:length_i_eff
    try
      eval(['length_j_eff = length(' length_j{j_leaf_tocheck(ijk)} ');']);
    catch
      fprintf('\n*******************************\nNo %s found for COCOS check/transform for i = %i \n*******************************\n\n',length_j{j_leaf_tocheck(ijk)},i);
      length_j_eff = 0;
    end
    for j=1:length_j_eff
      try
        eval(['aa_tmp_ids = ' leaf_name_matlab{j_leaf_tocheck(ijk)} ';'])
        aa_ids_isvalid = ids_value_isvalid(aa_tmp_ids);
        if isnumeric(aa_tmp_ids) && any(aa_ids_isvalid==[5]) && sum([~isfinite(aa_tmp_ids)])==1 && length(aa_tmp_ids)>1
          % 1 point Inf most probably q(lcfs) in diverted shape, so ok
          if nverbose >= 5; disp(['for ' leaf_name{j_leaf_tocheck(ijk)} ': 1 point Inf most probably q(lcfs) in diverted shape, so ok']); end
          aa_ids_isvalid = 0;
        end
        if isnumeric(aa_tmp_ids) && ~aa_ids_isvalid
          eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '=' cocoscoeffsym.(label_transformation{j_leaf_tocheck(ijk)}) ...
                '*' leaf_name_matlab{j_leaf_tocheck(ijk)} ';']);
          % error_bar is abs(delta) except if added
          if error_bar_added && eval([cocoscoeffsym.(label_transformation{j_leaf_tocheck(ijk)}) '* 1'])~=1
            try;eval(['a_upper=' leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_upper;']);catch;a_upper='catched';end
            if isnumeric(a_upper) && ~ids_value_isvalid(a_upper) && length(a_upper)==eval(['length(' leaf_name_matlab{j_leaf_tocheck(ijk)} ')'])
              if eval([cocoscoeffsym.(label_transformation{j_leaf_tocheck(ijk)}) '* 1']) > 0
                eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_upper = ' cocoscoeffsym.(label_transformation{j_leaf_tocheck(ijk)}) '* a_upper;']);
                eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_lower = ' cocoscoeffsym.(label_transformation{j_leaf_tocheck(ijk)}) '*' leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_lower;']);
              else
                eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_upper = ' cocoscoeffsym.(label_transformation{j_leaf_tocheck(ijk)}) '*' leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_lower;']);
                eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_lower = ' cocoscoeffsym.(label_transformation{j_leaf_tocheck(ijk)}) '* a_upper;']);
              end
            end
          end
        else
          switch aa_ids_isvalid
           case 1
            j_leaf_exist_default_float(i,j,ijk) = j_leaf_tocheck(ijk);
           case 2
            j_leaf_exist_default_integer(i,j,ijk) = j_leaf_tocheck(ijk);
           case 3
            j_leaf_exist_is_empty(i,j,ijk) = j_leaf_tocheck(ijk);
           case {4,5}
            j_leaf_exist_nonnumeric(i,j,ijk) = j_leaf_tocheck(ijk);
           otherwise
            error(['aa_ids_isvalid = ' num2str(aa_ids_isvalid) ' not expected, happened for ' leaf_name_matlab{j_leaf_tocheck(ijk)}]);
          end
        end
      catch MEleaf1
        disp([leaf_name_matlab{j_leaf_tocheck(ijk)} ' in catch'])
        if i==1 && nverbose >= 5; keyboard;end
        j_leaf_not_ok(i,j,ijk) = j_leaf_tocheck(ijk);
      end
    end
  end
end
seconds_2 = toc(seconds_0);
% standard grid_type nodes (not tensors)
j_leaf_grid_index_not_ok = [];
ijk_grid = find(is_grid_type==1);
for ii=1:length(ijk_grid)
  ijk = ijk_grid(ii);
  try
    eval(['length_i_eff = length(' length_i{j_leaf_tocheck(ijk)} ');']);
  catch
    fprintf('\n*******************************\nNo %s found for COCOS check/transform \n*******************************\n\n',length_i{j_leaf_tocheck(ijk)});
    length_i_eff = 0;
  end
  for i=1:length_i_eff
    try
      eval(['length_j_eff = length(' length_j{j_leaf_tocheck(ijk)} ');']);
    catch
      fprintf('\n*******************************\nNo %s found for COCOS check/transform for i = %i \n*******************************\n\n',length_j{j_leaf_tocheck(ijk)},i);
      length_j_eff = 0;
    end
    for j=1:length_j_eff
      try
        index_grid_type = eval(index_grid_type_leaf{ijk});
        if ~ids_value_isvalid(index_grid_type)
          index_grid_type_substr = ['_' num2str(index_grid_type)];
          eval(['aa_tmp_ids = ' leaf_name_matlab{j_leaf_tocheck(ijk)} ';'])
          aa_ids_isvalid = ids_value_isvalid(aa_tmp_ids);
          if isnumeric(aa_tmp_ids) && any(aa_ids_isvalid==[5]) && sum([~isfinite(aa_tmp_ids)])==1 && length(aa_tmp_ids)>1
            % 1 point Inf most probably q(lcfs) in diverted shape, so ok
            if nverbose >= 5; disp(['for ' leaf_name{j_leaf_tocheck(ijk)} ': 1 point Inf most probably q(lcfs) in diverted shape, so ok']); end
            aa_ids_isvalid = 0;
          end
          if isnumeric(aa_tmp_ids) && ~aa_ids_isvalid
            eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '=' cocoscoeffsym.([label_transformation{j_leaf_tocheck(ijk)} index_grid_type_substr]) ...
                  '*' leaf_name_matlab{j_leaf_tocheck(ijk)} ';']);
            % error_bar is abs(delta) except if added
            if error_bar_added && eval([cocoscoeffsym.([label_transformation{j_leaf_tocheck(ijk)} index_grid_type_substr]) '*1'])~=1
              try;eval(['a_upper=' leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_upper;']);catch;a_upper='catched';end
              if isnumeric(a_upper) && ~ids_value_isvalid(a_upper) && length(a_upper)==eval(['length(' leaf_name_matlab{j_leaf_tocheck(ijk)} ')'])
                if eval([cocoscoeffsym.([label_transformation{j_leaf_tocheck(ijk)} index_grid_type_substr]) '*1']) > 0
                  eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_upper = ' cocoscoeffsym.([label_transformation{j_leaf_tocheck(ijk)} index_grid_type_substr]) '* a_upper;']);
                  eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_lower = ' cocoscoeffsym.([label_transformation{j_leaf_tocheck(ijk)} index_grid_type_substr]) '*' leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_lower;']);
                else
                  eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_upper = ' cocoscoeffsym.([label_transformation{j_leaf_tocheck(ijk)} index_grid_type_substr]) '*' leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_lower;']);
                  eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '_error_lower = ' cocoscoeffsym.([label_transformation{j_leaf_tocheck(ijk)} index_grid_type_substr]) '* a_upper;']);
                end
              end
            end
          else
            switch aa_ids_isvalid
             case 1
              j_leaf_exist_default_float(i,j,ijk) = j_leaf_tocheck(ijk);
             case 2
              j_leaf_exist_default_integer(i,j,ijk) = j_leaf_tocheck(ijk);
             case 3
              j_leaf_exist_is_empty(i,j,ijk) = j_leaf_tocheck(ijk);
             case {4,5}
              j_leaf_exist_nonnumeric(i,j,ijk) = j_leaf_tocheck(ijk);
             otherwise
              error(['aa_ids_isvalid = ' num2str(aa_ids_isvalid) ' not expected, happened for ' leaf_name_matlab{j_leaf_tocheck(ijk)}]);
            end
          end
        else
          if ids_value_isvalid(index_grid_type) ~= 2
            j_leaf_grid_index_not_ok(i,j,ijk) = j_leaf_tocheck(ijk);
          end
        end
      catch MEleaf2
        disp([leaf_name_matlab{j_leaf_tocheck(ijk)} ' in catch'])
        if i==1 && nverbose >= 5; keyboard;end
        j_leaf_not_ok(i,j,ijk) = j_leaf_tocheck(ijk);
      end
    end
  end
end
seconds_3 = toc(seconds_0);
ijk_grid = find(is_grid_type==2 | is_grid_type==3);
% tensor contra and covariant
for ii=1:length(ijk_grid)
  ijk = ijk_grid(ii);
  try
    eval(['length_i_eff = length(' length_i{j_leaf_tocheck(ijk)} ');']);
  catch
    fprintf('\n*******************************\nNo %s found for COCOS check/transform \n*******************************\n\n',length_i{j_leaf_tocheck(ijk)});
    length_i_eff = 0;
  end
  for i=1:length_i_eff
    try
      eval(['length_j_eff = length(' length_j{j_leaf_tocheck(ijk)} ');']);
    catch
      fprintf('\n*******************************\nNo %s found for COCOS check/transform for i = %i \n*******************************\n\n',length_j{j_leaf_tocheck(ijk)},i);
      length_j_eff = 0;
    end
    for j=1:length_j_eff
      try
        index_grid_type = eval(index_grid_type_leaf{ijk});
        if ~ids_value_isvalid(index_grid_type)
          index_grid_type_substr = ['_' num2str(index_grid_type)];
          eval(['aa_tmp_ids = ' leaf_name_matlab{j_leaf_tocheck(ijk)} ';'])
          aa_ids_isvalid = ids_value_isvalid(aa_tmp_ids);
          if isnumeric(aa_tmp_ids) && any(aa_ids_isvalid==[5]) && sum([~isfinite(aa_tmp_ids)])==1 && length(aa_tmp_ids)>1
            aa_ids_isvalid = 0;
          end
          if isnumeric(aa_tmp_ids) && ~aa_ids_isvalid
            tens_sizes = eval(['size(' leaf_name_matlab{j_leaf_tocheck(ijk)} ')']);
            if length(tens_sizes) == 4 && tens_sizes(3)==3 && tens_sizes(4)==3
              for il=1:tens_sizes(3)
                for jl=1:tens_sizes(4)
                  eval([leaf_name_matlab{j_leaf_tocheck(ijk)} '(:,:,il,jl) =' cocoscoeffsym.([label_transformation{j_leaf_tocheck(ijk)} index_grid_type_substr]){il,jl} ...
                        '*' leaf_name_matlab{j_leaf_tocheck(ijk)} '(:,:,il,jl);']);
                end
              end
            end
          else
            switch aa_ids_isvalid
             case 1
              j_leaf_exist_default_float(i,j,ijk) = j_leaf_tocheck(ijk);
             case 2
              j_leaf_exist_default_integer(i,j,ijk) = j_leaf_tocheck(ijk);
             case 3
              j_leaf_exist_is_empty(i,j,ijk) = j_leaf_tocheck(ijk);
             case {4,5}
              j_leaf_exist_nonnumeric(i,j,ijk) = j_leaf_tocheck(ijk);
             otherwise
              error(['aa_ids_isvalid = ' num2str(aa_ids_isvalid) ' not expected, happened for ' leaf_name_matlab{j_leaf_tocheck(ijk)}]);
            end
          end
        else
          if ids_value_isvalid(index_grid_type) ~= 2
            j_leaf_grid_index_not_ok(i,j,ijk) = j_leaf_tocheck(ijk);
          end
        end
      catch MEleaf3
        disp([leaf_name_matlab{j_leaf_tocheck(ijk)} ' in catch'])
        if i==1 && nverbose >= 5; keyboard;end
        j_leaf_not_ok(i,j,ijk) = j_leaf_tocheck(ijk);
      end
    end
  end
end
seconds_4 = toc(seconds_0);
eval(['ids_out = ' ids_type ';']);

% display not ok nodes
if nverbose >= 3
  if ~isempty(j_leaf_not_ok) && sum(sum(sum(j_leaf_not_ok))) > 0
    disp([char(10) '************* nodes with COCOS transformation defined but not ok ************' char(10)])
    for ijk=1:size(j_leaf_not_ok,3)
      for jj=1:size(j_leaf_not_ok,2)
        ij=find(j_leaf_not_ok(:,jj,ijk)>0);
        if ~isempty(ij)
          disp([leaf_name_matlab{j_leaf_tocheck(ijk)} ' not ok with j=' num2str(jj) ' and i in [' num2str(ij(1)) ',' num2str(ij(end)) ']']);
        end
      end
    end
  end
  if ~isempty(j_leaf_exist_default_float) && sum(sum(sum(j_leaf_exist_default_float))) > 0
    disp([char(10) '************* nodes with COCOS transformation defined which exist but have default float value ************' char(10)])
    for ijk=1:size(j_leaf_exist_default_float,3)
      for jj=1:size(j_leaf_exist_default_float,2)
        ij=find(j_leaf_exist_default_float(:,jj,ijk)>0);
        if ~isempty(ij)
          disp([leaf_name_matlab{j_leaf_tocheck(ijk)} ' with def float with j=' num2str(jj) ' and i in [' num2str(ij(1)) ',' num2str(ij(end)) ']']);
        end
      end
    end
  end
  if ~isempty(j_leaf_exist_default_integer) && sum(sum(sum(j_leaf_exist_default_integer))) > 0
    disp([char(10) '************* nodes with COCOS transformation defined which exist but have default integer value ************' char(10)])
    for ijk=1:size(j_leaf_exist_default_integer,3)
      for jj=1:size(j_leaf_exist_default_integer,2)
        ij=find(j_leaf_exist_default_integer(:,jj,ijk)>0);
        if ~isempty(ij)
          disp([leaf_name_matlab{j_leaf_tocheck(ijk)} ' with def integer with j=' num2str(jj) ' and i in [' num2str(ij(1)) ',' num2str(ij(end)) ']']);
        end
      end
    end
  end
  if ~isempty(j_leaf_exist_is_empty) && sum(sum(sum(j_leaf_exist_is_empty))) > 0
    disp([char(10) '************* nodes with COCOS transformation defined which exist but are empty ************' char(10)])
    for ijk=1:size(j_leaf_exist_is_empty,3)
      for jj=1:size(j_leaf_exist_is_empty,2)
        ij=find(j_leaf_exist_is_empty(:,jj,ijk)>0);
        if ~isempty(ij)
          disp([leaf_name_matlab{j_leaf_tocheck(ijk)} ' exist but is empty with j=' num2str(jj) ' and i in [' num2str(ij(1)) ',' num2str(ij(end)) ']']);
        end
      end
    end
  end
  if ~isempty(j_leaf_grid_index_not_ok) && sum(sum(sum(j_leaf_grid_index_not_ok))) > 0
    disp([char(10) '************* nodes with COCOS transformation defined which exist but grid index not valid ************' char(10)])
    for ijk=1:size(j_leaf_grid_index_not_ok,3)
      for jj=1:size(j_leaf_grid_index_not_ok,2)
        ij=find(j_leaf_grid_index_not_ok(:,jj,ijk)>0);
        if ~isempty(ij)
          disp([leaf_name_matlab{j_leaf_tocheck(ijk)} ' exist but grid index not valid with j=' num2str(jj) ' and i in [' num2str(ij(1)) ',' num2str(ij(end)) ']']);
        end
      end
    end
  end
end

if nverbose >= 2
  if ~isempty(j_leaf_exist_nonnumeric) && sum(sum(sum(j_leaf_exist_nonnumeric))) > 0
    disp([char(10) '************* nodes with COCOS transformation defined which exist but unexpected: NaN or infinite ************' char(10)])
    for ijk=1:size(j_leaf_exist_nonnumeric,3)
      for jj=1:size(j_leaf_exist_nonnumeric,2)
        ij=find(j_leaf_exist_nonnumeric(:,jj,ijk)>0);
        if ~isempty(ij)
          disp([leaf_name_matlab{j_leaf_tocheck(ijk)} ' NaN or Inf with j=' num2str(jj) ' and i in [' num2str(ij(1)) ',' num2str(ij(end)) ']']);
        end
      end
    end
  end
end

eval(['ids_out = ' ids_type ';']);
if nverbose >= 3
  disp(['total time in ids_generic_cocos_nodes_transformation_symbolic for ' ids_type ': ' num2str(toc(seconds_0)) ' with ' ...
        num2str(seconds_1) ',' num2str(seconds_2-seconds_1) ',' num2str(seconds_3-seconds_2) ',' num2str(seconds_4-seconds_3) ...
        ' for init, normal, grid_types and tensors nodes respectively'])
end
