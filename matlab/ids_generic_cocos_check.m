function [ids_out,cocos_struct,stat_struct]=ids_generic_cocos_check(ids_in,ids_type,cocos_in,ipsign_in,b0sign_in,varargin);
%
% [ids_out,cocos_struct,stat_struct]=ids_generic_cocos_check(ids_in,ids_type,cocos_in,ipsign_in,b0sign_in,varargin);
%
% ids_out = ids_generic_cocos_check; % returns the list of ids for which a cocos_check is available
%
% Required inputs:
% ids_in: input ids, mandatory except if no args provided, returns a structure with the list of available ids types
% ids_type: name of ids like 'equilibrium', 'core_profiles', ...
% cocos_in: cocos to check ids_in with
%
% Optional inputs:
% ipsign_in: sign(ip) of ids_in if not available within ids_in or to use for the check
% b0sign_in: sign(b0) of ids_in if not available within ids_in or to use for the check
%

cocos_struct = struct([]);
ids_type_available = {'equilibrium', 'core_profiles'};

if nargin<=1
  if nargin==1; disp('needs an ids_type within the list returned in ids_out.ids_type_available'); end
  ids_out.ids_type_available = ids_type_available;
  return
end

% check ids_type
ij=strmatch(ids_type,ids_type_available,'exact');
if isempty(ij)
  disp(['ids_type: ' ids_type ' not available yet for cocos_transform, see list in ids_out.ids_type_available'])
  ids_out.ids_type_available = ids_type_available;
  return
end

try
  ids_out = ids_gen(ids_type);
catch
  ids_out = ids_in;
end

if nargin<3
  disp('the first 3 inputs are mandatory')
  return
end
if isempty(ids_in) || ~isstruct(ids_in)
  disp('expects a valid ids as 1st input')
  return
end
fieldnames_gen = sort(fieldnames(ids_out));
fieldnames_in = sort(fieldnames(ids_in));
if length(fieldnames_gen) ~= length(fieldnames_in)
  disp(['ids_in does not have the expected fieldnames for ids_type= ' ids_type])
  fieldnames_gen
  return
end
iok=zeros(size(fieldnames_in));
for i=1:length(fieldnames_gen)
  iok(i) = strcmp(fieldnames_gen{i},fieldnames_in{i});
end
if ~prod(iok)
  disp(['fieldnames in ids_in do not have exactly the expected fieldnames for ids_type= ' ids_type])
  fieldnames_gen
  return
end

if isempty(cocos_in) || cocos_in<1 || cocos_in>18 || (cocos_in>8 & cocos_in<11)
  disp('cocos_in should be in [1,8] or [11,18]')
  return
end

if ~exist('ipsign_in') || isempty(ipsign_in)
  ipsign_in = 0;
end
if ipsign_in~=-1 && ipsign_in~=0 && ipsign_in~=+1
  disp('ipsign_in should be +1, -1, 0 or empty')
  return
end

if ~exist('b0sign_in') || isempty(b0sign_in)
  b0sign_in = 0;
end
if b0sign_in~=-1 && b0sign_in~=0 && b0sign_in~=+1
  disp('b0sign_in should be +1, -1, 0 or empty')
  return
end

% call relevant cocos_check function
if nargin<6 || isempty(varargin)
  eval(['[ids_out,cocos_struct,stat_struct] = ids_' ids_type '_cocos_check(ids_in,cocos_in,ipsign_in,b0sign_in);'])
else
  eval(['[ids_out,cocos_struct,stat_struct] = ids_' ids_type '_cocos_check(ids_in,cocos_in,ipsign_in,b0sign_in,varargin{:});'])
end
