function status = ids_check_cocos(ids_to_check_or_idsname,user_or_ids_name,tokamak_name,datamodel_topversion,shot,run_number,occurence,nverbose,varargin);
%
%  status =
%  ids_check_cocos(ids_to_check_or_idsname,user_or_ids_name,tokamak_name,datamodel_topversion,shot,run_number,occurence,nverbose,varargin);
%
% Check cocos consistency with COCOS = 11 (by default) and Ip, B0 signs negative (by default)
%
% Now ids_to_check_or_idsname='equilibrium' to check implemented, look at closest time to t=0 only at this stage
%
% New:
%      2 main inputs:
%                     ids_to_check_or_idsname: ids structure
%                     user_or_ids_name: ids name of the input structure: 'equilibrium' or else
%
% If read from mds files, compulsary inputs:
%
%    ids_to_check_or_idsname: 'equilibrium', 'core_profiles', ...
%    user_or_ids_name:     username for the database ($USER if own database, 'public' if public database)
%    tokamak_name:      'ITER', 'iter', 'test', 'tcv', etc
%    datamodel_topversion:  '3' (only 3 implemented at this stage)
%    shot, run_number:         shot and run_number number
%
%  Optional inputs:
%
%    occurence: (0 as default) occurence number
%    nverbose:  0: return only status (0: all ok, >0: nb of problems)
%               1: return status and print list of fields with problems
%               default nverbose = 1
%
%    varargin{1}: ip sign to check (-1, default)
%    varargin{2}: b0 sign to check (-1, default)
%    varargin{3}: cocos value to check with (11, default, ITER expected)
%

status = 999999;
ids_to_check = [];

if nargin <= 1
  warning('requires at least 2 to 6 input (ids struture, ids_name for example)');
  return
elseif isstruct(ids_to_check_or_idsname) && nargin>= 2
  ids_to_check = ids_to_check_or_idsname;
  ids_to_check_or_idsname = user_or_ids_name;
  if ~exist('tokamak_name'); tokamak_name = 'from input structure'; end
  if ~exist('datamodel_topversion'); datamodel_topversion = '3'; end
  if ~exist('shot'); shot = -1; end
  if ~exist('run_number'); run_number = -1; end
  if ~exist('occurence'); occurence = 0; end
elseif nargin < 6 || isempty(ids_to_check_or_idsname) || isempty(user_or_ids_name) || isempty(tokamak_name) ...
      || isempty(datamodel_topversion) || isempty(shot) || isempty(run_number)
  error(' if ids structure not provided, requires at least 6 non-empty inputs: ids_to_check_or_idsname,user_or_ids_name,tokamak_name,datamodel_topversion,shot,run_number');
end

if ~exist('occurence') || isempty(occurence)
  occurence = 0;
end
if ~exist('nverbose') || isempty(nverbose)
  nverbose = 1;
end
ipsign = -1;
b0sign = -1;
cocos_to_check = 11;
if nargin >= 9  && ~isempty(varargin{1})
  ipsign = varargin{1};
end
if nargin >= 10 && ~isempty(varargin{2})
  b0sign = varargin{2};
end
if nargin >= 11 && ~isempty(varargin{3})
  cocos_to_check = varargin{3};
end

if isempty(ids_to_check)
  pulseCtx = imas_open_env ('ids',shot,run_number,user_or_ids_name,tokamak_name,datamodel_topversion);
  if pulseCtx < 0
    warning('index of pulse negative from imas_open_env, check if pulse exists')
    return
  end
  if occurence == 0
    ids_to_check=ids_get_slice(pulseCtx,[ids_to_check_or_idsname],0,1); % check only first/one slice to reduce size
  else
    ids_to_check=ids_get_slice(pulseCtx,[ids_to_check_or_idsname '/' num2str(occurence)],0,1); % check only first/one slice to reduce size
  end
end

[ids_out,cocos_struct,stat_struct]=ids_generic_cocos_check(ids_to_check,ids_to_check_or_idsname,cocos_to_check,ipsign,b0sign);

status = stat_struct.global_status;

if nverbose == 0; return; end

if stat_struct.global_status > 0
  fprintf('%s %s/%s/%s %s %d/%d/%d\n\n','Check for user/database/model = ', ...
	  user_or_ids_name,tokamak_name,datamodel_topversion,' with shot/run/occurence= ',shot,run_number,occurence);
  ichld = fieldnames(stat_struct);
  ichld = setdiff(ichld,'global_status');
  for i=1:length(ichld)
    isubchld = fieldnames(stat_struct.(ichld{i}));
    isubchld_char = char(isubchld);
    for j=1:length(isubchld)
      disp([ichld{i} '.' isubchld_char(j,:) ': ' stat_struct.(ichld{i}).(isubchld{j})]);
    end
  end
end
