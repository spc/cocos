function [ids_out,cocos_transform_struct]=ids_generic_cocos_transform(ids_in,ids_type,cocos_in,cocos_out,ipsign_out,b0sign_out,varargin);
%
% [ids_out,cocos_transform_struct]=ids_generic_cocos_transform(ids_in,ids_type,cocos_in,cocos_out,ipsign_out,b0sign_out,varargin);
%
% ids_out = ids_generic_cocos_transform; % returns the list of ids for which a cocos_transform is available
%
% Required inputs:
% ids_in: input ids, mandatory except if no args provided, returns a structure with the list of available ids types
% ids_type: name of ids like 'equilibrium', 'core_profiles', ...
% cocos_in: cocos of input ids, if not known set to 0 or empty and then provide cocos_out and ipsign_out and b0sign_out
% cocos_out: required cocos for ids_out. Needs to be a relevant option: 1-8 or 11-18
% 
% Optional inputs:
% ipsign_out: desired sign of plasma current in ids_out (if a specific sign is aimed for)
%             input required if cocos_in is not provided.
%             0 (or empty or not provided) means no specific sign required,
%               thus it will follow from the cocos_in to cocos_out transformation and original sign in ids_in.
% b0sign_out: desired sign of toroidal magnetic field in ids_out (if a specific sign is aimed for)
%             input required if cocos_in is not provided.
%             0 (or empty or not provided) means no specific sign required,
%               thus it will follow from the cocos_in to cocos_out transformation and original sign in ids_in.
% 

cocos_transform_struct = struct([]);
ids_type_available = {'equilibrium', 'core_profiles'};

if nargin<=1
  if nargin==1; disp('needs an ids_type within the list returned in ids_out.ids_type_available'); end
  ids_out.ids_type_available = ids_type_available;
  return
end

% check ids_type
ij=strmatch(ids_type,ids_type_available,'exact');
if isempty(ij)
  disp(['ids_type: ' ids_type ' not available yet for cocos_transform, see list in ids_out.ids_type_available'])
  ids_out.ids_type_available = ids_type_available;
  return
end

ids_out = ids_gen(ids_type);

if nargin<4
  disp('the first 4 inputs are mandatory')
  return
end
if isempty(ids_in) || ~isstruct(ids_in)
  disp('expects a valid ids as 1st input')
  return
end
fieldnames_gen = sort(fieldnames(ids_out));
fieldnames_in = sort(fieldnames(ids_in));
if length(fieldnames_gen) ~= length(fieldnames_in)
  disp(['ids_in does not have the expected fieldnames for ids_type= ' ids_type])
  fieldnames_gen
  return
end
iok=zeros(size(fieldnames_in));
for i=1:length(fieldnames_gen)
  iok(i) = strcmp(fieldnames_gen{i},fieldnames_in{i});
end
if ~prod(iok)
  disp(['fieldnames in ids_in do not have exactly the expected fieldnames for ids_type= ' ids_type])
  fieldnames_gen
  return
end

if isempty(cocos_in)
  cocos_in = 0;
end
if isempty(cocos_out) || cocos_out<1 || cocos_out>18 || (cocos_out>8 & cocos_out<11)
  disp('cocos_out should be in [1,8] or [11,18]')
  return
end

if ~exist('ipsign_out') || isempty(ipsign_out)
  ipsign_out = 0;
end
if ipsign_out~=-1 && ipsign_out~=0 && ipsign_out~=+1
  disp('ipsign_out should be +1, -1, 0 or empty')
  return
end

if ~exist('b0sign_out') || isempty(b0sign_out)
  b0sign_out = 0;
end
if b0sign_out~=-1 && b0sign_out~=0 && b0sign_out~=+1
  disp('b0sign_out should be +1, -1, 0 or empty')
  return
end

% call relevant cocos_transform function
if nargin<7 || isempty(varargin)
  eval(['[ids_out,cocos_transform_struct] = ids_' ids_type '_cocos_transform(ids_in,cocos_in,cocos_out,ipsign_out,b0sign_out);'])
else
  eval(['[ids_out,cocos_transform_struct] = ids_' ids_type '_cocos_transform(ids_in,cocos_in,cocos_out,ipsign_out,b0sign_out,varargin{:});'])
end
