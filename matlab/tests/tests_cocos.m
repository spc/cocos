classdef tests_cocos < matlab.unittest.TestCase
% a simple parametrized test with tags - modify at will
% see documentation of matlab.unittest suite for more details!

  properties(TestParameter)
    shot = {64770}; % list of shots for which to run a test
  end

  methods(Test,TestTags = {'fast'})
    function my_fast_test(testCase,shot)
      disp('Testing ids_generic_cocos_nodes_transformation_symbolic for tf')
      ids_top_name = 'tf';
      aa=gdat([],'ids','source',ids_top_name);
      aa.(ids_top_name).b_field_tor_vacuum_r.data = +1.5;
      cocos_in = 11; cocos_out = 2;
      [ids_out,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(aa.(ids_top_name),ids_top_name,cocos_in, cocos_out);
      testCase.assertTrue(aa.(ids_top_name).b_field_tor_vacuum_r.data == -ids_out.b_field_tor_vacuum_r.data);
      %
      % test equilibrum check
      disp('Testing ids_generic_cocos_check for equilibrium')
      equil = equilibrium_ids_83100;
      [ids_out,cocos_struct,stat_struct] = ids_generic_cocos_check(equil,'equilibrium',11);
      testCase.assertTrue(stat_struct.global_status==0,['problem with ids_generic_cocos_check ' num2str(stat_struct.global_status)])
      %
    end
  end

  methods(Test,TestTags = {'slow'})
    function my_slow_test(testCase,shot)
      ids_top_name = 'equilibrium';
      aa=gdat([],'ids','source',ids_top_name);
      aa.(ids_top_name).time_slice{1}.global_quantities.q_axis = +1.5;
      aa.(ids_top_name).time_slice{1}.global_quantities.psi_axis = +1.5;
      aa.(ids_top_name).time_slice{1}.profiles_1d.f = [1 2 3 4];
      cocos_in = 11; cocos_out = 2;
      [ids_out,cocoscoeff]=ids_generic_cocos_nodes_transformation_symbolic(aa.(ids_top_name),ids_top_name,cocos_in, cocos_out);
      testCase.assertTrue(aa.(ids_top_name).time_slice{1}.global_quantities.q_axis == +ids_out.time_slice{1}.global_quantities.q_axis);
      testCase.assertTrue( ...
          abs(aa.(ids_top_name).time_slice{1}.global_quantities.psi_axis - -2.*pi*ids_out.time_slice{1}.global_quantities.psi_axis) < 1e-4);
      testCase.assertTrue( ...
          abs(sum(aa.(ids_top_name).time_slice{1}.profiles_1d.f - -ids_out.time_slice{1}.profiles_1d.f)) < 1e-4);
    end
  end
end
