
module equilibrium_ids_module
! Declaration of the generic IDS GET routine 

interface ids_get        
   module procedure ids_get_equilibrium
end interface ids_get   


! Declaration of the generic IDS GET_SLICE routine 
interface ids_get_slice        
   module procedure  ids_get_slice_equilibrium
end interface ids_get_slice

   
! Declaration of the generic IDS PUT_SLICE routine 
interface ids_put_slice    
   module procedure ids_put_slice_equilibrium
end interface ids_put_slice


! Declaration of the generic IDS PUT routine 
interface ids_put        
   module procedure ids_put_equilibrium		
end interface ids_put

! Declaration of the generic IDS PUT_NON_TIMED routine 
interface ids_put_non_timed
   module procedure ids_put_non_timed_equilibrium 		 
end interface ids_put_non_timed

! Declaration of the generic IDS DELETE routine 
interface ids_delete        
   module procedure ids_delete_equilibrium		
end interface ids_delete

! Declaration of the generic IDS DEALLOCATE routine 
interface ids_deallocate        
   module procedure ids_deallocate_equilibrium		
end interface ids_deallocate


! Declaration of the generic IDS COPY routine 
interface ids_copy        
   module procedure ids_copy_equilibrium		
end interface ids_copy


contains

character(10) function int2str(num) 
   integer, intent(in):: num
   character(10) :: str
   ! convert integer to string using formatted write
   write(str, '(i10)') num
   int2str = adjustl(str)
end function int2str


!!!!!! Routines to GET the full IDS 
subroutine ids_get_equilibrium(idx,path,  IDS)

use ids_schemas
implicit none

character*(*) :: path
integer :: idx, status, lenstring, istring, itime, lentime
integer :: ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7,dum1,dum2,dum3,dum4,dum5,dum6,dum7
character(len=3)::ual_debug

character(len=132)::stringans      ! Temporary way of getting short strings
character(len=100000)::longstring
character(len=300) :: timepath    
character(len=132), dimension(:), pointer ::stringpointer   => null()     
integer :: obj_all_times,obj1,obj2,obj3,obj4,obj5,obj6,obj7
integer :: dimObj0,dimObj1,dimObj2,dimObj3,dimObj4,dimObj5,dimObj6,dimObj7
integer :: i1,i2,i3,i4,i5,i6,i7

integer :: itime_slice
integer :: ix_point
integer :: istrike_point
integer :: iprofiles_2d

integer :: int0d
real(DP) :: double0d


type(ids_equilibrium) :: IDS       

call getenv('ual_debug',ual_debug) ! Debug flag

call begin_IDS_get(idx, path,0,dum1)
      
! Get ids_properties/comment
longstring = ' '    
call get_string(idx,path, "ids_properties"//"/comment",longstring, status)       
if (status.EQ.0) then
   lenstring = len_trim(longstring)
   allocate(IDS%ids_properties%comment(floor(real(lenstring/132))+1))
   if (lenstring <= 132) then             
      IDS%ids_properties%comment = trim(longstring)
   else
      do istring=1,floor(real(lenstring/132))+1
          IDS%ids_properties%comment(istring) = trim(longstring(1+(istring-1)*132 : istring*132)) 
      enddo
   endif
   if (ual_debug =='yes') write(*,*) & 
      'Get IDS%ids_properties%comment',IDS%ids_properties%comment
endif   

! Get ids_properties/homogeneous_time
call get_int(idx,path, "ids_properties"//"/homogeneous_time",int0d,status)
if (status.EQ.0) then
   IDS%ids_properties%homogeneous_time = int0d
   if (ual_debug =='yes') write(*,*) & 
      'Get IDS%ids_properties%homogeneous_time'
endif                 

! Get ids_properties/cocos
call get_int(idx,path, "ids_properties"//"/cocos",int0d,status)
if (status.EQ.0) then
   IDS%ids_properties%cocos = int0d
   if (ual_debug =='yes') write(*,*) & 
      'Get IDS%ids_properties%cocos'
endif                 

! Get vacuum_toroidal_field/r0
call get_double(idx,path, "vacuum_toroidal_field"//"/r0",double0d,status)
if (status.EQ.0) then
   IDS%vacuum_toroidal_field%r0 = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get IDS%vacuum_toroidal_field%r0'
endif                 

! Get vacuum_toroidal_field/b0
   call get_dimension(idx,path, "vacuum_toroidal_field"//"/b0",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%vacuum_toroidal_field%b0(dim1))
      call get_vect1d_double(idx,path,"vacuum_toroidal_field"//"/b0", &
      IDS%vacuum_toroidal_field%b0 &
      ,dim1,dum1,status)
      if (ual_debug =='yes') write(*,*) &  
      'Get IDS%vacuum_toroidal_field%b0'
   endif        

! Structure array of type 3 : time_slice
call get_object(idx,path,"time_slice",obj_all_times,TIMED,status) ! read the whole timed block
if (status.EQ.0) then
   call get_object_dim(idx,obj_all_times,lentime)  ! the size of this top object is the number of time slices
   allocate(ids%time_slice(lentime))
   if (ual_debug =='yes') write(*,*) &
      'Get ids%time_slice, lentime =', lentime
   do i1 = 1,lentime     ! fill every time slice
      call get_object_from_object(idx,obj_all_times,"ALLTIMES",i1,obj1,status)
      if (status.EQ.0) then
         !call get_object_dim(idx,obj1,dimObj1)
               
! Get time_slice/boundary/type 
call get_int_from_object(idx,obj1,"time_slice/boundary/type",1,int0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%type = int0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%type'
endif                 

! Get time_slice/boundary/lcfs/r
call get_dimension_from_object(idx,obj1,"time_slice/boundary/lcfs/r",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%boundary%lcfs%r(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/boundary/lcfs/r",1, &
   ids%time_slice(i1)%boundary%lcfs%r,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%boundary%lcfs%r'
endif        

! Get time_slice/boundary/lcfs/z
call get_dimension_from_object(idx,obj1,"time_slice/boundary/lcfs/z",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%boundary%lcfs%z(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/boundary/lcfs/z",1, &
   ids%time_slice(i1)%boundary%lcfs%z,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%boundary%lcfs%z'
endif        

! Get time_slice/boundary/geometric_axis/r 
call get_double_from_object(idx,obj1,"time_slice/boundary/geometric_axis/r",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%geometric_axis%r = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%geometric_axis%r'
endif

! Get time_slice/boundary/geometric_axis/z 
call get_double_from_object(idx,obj1,"time_slice/boundary/geometric_axis/z",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%geometric_axis%z = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%geometric_axis%z'
endif

! Get time_slice/boundary/a_minor 
call get_double_from_object(idx,obj1,"time_slice/boundary/a_minor",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%a_minor = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%a_minor'
endif

! Get time_slice/boundary/elongation 
call get_double_from_object(idx,obj1,"time_slice/boundary/elongation",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%elongation = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%elongation'
endif

! Get time_slice/boundary/elongation_upper 
call get_double_from_object(idx,obj1,"time_slice/boundary/elongation_upper",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%elongation_upper = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%elongation_upper'
endif

! Get time_slice/boundary/elongation_lower 
call get_double_from_object(idx,obj1,"time_slice/boundary/elongation_lower",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%elongation_lower = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%elongation_lower'
endif

! Get time_slice/boundary/triangularity 
call get_double_from_object(idx,obj1,"time_slice/boundary/triangularity",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%triangularity = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%triangularity'
endif

! Get time_slice/boundary/triangularity_upper 
call get_double_from_object(idx,obj1,"time_slice/boundary/triangularity_upper",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%triangularity_upper = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%triangularity_upper'
endif

! Get time_slice/boundary/triangularity_lower 
call get_double_from_object(idx,obj1,"time_slice/boundary/triangularity_lower",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%triangularity_lower = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%triangularity_lower'
endif

! Get time_slice/boundary/x_point
call get_object_from_object(idx, obj1, "time_slice/boundary/x_point", 1, obj2,status)


if (status.EQ.0) then
   call get_object_dim(idx,obj2,dimObj2)
   if (dimObj2.GT.0) then
      if (associated(ids%time_slice(i1)%boundary%x_point)) then ! does this array already exist? (timed and non timed parts can share the same array)
         if (size(ids%time_slice(i1)%boundary%x_point).NE.dimObj2) then ! then it must have the right number of elements
            write(*,*) "Error in get: array of structures has different number of timed and nontimed elements for time_slice/boundary/x_point"
            deallocate(ids%time_slice(i1)%boundary%x_point)
         endif
      else
         allocate(ids%time_slice(i1)%boundary%x_point(dimObj2))
      endif
      if (associated(ids%time_slice(i1)%boundary%x_point)) then
         do i2 = 1,dimObj2     ! process array elements
            
! Get time_slice/boundary/x_point/r 
call get_double_from_object(idx,obj2,"x_point/r",i2 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%x_point(i2)%r = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%x_point(i2)%r'
endif

! Get time_slice/boundary/x_point/z 
call get_double_from_object(idx,obj2,"x_point/z",i2 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%x_point(i2)%z = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%x_point(i2)%z'
endif

         enddo
      endif
   endif
endif
  
! Get time_slice/boundary/strike_point
call get_object_from_object(idx, obj1, "time_slice/boundary/strike_point", 1, obj2,status)


if (status.EQ.0) then
   call get_object_dim(idx,obj2,dimObj2)
   if (dimObj2.GT.0) then
      if (associated(ids%time_slice(i1)%boundary%strike_point)) then ! does this array already exist? (timed and non timed parts can share the same array)
         if (size(ids%time_slice(i1)%boundary%strike_point).NE.dimObj2) then ! then it must have the right number of elements
            write(*,*) "Error in get: array of structures has different number of timed and nontimed elements for time_slice/boundary/strike_point"
            deallocate(ids%time_slice(i1)%boundary%strike_point)
         endif
      else
         allocate(ids%time_slice(i1)%boundary%strike_point(dimObj2))
      endif
      if (associated(ids%time_slice(i1)%boundary%strike_point)) then
         do i2 = 1,dimObj2     ! process array elements
            
! Get time_slice/boundary/strike_point/r 
call get_double_from_object(idx,obj2,"strike_point/r",i2 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%strike_point(i2)%r = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%strike_point(i2)%r'
endif

! Get time_slice/boundary/strike_point/z 
call get_double_from_object(idx,obj2,"strike_point/z",i2 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%strike_point(i2)%z = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%strike_point(i2)%z'
endif

         enddo
      endif
   endif
endif
  
! Get time_slice/boundary/active_limiter_point/r 
call get_double_from_object(idx,obj1,"time_slice/boundary/active_limiter_point/r",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%active_limiter_point%r = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%active_limiter_point%r'
endif

! Get time_slice/boundary/active_limiter_point/z 
call get_double_from_object(idx,obj1,"time_slice/boundary/active_limiter_point/z",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%boundary%active_limiter_point%z = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%boundary%active_limiter_point%z'
endif

! Get time_slice/global_quantities/beta_pol 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/beta_pol",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%beta_pol = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%beta_pol'
endif

! Get time_slice/global_quantities/beta_tor 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/beta_tor",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%beta_tor = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%beta_tor'
endif

! Get time_slice/global_quantities/beta_normal 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/beta_normal",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%beta_normal = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%beta_normal'
endif

! Get time_slice/global_quantities/ip 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/ip",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%ip = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%ip'
endif

! Get time_slice/global_quantities/li_3 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/li_3",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%li_3 = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%li_3'
endif

! Get time_slice/global_quantities/volume 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/volume",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%volume = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%volume'
endif

! Get time_slice/global_quantities/area 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/area",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%area = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%area'
endif

! Get time_slice/global_quantities/surface 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/surface",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%surface = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%surface'
endif

! Get time_slice/global_quantities/length_pol 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/length_pol",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%length_pol = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%length_pol'
endif

! Get time_slice/global_quantities/psi_axis 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/psi_axis",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%psi_axis = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%psi_axis'
endif

! Get time_slice/global_quantities/psi_boundary 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/psi_boundary",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%psi_boundary = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%psi_boundary'
endif

! Get time_slice/global_quantities/magnetic_axis/r 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/r",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%magnetic_axis%r = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%magnetic_axis%r'
endif

! Get time_slice/global_quantities/magnetic_axis/z 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/z",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%magnetic_axis%z = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%magnetic_axis%z'
endif

! Get time_slice/global_quantities/magnetic_axis/b_tor 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/b_tor",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%magnetic_axis%b_tor = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%magnetic_axis%b_tor'
endif

! Get time_slice/global_quantities/q_axis 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/q_axis",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%q_axis = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%q_axis'
endif

! Get time_slice/global_quantities/q_95 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/q_95",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%q_95 = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%q_95'
endif

! Get time_slice/global_quantities/q_min/value 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/q_min/value",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%q_min%value = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%q_min%value'
endif

! Get time_slice/global_quantities/q_min/rho_tor_norm 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/q_min/rho_tor_norm",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%q_min%rho_tor_norm = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%q_min%rho_tor_norm'
endif

! Get time_slice/global_quantities/w_mhd 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/w_mhd",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%global_quantities%w_mhd = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%global_quantities%w_mhd'
endif

! Get time_slice/profiles_1d/psi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/psi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%psi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/psi",1, &
   ids%time_slice(i1)%profiles_1d%psi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%psi'
endif        

! Get time_slice/profiles_1d/phi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/phi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%phi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/phi",1, &
   ids%time_slice(i1)%profiles_1d%phi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%phi'
endif        

! Get time_slice/profiles_1d/pressure
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/pressure",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%pressure(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/pressure",1, &
   ids%time_slice(i1)%profiles_1d%pressure,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%pressure'
endif        

! Get time_slice/profiles_1d/f
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/f",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%f(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/f",1, &
   ids%time_slice(i1)%profiles_1d%f,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%f'
endif        

! Get time_slice/profiles_1d/dpressure_dpsi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/dpressure_dpsi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%dpressure_dpsi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/dpressure_dpsi",1, &
   ids%time_slice(i1)%profiles_1d%dpressure_dpsi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%dpressure_dpsi'
endif        

! Get time_slice/profiles_1d/f_df_dpsi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/f_df_dpsi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%f_df_dpsi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/f_df_dpsi",1, &
   ids%time_slice(i1)%profiles_1d%f_df_dpsi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%f_df_dpsi'
endif        

! Get time_slice/profiles_1d/j_tor
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/j_tor",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%j_tor(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/j_tor",1, &
   ids%time_slice(i1)%profiles_1d%j_tor,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%j_tor'
endif        

! Get time_slice/profiles_1d/j_parallel
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/j_parallel",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%j_parallel(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/j_parallel",1, &
   ids%time_slice(i1)%profiles_1d%j_parallel,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%j_parallel'
endif        

! Get time_slice/profiles_1d/q
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/q",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%q(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/q",1, &
   ids%time_slice(i1)%profiles_1d%q,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%q'
endif        

! Get time_slice/profiles_1d/magnetic_shear
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/magnetic_shear",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%magnetic_shear(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/magnetic_shear",1, &
   ids%time_slice(i1)%profiles_1d%magnetic_shear,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%magnetic_shear'
endif        

! Get time_slice/profiles_1d/r_inboard
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/r_inboard",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%r_inboard(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/r_inboard",1, &
   ids%time_slice(i1)%profiles_1d%r_inboard,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%r_inboard'
endif        

! Get time_slice/profiles_1d/r_outboard
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/r_outboard",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%r_outboard(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/r_outboard",1, &
   ids%time_slice(i1)%profiles_1d%r_outboard,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%r_outboard'
endif        

! Get time_slice/profiles_1d/rho_tor
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/rho_tor",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%rho_tor(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/rho_tor",1, &
   ids%time_slice(i1)%profiles_1d%rho_tor,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%rho_tor'
endif        

! Get time_slice/profiles_1d/rho_tor_norm
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/rho_tor_norm",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%rho_tor_norm(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/rho_tor_norm",1, &
   ids%time_slice(i1)%profiles_1d%rho_tor_norm,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%rho_tor_norm'
endif        

! Get time_slice/profiles_1d/dpsi_drho_tor
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/dpsi_drho_tor",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%dpsi_drho_tor(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/dpsi_drho_tor",1, &
   ids%time_slice(i1)%profiles_1d%dpsi_drho_tor,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%dpsi_drho_tor'
endif        

! Get time_slice/profiles_1d/elongation
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/elongation",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%elongation(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/elongation",1, &
   ids%time_slice(i1)%profiles_1d%elongation,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%elongation'
endif        

! Get time_slice/profiles_1d/triangularity_upper
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/triangularity_upper",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%triangularity_upper(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/triangularity_upper",1, &
   ids%time_slice(i1)%profiles_1d%triangularity_upper,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%triangularity_upper'
endif        

! Get time_slice/profiles_1d/triangularity_lower
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/triangularity_lower",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%triangularity_lower(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/triangularity_lower",1, &
   ids%time_slice(i1)%profiles_1d%triangularity_lower,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%triangularity_lower'
endif        

! Get time_slice/profiles_1d/volume
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/volume",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%volume(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/volume",1, &
   ids%time_slice(i1)%profiles_1d%volume,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%volume'
endif        

! Get time_slice/profiles_1d/dvolume_dpsi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/dvolume_dpsi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%dvolume_dpsi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/dvolume_dpsi",1, &
   ids%time_slice(i1)%profiles_1d%dvolume_dpsi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%dvolume_dpsi'
endif        

! Get time_slice/profiles_1d/dvolume_drho_tor
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/dvolume_drho_tor",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%dvolume_drho_tor(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/dvolume_drho_tor",1, &
   ids%time_slice(i1)%profiles_1d%dvolume_drho_tor,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%dvolume_drho_tor'
endif        

! Get time_slice/profiles_1d/area
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/area",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%area(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/area",1, &
   ids%time_slice(i1)%profiles_1d%area,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%area'
endif        

! Get time_slice/profiles_1d/darea_dpsi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/darea_dpsi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%darea_dpsi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/darea_dpsi",1, &
   ids%time_slice(i1)%profiles_1d%darea_dpsi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%darea_dpsi'
endif        

! Get time_slice/profiles_1d/surface
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/surface",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%surface(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/surface",1, &
   ids%time_slice(i1)%profiles_1d%surface,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%surface'
endif        

! Get time_slice/profiles_1d/trapped_fraction
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/trapped_fraction",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%trapped_fraction(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/trapped_fraction",1, &
   ids%time_slice(i1)%profiles_1d%trapped_fraction,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%trapped_fraction'
endif        

! Get time_slice/profiles_1d/gm1
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm1",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%gm1(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm1",1, &
   ids%time_slice(i1)%profiles_1d%gm1,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%gm1'
endif        

! Get time_slice/profiles_1d/gm2
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm2",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%gm2(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm2",1, &
   ids%time_slice(i1)%profiles_1d%gm2,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%gm2'
endif        

! Get time_slice/profiles_1d/gm3
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm3",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%gm3(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm3",1, &
   ids%time_slice(i1)%profiles_1d%gm3,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%gm3'
endif        

! Get time_slice/profiles_1d/gm4
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm4",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%gm4(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm4",1, &
   ids%time_slice(i1)%profiles_1d%gm4,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%gm4'
endif        

! Get time_slice/profiles_1d/gm5
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm5",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%gm5(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm5",1, &
   ids%time_slice(i1)%profiles_1d%gm5,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%gm5'
endif        

! Get time_slice/profiles_1d/gm6
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm6",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%gm6(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm6",1, &
   ids%time_slice(i1)%profiles_1d%gm6,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%gm6'
endif        

! Get time_slice/profiles_1d/gm7
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm7",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%gm7(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm7",1, &
   ids%time_slice(i1)%profiles_1d%gm7,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%gm7'
endif        

! Get time_slice/profiles_1d/gm8
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm8",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%gm8(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm8",1, &
   ids%time_slice(i1)%profiles_1d%gm8,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%gm8'
endif        

! Get time_slice/profiles_1d/gm9
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm9",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%gm9(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm9",1, &
   ids%time_slice(i1)%profiles_1d%gm9,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%gm9'
endif        

! Get time_slice/profiles_1d/b_average
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/b_average",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%b_average(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/b_average",1, &
   ids%time_slice(i1)%profiles_1d%b_average,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%b_average'
endif        

! Get time_slice/profiles_1d/b_min
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/b_min",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%b_min(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/b_min",1, &
   ids%time_slice(i1)%profiles_1d%b_min,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%b_min'
endif        

! Get time_slice/profiles_1d/b_max
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/b_max",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_1d%b_max(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/b_max",1, &
   ids%time_slice(i1)%profiles_1d%b_max,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_1d%b_max'
endif        

! Get time_slice/profiles_2d
call get_object_from_object(idx, obj1, "time_slice/profiles_2d", 1, obj2,status)


if (status.EQ.0) then
   call get_object_dim(idx,obj2,dimObj2)
   if (dimObj2.GT.0) then
      if (associated(ids%time_slice(i1)%profiles_2d)) then ! does this array already exist? (timed and non timed parts can share the same array)
         if (size(ids%time_slice(i1)%profiles_2d).NE.dimObj2) then ! then it must have the right number of elements
            write(*,*) "Error in get: array of structures has different number of timed and nontimed elements for time_slice/profiles_2d"
            deallocate(ids%time_slice(i1)%profiles_2d)
         endif
      else
         allocate(ids%time_slice(i1)%profiles_2d(dimObj2))
      endif
      if (associated(ids%time_slice(i1)%profiles_2d)) then
         do i2 = 1,dimObj2     ! process array elements
            
! Get time_slice/profiles_2d/grid_type/name  
longstring = ' '
call get_string_from_object(idx,obj2,"profiles_2d/grid_type/name",i2,longstring,status)
if (status.EQ.0) then
   lenstring = len_trim(longstring)      
   allocate(ids%time_slice(i1)%profiles_2d(i2)%grid_type%name(floor(real(lenstring/132))+1))
   if (lenstring <= 132) then             
      ids%time_slice(i1)%profiles_2d(i2)%grid_type%name = trim(longstring)
   else
      do istring=1,floor(real(lenstring/132))+1
          ids%time_slice(i1)%profiles_2d(i2)%grid_type%name(istring) = trim(longstring(1+(istring-1)*132 : istring*132))
      enddo
   endif
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%grid_type%name'
endif   

! Get time_slice/profiles_2d/grid_type/index 
call get_int_from_object(idx,obj2,"profiles_2d/grid_type/index",i2,int0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%profiles_2d(i2)%grid_type%index = int0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%grid_type%index'
endif                 

! Get time_slice/profiles_2d/grid_type/description  
longstring = ' '
call get_string_from_object(idx,obj2,"profiles_2d/grid_type/description",i2,longstring,status)
if (status.EQ.0) then
   lenstring = len_trim(longstring)      
   allocate(ids%time_slice(i1)%profiles_2d(i2)%grid_type%description(floor(real(lenstring/132))+1))
   if (lenstring <= 132) then             
      ids%time_slice(i1)%profiles_2d(i2)%grid_type%description = trim(longstring)
   else
      do istring=1,floor(real(lenstring/132))+1
          ids%time_slice(i1)%profiles_2d(i2)%grid_type%description(istring) = trim(longstring(1+(istring-1)*132 : istring*132))
      enddo
   endif
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%grid_type%description'
endif   

! Get time_slice/profiles_2d/grid/dim1
call get_dimension_from_object(idx,obj2,"profiles_2d/grid/dim1",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%grid%dim1(dim1))
   call get_vect1d_double_from_object(idx,obj2,"profiles_2d/grid/dim1",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%grid%dim1,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_2d(i2)%grid%dim1'
endif        

! Get time_slice/profiles_2d/grid/dim2
call get_dimension_from_object(idx,obj2,"profiles_2d/grid/dim2",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%grid%dim2(dim1))
   call get_vect1d_double_from_object(idx,obj2,"profiles_2d/grid/dim2",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%grid%dim2,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%profiles_2d(i2)%grid%dim2'
endif        

! Get time_slice/profiles_2d/r        
call get_dimension_from_object(idx,obj2,"profiles_2d/r",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%r(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/r",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%r, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%r'
endif 

! Get time_slice/profiles_2d/z        
call get_dimension_from_object(idx,obj2,"profiles_2d/z",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%z(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/z",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%z, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%z'
endif 

! Get time_slice/profiles_2d/psi        
call get_dimension_from_object(idx,obj2,"profiles_2d/psi",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%psi(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/psi",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%psi, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%psi'
endif 

! Get time_slice/profiles_2d/theta        
call get_dimension_from_object(idx,obj2,"profiles_2d/theta",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%theta(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/theta",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%theta, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%theta'
endif 

! Get time_slice/profiles_2d/phi        
call get_dimension_from_object(idx,obj2,"profiles_2d/phi",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%phi(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/phi",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%phi, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%phi'
endif 

! Get time_slice/profiles_2d/j_tor        
call get_dimension_from_object(idx,obj2,"profiles_2d/j_tor",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%j_tor(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/j_tor",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%j_tor, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%j_tor'
endif 

! Get time_slice/profiles_2d/j_parallel        
call get_dimension_from_object(idx,obj2,"profiles_2d/j_parallel",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%j_parallel(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/j_parallel",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%j_parallel, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%j_parallel'
endif 

! Get time_slice/profiles_2d/b_r        
call get_dimension_from_object(idx,obj2,"profiles_2d/b_r",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%b_r(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/b_r",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%b_r, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%b_r'
endif 

! Get time_slice/profiles_2d/b_z        
call get_dimension_from_object(idx,obj2,"profiles_2d/b_z",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%b_z(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/b_z",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%b_z, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%b_z'
endif 

! Get time_slice/profiles_2d/b_tor        
call get_dimension_from_object(idx,obj2,"profiles_2d/b_tor",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%profiles_2d(i2)%b_tor(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/b_tor",i2, &
   ids%time_slice(i1)%profiles_2d(i2)%b_tor, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%profiles_2d(i2)%b_tor'
endif 

         enddo
      endif
   endif
endif
  
! Get time_slice/coordinate_system/grid_type/name  
longstring = ' '
call get_string_from_object(idx,obj1,"time_slice/coordinate_system/grid_type/name",1,longstring,status)
if (status.EQ.0) then
   lenstring = len_trim(longstring)      
   allocate(ids%time_slice(i1)%coordinate_system%grid_type%name(floor(real(lenstring/132))+1))
   if (lenstring <= 132) then             
      ids%time_slice(i1)%coordinate_system%grid_type%name = trim(longstring)
   else
      do istring=1,floor(real(lenstring/132))+1
          ids%time_slice(i1)%coordinate_system%grid_type%name(istring) = trim(longstring(1+(istring-1)*132 : istring*132))
      enddo
   endif
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%grid_type%name'
endif   

! Get time_slice/coordinate_system/grid_type/index 
call get_int_from_object(idx,obj1,"time_slice/coordinate_system/grid_type/index",1,int0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%coordinate_system%grid_type%index = int0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%grid_type%index'
endif                 

! Get time_slice/coordinate_system/grid_type/description  
longstring = ' '
call get_string_from_object(idx,obj1,"time_slice/coordinate_system/grid_type/description",1,longstring,status)
if (status.EQ.0) then
   lenstring = len_trim(longstring)      
   allocate(ids%time_slice(i1)%coordinate_system%grid_type%description(floor(real(lenstring/132))+1))
   if (lenstring <= 132) then             
      ids%time_slice(i1)%coordinate_system%grid_type%description = trim(longstring)
   else
      do istring=1,floor(real(lenstring/132))+1
          ids%time_slice(i1)%coordinate_system%grid_type%description(istring) = trim(longstring(1+(istring-1)*132 : istring*132))
      enddo
   endif
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%grid_type%description'
endif   

! Get time_slice/coordinate_system/grid/dim1
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/grid/dim1",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%grid%dim1(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/coordinate_system/grid/dim1",1, &
   ids%time_slice(i1)%coordinate_system%grid%dim1,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%coordinate_system%grid%dim1'
endif        

! Get time_slice/coordinate_system/grid/dim2
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/grid/dim2",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%grid%dim2(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/coordinate_system/grid/dim2",1, &
   ids%time_slice(i1)%coordinate_system%grid%dim2,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(i1)%coordinate_system%grid%dim2'
endif        

! Get time_slice/coordinate_system/r        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/r",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%r(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/r",1, &
   ids%time_slice(i1)%coordinate_system%r, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%r'
endif 

! Get time_slice/coordinate_system/z        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/z",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%z(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/z",1, &
   ids%time_slice(i1)%coordinate_system%z, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%z'
endif 

! Get time_slice/coordinate_system/jacobian        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/jacobian",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%jacobian(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/jacobian",1, &
   ids%time_slice(i1)%coordinate_system%jacobian, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%jacobian'
endif 

! Get time_slice/coordinate_system/g_11        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_11",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%g_11(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_11",1, &
   ids%time_slice(i1)%coordinate_system%g_11, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%g_11'
endif 

! Get time_slice/coordinate_system/g_12        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_12",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%g_12(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_12",1, &
   ids%time_slice(i1)%coordinate_system%g_12, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%g_12'
endif 

! Get time_slice/coordinate_system/g_13        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_13",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%g_13(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_13",1, &
   ids%time_slice(i1)%coordinate_system%g_13, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%g_13'
endif 

! Get time_slice/coordinate_system/g_22        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_22",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%g_22(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_22",1, &
   ids%time_slice(i1)%coordinate_system%g_22, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%g_22'
endif 

! Get time_slice/coordinate_system/g_23        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_23",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%g_23(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_23",1, &
   ids%time_slice(i1)%coordinate_system%g_23, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%g_23'
endif 

! Get time_slice/coordinate_system/g_33        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_33",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(i1)%coordinate_system%g_33(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_33",1, &
   ids%time_slice(i1)%coordinate_system%g_33, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%coordinate_system%g_33'
endif 

! Get time_slice/time 
call get_double_from_object(idx,obj1,"time_slice/time",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(i1)%time = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(i1)%time'
endif

      endif
   enddo
   call release_object(idx,obj_all_times)
endif

! Get code/name
   call get_dimension(idx,path, "code"//"/name",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%code%name(dim1))
      call get_Vect1d_string(idx,path, "code"//"/name", &
          IDS%code%name,dim1,dum1,status)
      if (ual_debug =='yes') write(*,*) & 
      'Get IDS%code%name'
   endif   

! Get code/version
   call get_dimension(idx,path, "code"//"/version",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%code%version(dim1))
      call get_Vect1d_string(idx,path, "code"//"/version", &
          IDS%code%version,dim1,dum1,status)
      if (ual_debug =='yes') write(*,*) & 
      'Get IDS%code%version'
   endif   

! Get code/parameters
   call get_dimension(idx,path, "code"//"/parameters",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%code%parameters(dim1))
      call get_Vect1d_string(idx,path, "code"//"/parameters", &
          IDS%code%parameters,dim1,dum1,status)
      if (ual_debug =='yes') write(*,*) & 
      'Get IDS%code%parameters'
   endif   

! Get code/output_flag
   call get_dimension(idx,path, "code"//"/output_flag",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%code%output_flag(dim1))
      call get_vect1d_int(idx,path,"code"//"/output_flag", &
      IDS%code%output_flag &
      ,dim1,dum1,status)
      if (ual_debug =='yes') write(*,*) &  
      'Get IDS%code%output_flag'
   endif        

! Get time
   call get_dimension(idx,path, "time",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%time(dim1))
      call get_vect1d_double(idx,path,"time", &
      IDS%time &
      ,dim1,dum1,status)
      if (ual_debug =='yes') write(*,*) &  
      'Get IDS%time'
   endif        

call end_IDS_get(idx, path)      

return
end subroutine ids_get_equilibrium



!!!!!! Routines to GET one time slice of a IDS, with time interpolation -->
subroutine ids_get_slice_equilibrium(idx,path,  IDS, twant, interpol)

use ids_schemas
implicit none

character*(*) :: path
integer :: status, interpol, idx, lenstring, istring
real(DP) :: twant,tret
character(len=3)::ual_debug

integer :: int0D
integer,pointer :: vect1DInt(:), vect2DInt(:,:), vect3DInt(:,:,:) => null()
integer :: ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7,dum1,dum2,dum3,dum4,dum5,dum6,dum7
real(DP) :: double0D
real(DP), pointer :: vect1DDouble(:), time(:), vect2DDouble(:,:), vect3DDouble(:,:,:), vect4DDouble(:,:,:,:) => null()
real(DP), pointer :: vect5DDouble(:,:,:,:,:), vect6DDouble(:,:,:,:,:,:) => null()
character(len=132), dimension(:), pointer :: stringans => null()
character(len=100000)::longstring
character(len=300) :: timepath    
integer :: obj_single_time,obj1,obj2,obj3,obj4,obj5,obj6,obj7
integer :: dimObj1,dimObj2,dimObj3,dimObj4,dimObj5,dimObj6,dimObj7
integer :: i1,i2,i3,i4,i5,i6,i7

integer :: itime_slice
integer :: ix_point
integer :: istrike_point
integer :: iprofiles_2d

type(ids_equilibrium) :: IDS      

call getenv('ual_debug',ual_debug)


call begin_IDS_Get_Slice(idx,path, twant,status)
if (status.EQ.0) then
	      
! Get ids_properties/comment
longstring = ' '    
call get_string(idx,path, "ids_properties"//"/comment",longstring, status)       
if (status.EQ.0) then
   lenstring = len_trim(longstring)
   allocate(IDS%ids_properties%comment(floor(real(lenstring/132))+1))
   if (lenstring <= 132) then             
      IDS%ids_properties%comment = trim(longstring)
   else
      do istring=1,floor(real(lenstring/132))+1
          IDS%ids_properties%comment(istring) = trim(longstring(1+(istring-1)*132 : istring*132)) 
      enddo
   endif
   if (ual_debug =='yes') write(*,*) & 
      'Get IDS%ids_properties%comment',IDS%ids_properties%comment
endif   

! Get ids_properties/homogeneous_time
call get_int(idx,path, "ids_properties"//"/homogeneous_time",int0d,status)
if (status.EQ.0) then
   IDS%ids_properties%homogeneous_time = int0d
   if (ual_debug =='yes') write(*,*) & 
      'Get IDS%ids_properties%homogeneous_time'
endif                 

! Get ids_properties/cocos
call get_int(idx,path, "ids_properties"//"/cocos",int0d,status)
if (status.EQ.0) then
   IDS%ids_properties%cocos = int0d
   if (ual_debug =='yes') write(*,*) & 
      'Get IDS%ids_properties%cocos'
endif                 

! Get vacuum_toroidal_field/r0
call get_double(idx,path, "vacuum_toroidal_field"//"/r0",double0d,status)
if (status.EQ.0) then
   IDS%vacuum_toroidal_field%r0 = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get IDS%vacuum_toroidal_field%r0'
endif                 

! Get vacuum_toroidal_field/b0
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       
       timepath="time"
       
   else       
       timepath="time"
   endif   

! Get vacuum_toroidal_field/b0
   call get_dimension(idx,path, "vacuum_toroidal_field"//"/b0",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%vacuum_toroidal_field%b0(1))
      call get_double_slice(idx,path,"vacuum_toroidal_field"//"/b0", &
      trim(timepath),&
      IDS%vacuum_toroidal_field%b0 &
      ,twant,tret,interpol,status)
      if (ual_debug =='yes') write(*,*) &  
      'Get IDS%vacuum_toroidal_field%b0'
   endif        

! Structure array of type 3 : time_slice
call get_object_slice(idx,path,"time_slice",twant,obj_single_time,status) ! read the timed block containing a single slice
if (status.EQ.0) then
   call get_object_from_object(idx,obj_single_time,"ALLTIMES",1,obj1,status)   ! Even if obj_single_time contains a single slice, the slice has to be extracted like this as obj1
   if (status.EQ.0) then
      call get_object_dim(idx,obj1,dimObj1)
      if (dimObj1.GT.0) then
         allocate(ids%time_slice(1))
         if (ual_debug =='yes') write(*,*) &
            'Get_slice ids%time_slice'
               
! Get time_slice/boundary/type 
call get_int_from_object(idx,obj1,"time_slice/boundary/type",1,int0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%type = int0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%type'
endif                 

! Get time_slice/boundary/lcfs/r
call get_dimension_from_object(idx,obj1,"time_slice/boundary/lcfs/r",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%boundary%lcfs%r(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/boundary/lcfs/r",1, &
   ids%time_slice(1)%boundary%lcfs%r,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%boundary%lcfs%r'
endif        

! Get time_slice/boundary/lcfs/z
call get_dimension_from_object(idx,obj1,"time_slice/boundary/lcfs/z",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%boundary%lcfs%z(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/boundary/lcfs/z",1, &
   ids%time_slice(1)%boundary%lcfs%z,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%boundary%lcfs%z'
endif        

! Get time_slice/boundary/geometric_axis/r 
call get_double_from_object(idx,obj1,"time_slice/boundary/geometric_axis/r",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%geometric_axis%r = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%geometric_axis%r'
endif

! Get time_slice/boundary/geometric_axis/z 
call get_double_from_object(idx,obj1,"time_slice/boundary/geometric_axis/z",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%geometric_axis%z = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%geometric_axis%z'
endif

! Get time_slice/boundary/a_minor 
call get_double_from_object(idx,obj1,"time_slice/boundary/a_minor",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%a_minor = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%a_minor'
endif

! Get time_slice/boundary/elongation 
call get_double_from_object(idx,obj1,"time_slice/boundary/elongation",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%elongation = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%elongation'
endif

! Get time_slice/boundary/elongation_upper 
call get_double_from_object(idx,obj1,"time_slice/boundary/elongation_upper",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%elongation_upper = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%elongation_upper'
endif

! Get time_slice/boundary/elongation_lower 
call get_double_from_object(idx,obj1,"time_slice/boundary/elongation_lower",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%elongation_lower = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%elongation_lower'
endif

! Get time_slice/boundary/triangularity 
call get_double_from_object(idx,obj1,"time_slice/boundary/triangularity",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%triangularity = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%triangularity'
endif

! Get time_slice/boundary/triangularity_upper 
call get_double_from_object(idx,obj1,"time_slice/boundary/triangularity_upper",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%triangularity_upper = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%triangularity_upper'
endif

! Get time_slice/boundary/triangularity_lower 
call get_double_from_object(idx,obj1,"time_slice/boundary/triangularity_lower",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%triangularity_lower = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%triangularity_lower'
endif

! Get time_slice/boundary/x_point
call get_object_from_object(idx, obj1, "time_slice/boundary/x_point", 1, obj2,status)


if (status.EQ.0) then
   call get_object_dim(idx,obj2,dimObj2)
   if (dimObj2.GT.0) then
      if (associated(ids%time_slice(1)%boundary%x_point)) then ! does this array already exist? (timed and non timed parts can share the same array)
         if (size(ids%time_slice(1)%boundary%x_point).NE.dimObj2) then ! then it must have the right number of elements
            write(*,*) "Error in get: array of structures has different number of timed and nontimed elements for time_slice/boundary/x_point"
            deallocate(ids%time_slice(1)%boundary%x_point)
         endif
      else
         allocate(ids%time_slice(1)%boundary%x_point(dimObj2))
      endif
      if (associated(ids%time_slice(1)%boundary%x_point)) then
         do i2 = 1,dimObj2     ! process array elements
            
! Get time_slice/boundary/x_point/r 
call get_double_from_object(idx,obj2,"x_point/r",i2 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%x_point(i2)%r = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%x_point(i2)%r'
endif

! Get time_slice/boundary/x_point/z 
call get_double_from_object(idx,obj2,"x_point/z",i2 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%x_point(i2)%z = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%x_point(i2)%z'
endif

         enddo
      endif
   endif
endif
  
! Get time_slice/boundary/strike_point
call get_object_from_object(idx, obj1, "time_slice/boundary/strike_point", 1, obj2,status)


if (status.EQ.0) then
   call get_object_dim(idx,obj2,dimObj2)
   if (dimObj2.GT.0) then
      if (associated(ids%time_slice(1)%boundary%strike_point)) then ! does this array already exist? (timed and non timed parts can share the same array)
         if (size(ids%time_slice(1)%boundary%strike_point).NE.dimObj2) then ! then it must have the right number of elements
            write(*,*) "Error in get: array of structures has different number of timed and nontimed elements for time_slice/boundary/strike_point"
            deallocate(ids%time_slice(1)%boundary%strike_point)
         endif
      else
         allocate(ids%time_slice(1)%boundary%strike_point(dimObj2))
      endif
      if (associated(ids%time_slice(1)%boundary%strike_point)) then
         do i2 = 1,dimObj2     ! process array elements
            
! Get time_slice/boundary/strike_point/r 
call get_double_from_object(idx,obj2,"strike_point/r",i2 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%strike_point(i2)%r = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%strike_point(i2)%r'
endif

! Get time_slice/boundary/strike_point/z 
call get_double_from_object(idx,obj2,"strike_point/z",i2 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%strike_point(i2)%z = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%strike_point(i2)%z'
endif

         enddo
      endif
   endif
endif
  
! Get time_slice/boundary/active_limiter_point/r 
call get_double_from_object(idx,obj1,"time_slice/boundary/active_limiter_point/r",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%active_limiter_point%r = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%active_limiter_point%r'
endif

! Get time_slice/boundary/active_limiter_point/z 
call get_double_from_object(idx,obj1,"time_slice/boundary/active_limiter_point/z",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%boundary%active_limiter_point%z = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%boundary%active_limiter_point%z'
endif

! Get time_slice/global_quantities/beta_pol 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/beta_pol",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%beta_pol = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%beta_pol'
endif

! Get time_slice/global_quantities/beta_tor 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/beta_tor",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%beta_tor = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%beta_tor'
endif

! Get time_slice/global_quantities/beta_normal 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/beta_normal",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%beta_normal = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%beta_normal'
endif

! Get time_slice/global_quantities/ip 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/ip",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%ip = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%ip'
endif

! Get time_slice/global_quantities/li_3 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/li_3",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%li_3 = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%li_3'
endif

! Get time_slice/global_quantities/volume 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/volume",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%volume = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%volume'
endif

! Get time_slice/global_quantities/area 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/area",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%area = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%area'
endif

! Get time_slice/global_quantities/surface 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/surface",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%surface = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%surface'
endif

! Get time_slice/global_quantities/length_pol 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/length_pol",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%length_pol = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%length_pol'
endif

! Get time_slice/global_quantities/psi_axis 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/psi_axis",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%psi_axis = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%psi_axis'
endif

! Get time_slice/global_quantities/psi_boundary 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/psi_boundary",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%psi_boundary = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%psi_boundary'
endif

! Get time_slice/global_quantities/magnetic_axis/r 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/r",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%magnetic_axis%r = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%magnetic_axis%r'
endif

! Get time_slice/global_quantities/magnetic_axis/z 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/z",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%magnetic_axis%z = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%magnetic_axis%z'
endif

! Get time_slice/global_quantities/magnetic_axis/b_tor 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/b_tor",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%magnetic_axis%b_tor = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%magnetic_axis%b_tor'
endif

! Get time_slice/global_quantities/q_axis 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/q_axis",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%q_axis = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%q_axis'
endif

! Get time_slice/global_quantities/q_95 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/q_95",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%q_95 = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%q_95'
endif

! Get time_slice/global_quantities/q_min/value 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/q_min/value",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%q_min%value = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%q_min%value'
endif

! Get time_slice/global_quantities/q_min/rho_tor_norm 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/q_min/rho_tor_norm",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%q_min%rho_tor_norm = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%q_min%rho_tor_norm'
endif

! Get time_slice/global_quantities/w_mhd 
call get_double_from_object(idx,obj1,"time_slice/global_quantities/w_mhd",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%global_quantities%w_mhd = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%global_quantities%w_mhd'
endif

! Get time_slice/profiles_1d/psi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/psi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%psi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/psi",1, &
   ids%time_slice(1)%profiles_1d%psi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%psi'
endif        

! Get time_slice/profiles_1d/phi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/phi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%phi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/phi",1, &
   ids%time_slice(1)%profiles_1d%phi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%phi'
endif        

! Get time_slice/profiles_1d/pressure
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/pressure",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%pressure(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/pressure",1, &
   ids%time_slice(1)%profiles_1d%pressure,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%pressure'
endif        

! Get time_slice/profiles_1d/f
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/f",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%f(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/f",1, &
   ids%time_slice(1)%profiles_1d%f,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%f'
endif        

! Get time_slice/profiles_1d/dpressure_dpsi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/dpressure_dpsi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%dpressure_dpsi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/dpressure_dpsi",1, &
   ids%time_slice(1)%profiles_1d%dpressure_dpsi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%dpressure_dpsi'
endif        

! Get time_slice/profiles_1d/f_df_dpsi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/f_df_dpsi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%f_df_dpsi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/f_df_dpsi",1, &
   ids%time_slice(1)%profiles_1d%f_df_dpsi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%f_df_dpsi'
endif        

! Get time_slice/profiles_1d/j_tor
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/j_tor",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%j_tor(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/j_tor",1, &
   ids%time_slice(1)%profiles_1d%j_tor,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%j_tor'
endif        

! Get time_slice/profiles_1d/j_parallel
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/j_parallel",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%j_parallel(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/j_parallel",1, &
   ids%time_slice(1)%profiles_1d%j_parallel,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%j_parallel'
endif        

! Get time_slice/profiles_1d/q
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/q",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%q(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/q",1, &
   ids%time_slice(1)%profiles_1d%q,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%q'
endif        

! Get time_slice/profiles_1d/magnetic_shear
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/magnetic_shear",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%magnetic_shear(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/magnetic_shear",1, &
   ids%time_slice(1)%profiles_1d%magnetic_shear,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%magnetic_shear'
endif        

! Get time_slice/profiles_1d/r_inboard
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/r_inboard",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%r_inboard(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/r_inboard",1, &
   ids%time_slice(1)%profiles_1d%r_inboard,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%r_inboard'
endif        

! Get time_slice/profiles_1d/r_outboard
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/r_outboard",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%r_outboard(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/r_outboard",1, &
   ids%time_slice(1)%profiles_1d%r_outboard,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%r_outboard'
endif        

! Get time_slice/profiles_1d/rho_tor
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/rho_tor",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%rho_tor(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/rho_tor",1, &
   ids%time_slice(1)%profiles_1d%rho_tor,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%rho_tor'
endif        

! Get time_slice/profiles_1d/rho_tor_norm
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/rho_tor_norm",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%rho_tor_norm(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/rho_tor_norm",1, &
   ids%time_slice(1)%profiles_1d%rho_tor_norm,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%rho_tor_norm'
endif        

! Get time_slice/profiles_1d/dpsi_drho_tor
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/dpsi_drho_tor",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%dpsi_drho_tor(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/dpsi_drho_tor",1, &
   ids%time_slice(1)%profiles_1d%dpsi_drho_tor,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%dpsi_drho_tor'
endif        

! Get time_slice/profiles_1d/elongation
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/elongation",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%elongation(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/elongation",1, &
   ids%time_slice(1)%profiles_1d%elongation,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%elongation'
endif        

! Get time_slice/profiles_1d/triangularity_upper
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/triangularity_upper",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%triangularity_upper(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/triangularity_upper",1, &
   ids%time_slice(1)%profiles_1d%triangularity_upper,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%triangularity_upper'
endif        

! Get time_slice/profiles_1d/triangularity_lower
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/triangularity_lower",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%triangularity_lower(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/triangularity_lower",1, &
   ids%time_slice(1)%profiles_1d%triangularity_lower,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%triangularity_lower'
endif        

! Get time_slice/profiles_1d/volume
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/volume",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%volume(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/volume",1, &
   ids%time_slice(1)%profiles_1d%volume,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%volume'
endif        

! Get time_slice/profiles_1d/dvolume_dpsi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/dvolume_dpsi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%dvolume_dpsi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/dvolume_dpsi",1, &
   ids%time_slice(1)%profiles_1d%dvolume_dpsi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%dvolume_dpsi'
endif        

! Get time_slice/profiles_1d/dvolume_drho_tor
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/dvolume_drho_tor",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%dvolume_drho_tor(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/dvolume_drho_tor",1, &
   ids%time_slice(1)%profiles_1d%dvolume_drho_tor,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%dvolume_drho_tor'
endif        

! Get time_slice/profiles_1d/area
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/area",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%area(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/area",1, &
   ids%time_slice(1)%profiles_1d%area,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%area'
endif        

! Get time_slice/profiles_1d/darea_dpsi
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/darea_dpsi",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%darea_dpsi(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/darea_dpsi",1, &
   ids%time_slice(1)%profiles_1d%darea_dpsi,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%darea_dpsi'
endif        

! Get time_slice/profiles_1d/surface
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/surface",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%surface(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/surface",1, &
   ids%time_slice(1)%profiles_1d%surface,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%surface'
endif        

! Get time_slice/profiles_1d/trapped_fraction
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/trapped_fraction",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%trapped_fraction(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/trapped_fraction",1, &
   ids%time_slice(1)%profiles_1d%trapped_fraction,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%trapped_fraction'
endif        

! Get time_slice/profiles_1d/gm1
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm1",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%gm1(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm1",1, &
   ids%time_slice(1)%profiles_1d%gm1,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%gm1'
endif        

! Get time_slice/profiles_1d/gm2
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm2",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%gm2(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm2",1, &
   ids%time_slice(1)%profiles_1d%gm2,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%gm2'
endif        

! Get time_slice/profiles_1d/gm3
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm3",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%gm3(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm3",1, &
   ids%time_slice(1)%profiles_1d%gm3,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%gm3'
endif        

! Get time_slice/profiles_1d/gm4
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm4",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%gm4(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm4",1, &
   ids%time_slice(1)%profiles_1d%gm4,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%gm4'
endif        

! Get time_slice/profiles_1d/gm5
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm5",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%gm5(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm5",1, &
   ids%time_slice(1)%profiles_1d%gm5,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%gm5'
endif        

! Get time_slice/profiles_1d/gm6
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm6",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%gm6(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm6",1, &
   ids%time_slice(1)%profiles_1d%gm6,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%gm6'
endif        

! Get time_slice/profiles_1d/gm7
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm7",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%gm7(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm7",1, &
   ids%time_slice(1)%profiles_1d%gm7,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%gm7'
endif        

! Get time_slice/profiles_1d/gm8
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm8",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%gm8(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm8",1, &
   ids%time_slice(1)%profiles_1d%gm8,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%gm8'
endif        

! Get time_slice/profiles_1d/gm9
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/gm9",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%gm9(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/gm9",1, &
   ids%time_slice(1)%profiles_1d%gm9,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%gm9'
endif        

! Get time_slice/profiles_1d/b_average
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/b_average",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%b_average(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/b_average",1, &
   ids%time_slice(1)%profiles_1d%b_average,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%b_average'
endif        

! Get time_slice/profiles_1d/b_min
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/b_min",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%b_min(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/b_min",1, &
   ids%time_slice(1)%profiles_1d%b_min,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%b_min'
endif        

! Get time_slice/profiles_1d/b_max
call get_dimension_from_object(idx,obj1,"time_slice/profiles_1d/b_max",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_1d%b_max(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/profiles_1d/b_max",1, &
   ids%time_slice(1)%profiles_1d%b_max,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_1d%b_max'
endif        

! Get time_slice/profiles_2d
call get_object_from_object(idx, obj1, "time_slice/profiles_2d", 1, obj2,status)


if (status.EQ.0) then
   call get_object_dim(idx,obj2,dimObj2)
   if (dimObj2.GT.0) then
      if (associated(ids%time_slice(1)%profiles_2d)) then ! does this array already exist? (timed and non timed parts can share the same array)
         if (size(ids%time_slice(1)%profiles_2d).NE.dimObj2) then ! then it must have the right number of elements
            write(*,*) "Error in get: array of structures has different number of timed and nontimed elements for time_slice/profiles_2d"
            deallocate(ids%time_slice(1)%profiles_2d)
         endif
      else
         allocate(ids%time_slice(1)%profiles_2d(dimObj2))
      endif
      if (associated(ids%time_slice(1)%profiles_2d)) then
         do i2 = 1,dimObj2     ! process array elements
            
! Get time_slice/profiles_2d/grid_type/name  
longstring = ' '
call get_string_from_object(idx,obj2,"profiles_2d/grid_type/name",i2,longstring,status)
if (status.EQ.0) then
   lenstring = len_trim(longstring)      
   allocate(ids%time_slice(1)%profiles_2d(i2)%grid_type%name(floor(real(lenstring/132))+1))
   if (lenstring <= 132) then             
      ids%time_slice(1)%profiles_2d(i2)%grid_type%name = trim(longstring)
   else
      do istring=1,floor(real(lenstring/132))+1
          ids%time_slice(1)%profiles_2d(i2)%grid_type%name(istring) = trim(longstring(1+(istring-1)*132 : istring*132))
      enddo
   endif
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%grid_type%name'
endif   

! Get time_slice/profiles_2d/grid_type/index 
call get_int_from_object(idx,obj2,"profiles_2d/grid_type/index",i2,int0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%profiles_2d(i2)%grid_type%index = int0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%grid_type%index'
endif                 

! Get time_slice/profiles_2d/grid_type/description  
longstring = ' '
call get_string_from_object(idx,obj2,"profiles_2d/grid_type/description",i2,longstring,status)
if (status.EQ.0) then
   lenstring = len_trim(longstring)      
   allocate(ids%time_slice(1)%profiles_2d(i2)%grid_type%description(floor(real(lenstring/132))+1))
   if (lenstring <= 132) then             
      ids%time_slice(1)%profiles_2d(i2)%grid_type%description = trim(longstring)
   else
      do istring=1,floor(real(lenstring/132))+1
          ids%time_slice(1)%profiles_2d(i2)%grid_type%description(istring) = trim(longstring(1+(istring-1)*132 : istring*132))
      enddo
   endif
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%grid_type%description'
endif   

! Get time_slice/profiles_2d/grid/dim1
call get_dimension_from_object(idx,obj2,"profiles_2d/grid/dim1",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%grid%dim1(dim1))
   call get_vect1d_double_from_object(idx,obj2,"profiles_2d/grid/dim1",i2, &
   ids%time_slice(1)%profiles_2d(i2)%grid%dim1,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_2d(i2)%grid%dim1'
endif        

! Get time_slice/profiles_2d/grid/dim2
call get_dimension_from_object(idx,obj2,"profiles_2d/grid/dim2",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%grid%dim2(dim1))
   call get_vect1d_double_from_object(idx,obj2,"profiles_2d/grid/dim2",i2, &
   ids%time_slice(1)%profiles_2d(i2)%grid%dim2,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%profiles_2d(i2)%grid%dim2'
endif        

! Get time_slice/profiles_2d/r        
call get_dimension_from_object(idx,obj2,"profiles_2d/r",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%r(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/r",i2, &
   ids%time_slice(1)%profiles_2d(i2)%r, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%r'
endif 

! Get time_slice/profiles_2d/z        
call get_dimension_from_object(idx,obj2,"profiles_2d/z",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%z(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/z",i2, &
   ids%time_slice(1)%profiles_2d(i2)%z, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%z'
endif 

! Get time_slice/profiles_2d/psi        
call get_dimension_from_object(idx,obj2,"profiles_2d/psi",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%psi(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/psi",i2, &
   ids%time_slice(1)%profiles_2d(i2)%psi, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%psi'
endif 

! Get time_slice/profiles_2d/theta        
call get_dimension_from_object(idx,obj2,"profiles_2d/theta",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%theta(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/theta",i2, &
   ids%time_slice(1)%profiles_2d(i2)%theta, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%theta'
endif 

! Get time_slice/profiles_2d/phi        
call get_dimension_from_object(idx,obj2,"profiles_2d/phi",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%phi(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/phi",i2, &
   ids%time_slice(1)%profiles_2d(i2)%phi, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%phi'
endif 

! Get time_slice/profiles_2d/j_tor        
call get_dimension_from_object(idx,obj2,"profiles_2d/j_tor",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%j_tor(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/j_tor",i2, &
   ids%time_slice(1)%profiles_2d(i2)%j_tor, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%j_tor'
endif 

! Get time_slice/profiles_2d/j_parallel        
call get_dimension_from_object(idx,obj2,"profiles_2d/j_parallel",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%j_parallel(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/j_parallel",i2, &
   ids%time_slice(1)%profiles_2d(i2)%j_parallel, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%j_parallel'
endif 

! Get time_slice/profiles_2d/b_r        
call get_dimension_from_object(idx,obj2,"profiles_2d/b_r",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%b_r(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/b_r",i2, &
   ids%time_slice(1)%profiles_2d(i2)%b_r, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%b_r'
endif 

! Get time_slice/profiles_2d/b_z        
call get_dimension_from_object(idx,obj2,"profiles_2d/b_z",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%b_z(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/b_z",i2, &
   ids%time_slice(1)%profiles_2d(i2)%b_z, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%b_z'
endif 

! Get time_slice/profiles_2d/b_tor        
call get_dimension_from_object(idx,obj2,"profiles_2d/b_tor",i2,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%profiles_2d(i2)%b_tor(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj2,"profiles_2d/b_tor",i2, &
   ids%time_slice(1)%profiles_2d(i2)%b_tor, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%profiles_2d(i2)%b_tor'
endif 

         enddo
      endif
   endif
endif
  
! Get time_slice/coordinate_system/grid_type/name  
longstring = ' '
call get_string_from_object(idx,obj1,"time_slice/coordinate_system/grid_type/name",1,longstring,status)
if (status.EQ.0) then
   lenstring = len_trim(longstring)      
   allocate(ids%time_slice(1)%coordinate_system%grid_type%name(floor(real(lenstring/132))+1))
   if (lenstring <= 132) then             
      ids%time_slice(1)%coordinate_system%grid_type%name = trim(longstring)
   else
      do istring=1,floor(real(lenstring/132))+1
          ids%time_slice(1)%coordinate_system%grid_type%name(istring) = trim(longstring(1+(istring-1)*132 : istring*132))
      enddo
   endif
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%grid_type%name'
endif   

! Get time_slice/coordinate_system/grid_type/index 
call get_int_from_object(idx,obj1,"time_slice/coordinate_system/grid_type/index",1,int0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%coordinate_system%grid_type%index = int0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%grid_type%index'
endif                 

! Get time_slice/coordinate_system/grid_type/description  
longstring = ' '
call get_string_from_object(idx,obj1,"time_slice/coordinate_system/grid_type/description",1,longstring,status)
if (status.EQ.0) then
   lenstring = len_trim(longstring)      
   allocate(ids%time_slice(1)%coordinate_system%grid_type%description(floor(real(lenstring/132))+1))
   if (lenstring <= 132) then             
      ids%time_slice(1)%coordinate_system%grid_type%description = trim(longstring)
   else
      do istring=1,floor(real(lenstring/132))+1
          ids%time_slice(1)%coordinate_system%grid_type%description(istring) = trim(longstring(1+(istring-1)*132 : istring*132))
      enddo
   endif
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%grid_type%description'
endif   

! Get time_slice/coordinate_system/grid/dim1
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/grid/dim1",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%grid%dim1(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/coordinate_system/grid/dim1",1, &
   ids%time_slice(1)%coordinate_system%grid%dim1,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%coordinate_system%grid%dim1'
endif        

! Get time_slice/coordinate_system/grid/dim2
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/grid/dim2",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%grid%dim2(dim1))
   call get_vect1d_double_from_object(idx,obj1,"time_slice/coordinate_system/grid/dim2",1, &
   ids%time_slice(1)%coordinate_system%grid%dim2,dim1,dum1,status)
   if (ual_debug =='yes') write(*,*) &  
      'Get ids%time_slice(1)%coordinate_system%grid%dim2'
endif        

! Get time_slice/coordinate_system/r        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/r",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%r(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/r",1, &
   ids%time_slice(1)%coordinate_system%r, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%r'
endif 

! Get time_slice/coordinate_system/z        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/z",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%z(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/z",1, &
   ids%time_slice(1)%coordinate_system%z, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%z'
endif 

! Get time_slice/coordinate_system/jacobian        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/jacobian",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%jacobian(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/jacobian",1, &
   ids%time_slice(1)%coordinate_system%jacobian, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%jacobian'
endif 

! Get time_slice/coordinate_system/g_11        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_11",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%g_11(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_11",1, &
   ids%time_slice(1)%coordinate_system%g_11, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%g_11'
endif 

! Get time_slice/coordinate_system/g_12        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_12",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%g_12(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_12",1, &
   ids%time_slice(1)%coordinate_system%g_12, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%g_12'
endif 

! Get time_slice/coordinate_system/g_13        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_13",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%g_13(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_13",1, &
   ids%time_slice(1)%coordinate_system%g_13, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%g_13'
endif 

! Get time_slice/coordinate_system/g_22        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_22",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%g_22(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_22",1, &
   ids%time_slice(1)%coordinate_system%g_22, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%g_22'
endif 

! Get time_slice/coordinate_system/g_23        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_23",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%g_23(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_23",1, &
   ids%time_slice(1)%coordinate_system%g_23, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%g_23'
endif 

! Get time_slice/coordinate_system/g_33        
call get_dimension_from_object(idx,obj1,"time_slice/coordinate_system/g_33",1,ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
if (dim1.GT.0) then
   allocate(ids%time_slice(1)%coordinate_system%g_33(dim1,dim2))
   call get_vect2d_double_from_object(idx,obj1,"time_slice/coordinate_system/g_33",1, &
   ids%time_slice(1)%coordinate_system%g_33, &
   dim1,dim2,dum1,dum2,status)
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%coordinate_system%g_33'
endif 

! Get time_slice/time 
call get_double_from_object(idx,obj1,"time_slice/time",1 ,double0d,status)
if (status.EQ.0) then
   ids%time_slice(1)%time = double0d
   if (ual_debug =='yes') write(*,*) & 
      'Get ids%time_slice(1)%time'
endif

      else
         if (associated(IDS%time_slice)) then
            deallocate(IDS%time_slice);
         endif
      endif
   endif
   call release_object(idx,obj_single_time)
endif

! Get code/name
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       
       timepath="time"
       
   else       
       timepath="time"
   endif   

   call get_dimension(idx,path, "code"//"/name",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%code%name(1))
      call get_string_slice(idx,path, "code"//"/name", &
          trim(timepath),&
          IDS%code%name,twant,tret,interpol,status)
      if (ual_debug =='yes') write(*,*) & 
      'Get IDS%code%name'
   endif   

! Get code/version
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       
       timepath="time"
       
   else       
       timepath="time"
   endif   

   call get_dimension(idx,path, "code"//"/version",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%code%version(1))
      call get_string_slice(idx,path, "code"//"/version", &
          trim(timepath),&
          IDS%code%version,twant,tret,interpol,status)
      if (ual_debug =='yes') write(*,*) & 
      'Get IDS%code%version'
   endif   

! Get code/parameters
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       
       timepath="time"
       
   else       
       timepath="time"
   endif   

   call get_dimension(idx,path, "code"//"/parameters",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%code%parameters(1))
      call get_string_slice(idx,path, "code"//"/parameters", &
          trim(timepath),&
          IDS%code%parameters,twant,tret,interpol,status)
      if (ual_debug =='yes') write(*,*) & 
      'Get IDS%code%parameters'
   endif   

! Get code/output_flag
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       
       timepath="time"
       
   else       
       timepath="time"
   endif   

   call get_dimension(idx,path, "code"//"/output_flag",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%code%output_flag(1))
      call get_int_slice(idx,path,"code"//"/output_flag", &
      trim(timepath),&
      IDS%code%output_flag &
      ,twant,tret,interpol,status)
      if (ual_debug =='yes') write(*,*) &  
      'Get IDS%code%output_flag'
   endif        

! Get time
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       timepath="time"
   else
       timepath="time"
   endif   

! Get time
   call get_dimension(idx,path, "time",ndims,dim1,dim2,dim3,dim4,dim5,dim6,dim7)
   if (dim1.GT.0) then
      allocate(IDS%time(1))
      call get_double_slice(idx,path,"time", &
      trim(timepath),&
      IDS%time &
      ,twant,tret,interpol,status)
      if (ual_debug =='yes') write(*,*) &  
      'Get IDS%time'
   endif        

else
   write(*,*) 'Get slice impossible, IDS is missing or requested time slice is not within the time interval of the IDS'
endif
call end_IDS_Get_Slice(idx,path)	      

return
end subroutine ids_GET_SLICE_equilibrium

!!!!!! Routines to PUT the full IDS
subroutine ids_put_equilibrium(idx, path,  IDS)

use ids_schemas
implicit none
!integer, parameter :: DP=kind(1.0D0)

character*(*) :: path
integer :: idx, lentime
character(len=3)::ual_debug

type(ids_equilibrium) :: IDS       

! internal variables declaration
integer :: itime
integer :: int0D
integer,pointer :: vect1DInt(:), vect2DInt(:,:), vect3DInt(:,:,:) => null()
integer :: i,dim1,dim2,dim3,dim4,dim5,dim6,dim7, lenstring, istring
integer, pointer :: dimtab(:) => null()
real(DP) :: double0D
real(DP), pointer :: vect1DDouble(:), time(:), vect2DDouble(:,:), vect3DDouble(:,:,:), vect4DDouble(:,:,:,:) => null()
real(DP), pointer :: vect5DDouble(:,:,:,:,:), vect6DDouble(:,:,:,:,:,:) => null()
character(len=132), dimension(:), pointer :: stri => null()
character(len=100000)::longstring
character(len=300) :: timepath    
integer :: obj_all_times,obj1,obj2,obj3,obj4,obj5,obj6,obj7
integer :: i1,i2,i3,i4,i5,i6,i7

integer :: itime_slice
integer :: ix_point
integer :: istrike_point
integer :: iprofiles_2d

call getenv('ual_debug',ual_debug) ! Debug flag

! Systematic delete of the previous IDS, in case it existed
call ids_delete(idx,path,IDS)

! And systematic erase of the previous changes in cache
! The ids_discard routines are obsolete, do not call them anymore
! call ids_discard(idx,path,IDS)


if ((IDS%IDS_Properties%homogeneous_time.EQ.1).AND.(.NOT.(associated(IDS%time)))) then
   write(*,*) "ERROR : the IDS%time vector of an homogeneous_time IDS must be associated"
   return
endif


call begin_ids_put(idx, path)


! Put ids_properties/comment
if (associated(IDS%ids_properties%comment)) then
   longstring = ' '    
   lenstring = size(IDS%ids_properties%comment)      
   if (lenstring.EQ.1) then             
      longstring = trim(IDS%ids_properties%comment(1))
   else
      do istring=1,lenstring
          longstring(1+(istring-1)*132 : istring*132) = IDS%ids_properties%comment(istring)
      enddo
   endif
   call put_string(idx,path, "ids_properties"//"/comment",trim(longstring))       ! should clean up longstring after that, or send to the put only the right length, which has been updated
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%ids_properties%comment',IDS%ids_properties%comment
endif

! Put ids_properties/homogeneous_time
if (IDS%ids_properties%homogeneous_time.NE.-999999999) then
   call put_int(idx,path, "ids_properties"//"/homogeneous_time",&
       IDS%ids_properties%homogeneous_time)        
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%ids_properties%homogeneous_time',IDS%ids_properties%homogeneous_time
endif

! Put ids_properties/cocos
if (IDS%ids_properties%cocos.NE.-999999999) then
   call put_int(idx,path, "ids_properties"//"/cocos",&
       IDS%ids_properties%cocos)        
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%ids_properties%cocos',IDS%ids_properties%cocos
endif

! Put vacuum_toroidal_field/r0
if (IDS%vacuum_toroidal_field%r0.NE.-9.D40) then 
   call put_double(idx,path, "vacuum_toroidal_field"//"/r0",IDS%vacuum_toroidal_field%r0)  
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%vacuum_toroidal_field%r0',IDS%vacuum_toroidal_field%r0
endif

! Put vacuum_toroidal_field/b0
if (associated(IDS%vacuum_toroidal_field%b0)) then
   
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       
       timepath="time"
       call begin_IDS_put_timed(idx, path,size( IDS%time), IDS%time)
       
   else
        timepath="time"
        call begin_IDS_put_timed(idx, path,size(IDS%time),IDS%time)
   endif   
      
   call put_vect1d_double(idx,path, "vacuum_toroidal_field"//"/b0",&
   trim(timepath),&
   IDS%vacuum_toroidal_field%b0,&
   size(IDS%vacuum_toroidal_field%b0),1)
   
   call end_IDS_put_timed(idx, path)
   
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%vacuum_toroidal_field%b0',IDS%vacuum_toroidal_field%b0
endif

! Structure array of type 3 : time_slice
if (associated(IDS%time_slice)) then
   call begin_object(idx,-1,1,path//"/time_slice",TIMED_CLEAR,obj_all_times)
   do i1 = 1,size(IDS%time_slice)
      call begin_object(idx,obj_all_times,i1,"ALLTIMES",TIMED,obj1)
         
! Put time_slice/boundary/type        
if (IDS%time_slice(i1)%boundary%type.NE.-999999999) then
   call put_int_in_object(idx,obj1,"time_slice/boundary/type",1,IDS%time_slice(i1)%boundary%type)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%type',IDS%time_slice(i1)%boundary%type
endif

! Put time_slice/boundary/lcfs/r

if (associated(IDS%time_slice(i1)%boundary%lcfs%r)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/boundary/lcfs/r",1,&
   IDS%time_slice(i1)%boundary%lcfs%r,&
   size(IDS%time_slice(i1)%boundary%lcfs%r))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%lcfs%r',IDS%time_slice(i1)%boundary%lcfs%r
endif

! Put time_slice/boundary/lcfs/z

if (associated(IDS%time_slice(i1)%boundary%lcfs%z)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/boundary/lcfs/z",1,&
   IDS%time_slice(i1)%boundary%lcfs%z,&
   size(IDS%time_slice(i1)%boundary%lcfs%z))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%lcfs%z',IDS%time_slice(i1)%boundary%lcfs%z
endif

! Put time_slice/boundary/geometric_axis/r
if (IDS%time_slice(i1)%boundary%geometric_axis%r.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/geometric_axis/r",1,IDS%time_slice(i1)%boundary%geometric_axis%r)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%geometric_axis%r',IDS%time_slice(i1)%boundary%geometric_axis%r
endif

! Put time_slice/boundary/geometric_axis/z
if (IDS%time_slice(i1)%boundary%geometric_axis%z.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/geometric_axis/z",1,IDS%time_slice(i1)%boundary%geometric_axis%z)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%geometric_axis%z',IDS%time_slice(i1)%boundary%geometric_axis%z
endif

! Put time_slice/boundary/a_minor
if (IDS%time_slice(i1)%boundary%a_minor.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/a_minor",1,IDS%time_slice(i1)%boundary%a_minor)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%a_minor',IDS%time_slice(i1)%boundary%a_minor
endif

! Put time_slice/boundary/elongation
if (IDS%time_slice(i1)%boundary%elongation.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/elongation",1,IDS%time_slice(i1)%boundary%elongation)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%elongation',IDS%time_slice(i1)%boundary%elongation
endif

! Put time_slice/boundary/elongation_upper
if (IDS%time_slice(i1)%boundary%elongation_upper.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/elongation_upper",1,IDS%time_slice(i1)%boundary%elongation_upper)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%elongation_upper',IDS%time_slice(i1)%boundary%elongation_upper
endif

! Put time_slice/boundary/elongation_lower
if (IDS%time_slice(i1)%boundary%elongation_lower.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/elongation_lower",1,IDS%time_slice(i1)%boundary%elongation_lower)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%elongation_lower',IDS%time_slice(i1)%boundary%elongation_lower
endif

! Put time_slice/boundary/triangularity
if (IDS%time_slice(i1)%boundary%triangularity.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/triangularity",1,IDS%time_slice(i1)%boundary%triangularity)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%triangularity',IDS%time_slice(i1)%boundary%triangularity
endif

! Put time_slice/boundary/triangularity_upper
if (IDS%time_slice(i1)%boundary%triangularity_upper.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/triangularity_upper",1,IDS%time_slice(i1)%boundary%triangularity_upper)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%triangularity_upper',IDS%time_slice(i1)%boundary%triangularity_upper
endif

! Put time_slice/boundary/triangularity_lower
if (IDS%time_slice(i1)%boundary%triangularity_lower.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/triangularity_lower",1,IDS%time_slice(i1)%boundary%triangularity_lower)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%triangularity_lower',IDS%time_slice(i1)%boundary%triangularity_lower
endif

! Put time_slice/boundary/x_point
if (associated(IDS%time_slice(i1)%boundary%x_point)) then
call begin_object(idx,obj1,1,"time_slice/boundary/x_point",NON_TIMED,obj2)
! Start to declare a nested Type 2 Aos 
   do i2 = 1,size(IDS%time_slice(i1)%boundary%x_point)
         
! Put time_slice/boundary/x_point/r
if (IDS%time_slice(i1)%boundary%x_point(i2)%r.NE.-9.D40) then
   call put_double_in_object(idx,obj2,"x_point/r",i2,IDS%time_slice(i1)%boundary%x_point(i2)%r)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%x_point(i2)%r',IDS%time_slice(i1)%boundary%x_point(i2)%r
endif

! Put time_slice/boundary/x_point/z
if (IDS%time_slice(i1)%boundary%x_point(i2)%z.NE.-9.D40) then
   call put_double_in_object(idx,obj2,"x_point/z",i2,IDS%time_slice(i1)%boundary%x_point(i2)%z)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%x_point(i2)%z',IDS%time_slice(i1)%boundary%x_point(i2)%z
endif

   enddo
call put_object_in_object(idx,obj1, "time_slice/boundary/x_point",1, obj2)
endif
    
! Put time_slice/boundary/strike_point
if (associated(IDS%time_slice(i1)%boundary%strike_point)) then
call begin_object(idx,obj1,1,"time_slice/boundary/strike_point",NON_TIMED,obj2)
! Start to declare a nested Type 2 Aos 
   do i2 = 1,size(IDS%time_slice(i1)%boundary%strike_point)
         
! Put time_slice/boundary/strike_point/r
if (IDS%time_slice(i1)%boundary%strike_point(i2)%r.NE.-9.D40) then
   call put_double_in_object(idx,obj2,"strike_point/r",i2,IDS%time_slice(i1)%boundary%strike_point(i2)%r)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%strike_point(i2)%r',IDS%time_slice(i1)%boundary%strike_point(i2)%r
endif

! Put time_slice/boundary/strike_point/z
if (IDS%time_slice(i1)%boundary%strike_point(i2)%z.NE.-9.D40) then
   call put_double_in_object(idx,obj2,"strike_point/z",i2,IDS%time_slice(i1)%boundary%strike_point(i2)%z)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%strike_point(i2)%z',IDS%time_slice(i1)%boundary%strike_point(i2)%z
endif

   enddo
call put_object_in_object(idx,obj1, "time_slice/boundary/strike_point",1, obj2)
endif
    
! Put time_slice/boundary/active_limiter_point/r
if (IDS%time_slice(i1)%boundary%active_limiter_point%r.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/active_limiter_point/r",1,IDS%time_slice(i1)%boundary%active_limiter_point%r)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%active_limiter_point%r',IDS%time_slice(i1)%boundary%active_limiter_point%r
endif

! Put time_slice/boundary/active_limiter_point/z
if (IDS%time_slice(i1)%boundary%active_limiter_point%z.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/active_limiter_point/z",1,IDS%time_slice(i1)%boundary%active_limiter_point%z)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%boundary%active_limiter_point%z',IDS%time_slice(i1)%boundary%active_limiter_point%z
endif

! Put time_slice/global_quantities/beta_pol
if (IDS%time_slice(i1)%global_quantities%beta_pol.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/beta_pol",1,IDS%time_slice(i1)%global_quantities%beta_pol)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%beta_pol',IDS%time_slice(i1)%global_quantities%beta_pol
endif

! Put time_slice/global_quantities/beta_tor
if (IDS%time_slice(i1)%global_quantities%beta_tor.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/beta_tor",1,IDS%time_slice(i1)%global_quantities%beta_tor)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%beta_tor',IDS%time_slice(i1)%global_quantities%beta_tor
endif

! Put time_slice/global_quantities/beta_normal
if (IDS%time_slice(i1)%global_quantities%beta_normal.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/beta_normal",1,IDS%time_slice(i1)%global_quantities%beta_normal)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%beta_normal',IDS%time_slice(i1)%global_quantities%beta_normal
endif

! Put time_slice/global_quantities/ip
if (IDS%time_slice(i1)%global_quantities%ip.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/ip",1,IDS%time_slice(i1)%global_quantities%ip)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%ip',IDS%time_slice(i1)%global_quantities%ip
endif

! Put time_slice/global_quantities/li_3
if (IDS%time_slice(i1)%global_quantities%li_3.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/li_3",1,IDS%time_slice(i1)%global_quantities%li_3)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%li_3',IDS%time_slice(i1)%global_quantities%li_3
endif

! Put time_slice/global_quantities/volume
if (IDS%time_slice(i1)%global_quantities%volume.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/volume",1,IDS%time_slice(i1)%global_quantities%volume)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%volume',IDS%time_slice(i1)%global_quantities%volume
endif

! Put time_slice/global_quantities/area
if (IDS%time_slice(i1)%global_quantities%area.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/area",1,IDS%time_slice(i1)%global_quantities%area)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%area',IDS%time_slice(i1)%global_quantities%area
endif

! Put time_slice/global_quantities/surface
if (IDS%time_slice(i1)%global_quantities%surface.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/surface",1,IDS%time_slice(i1)%global_quantities%surface)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%surface',IDS%time_slice(i1)%global_quantities%surface
endif

! Put time_slice/global_quantities/length_pol
if (IDS%time_slice(i1)%global_quantities%length_pol.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/length_pol",1,IDS%time_slice(i1)%global_quantities%length_pol)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%length_pol',IDS%time_slice(i1)%global_quantities%length_pol
endif

! Put time_slice/global_quantities/psi_axis
if (IDS%time_slice(i1)%global_quantities%psi_axis.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/psi_axis",1,IDS%time_slice(i1)%global_quantities%psi_axis)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%psi_axis',IDS%time_slice(i1)%global_quantities%psi_axis
endif

! Put time_slice/global_quantities/psi_boundary
if (IDS%time_slice(i1)%global_quantities%psi_boundary.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/psi_boundary",1,IDS%time_slice(i1)%global_quantities%psi_boundary)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%psi_boundary',IDS%time_slice(i1)%global_quantities%psi_boundary
endif

! Put time_slice/global_quantities/magnetic_axis/r
if (IDS%time_slice(i1)%global_quantities%magnetic_axis%r.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/r",1,IDS%time_slice(i1)%global_quantities%magnetic_axis%r)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%magnetic_axis%r',IDS%time_slice(i1)%global_quantities%magnetic_axis%r
endif

! Put time_slice/global_quantities/magnetic_axis/z
if (IDS%time_slice(i1)%global_quantities%magnetic_axis%z.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/z",1,IDS%time_slice(i1)%global_quantities%magnetic_axis%z)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%magnetic_axis%z',IDS%time_slice(i1)%global_quantities%magnetic_axis%z
endif

! Put time_slice/global_quantities/magnetic_axis/b_tor
if (IDS%time_slice(i1)%global_quantities%magnetic_axis%b_tor.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/b_tor",1,IDS%time_slice(i1)%global_quantities%magnetic_axis%b_tor)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%magnetic_axis%b_tor',IDS%time_slice(i1)%global_quantities%magnetic_axis%b_tor
endif

! Put time_slice/global_quantities/q_axis
if (IDS%time_slice(i1)%global_quantities%q_axis.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/q_axis",1,IDS%time_slice(i1)%global_quantities%q_axis)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%q_axis',IDS%time_slice(i1)%global_quantities%q_axis
endif

! Put time_slice/global_quantities/q_95
if (IDS%time_slice(i1)%global_quantities%q_95.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/q_95",1,IDS%time_slice(i1)%global_quantities%q_95)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%q_95',IDS%time_slice(i1)%global_quantities%q_95
endif

! Put time_slice/global_quantities/q_min/value
if (IDS%time_slice(i1)%global_quantities%q_min%value.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/q_min/value",1,IDS%time_slice(i1)%global_quantities%q_min%value)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%q_min%value',IDS%time_slice(i1)%global_quantities%q_min%value
endif

! Put time_slice/global_quantities/q_min/rho_tor_norm
if (IDS%time_slice(i1)%global_quantities%q_min%rho_tor_norm.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/q_min/rho_tor_norm",1,IDS%time_slice(i1)%global_quantities%q_min%rho_tor_norm)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%q_min%rho_tor_norm',IDS%time_slice(i1)%global_quantities%q_min%rho_tor_norm
endif

! Put time_slice/global_quantities/w_mhd
if (IDS%time_slice(i1)%global_quantities%w_mhd.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/w_mhd",1,IDS%time_slice(i1)%global_quantities%w_mhd)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%global_quantities%w_mhd',IDS%time_slice(i1)%global_quantities%w_mhd
endif

! Put time_slice/profiles_1d/psi

if (associated(IDS%time_slice(i1)%profiles_1d%psi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/psi",1,&
   IDS%time_slice(i1)%profiles_1d%psi,&
   size(IDS%time_slice(i1)%profiles_1d%psi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%psi',IDS%time_slice(i1)%profiles_1d%psi
endif

! Put time_slice/profiles_1d/phi

if (associated(IDS%time_slice(i1)%profiles_1d%phi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/phi",1,&
   IDS%time_slice(i1)%profiles_1d%phi,&
   size(IDS%time_slice(i1)%profiles_1d%phi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%phi',IDS%time_slice(i1)%profiles_1d%phi
endif

! Put time_slice/profiles_1d/pressure

if (associated(IDS%time_slice(i1)%profiles_1d%pressure)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/pressure",1,&
   IDS%time_slice(i1)%profiles_1d%pressure,&
   size(IDS%time_slice(i1)%profiles_1d%pressure))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%pressure',IDS%time_slice(i1)%profiles_1d%pressure
endif

! Put time_slice/profiles_1d/f

if (associated(IDS%time_slice(i1)%profiles_1d%f)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/f",1,&
   IDS%time_slice(i1)%profiles_1d%f,&
   size(IDS%time_slice(i1)%profiles_1d%f))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%f',IDS%time_slice(i1)%profiles_1d%f
endif

! Put time_slice/profiles_1d/dpressure_dpsi

if (associated(IDS%time_slice(i1)%profiles_1d%dpressure_dpsi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/dpressure_dpsi",1,&
   IDS%time_slice(i1)%profiles_1d%dpressure_dpsi,&
   size(IDS%time_slice(i1)%profiles_1d%dpressure_dpsi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%dpressure_dpsi',IDS%time_slice(i1)%profiles_1d%dpressure_dpsi
endif

! Put time_slice/profiles_1d/f_df_dpsi

if (associated(IDS%time_slice(i1)%profiles_1d%f_df_dpsi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/f_df_dpsi",1,&
   IDS%time_slice(i1)%profiles_1d%f_df_dpsi,&
   size(IDS%time_slice(i1)%profiles_1d%f_df_dpsi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%f_df_dpsi',IDS%time_slice(i1)%profiles_1d%f_df_dpsi
endif

! Put time_slice/profiles_1d/j_tor

if (associated(IDS%time_slice(i1)%profiles_1d%j_tor)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/j_tor",1,&
   IDS%time_slice(i1)%profiles_1d%j_tor,&
   size(IDS%time_slice(i1)%profiles_1d%j_tor))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%j_tor',IDS%time_slice(i1)%profiles_1d%j_tor
endif

! Put time_slice/profiles_1d/j_parallel

if (associated(IDS%time_slice(i1)%profiles_1d%j_parallel)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/j_parallel",1,&
   IDS%time_slice(i1)%profiles_1d%j_parallel,&
   size(IDS%time_slice(i1)%profiles_1d%j_parallel))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%j_parallel',IDS%time_slice(i1)%profiles_1d%j_parallel
endif

! Put time_slice/profiles_1d/q

if (associated(IDS%time_slice(i1)%profiles_1d%q)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/q",1,&
   IDS%time_slice(i1)%profiles_1d%q,&
   size(IDS%time_slice(i1)%profiles_1d%q))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%q',IDS%time_slice(i1)%profiles_1d%q
endif

! Put time_slice/profiles_1d/magnetic_shear

if (associated(IDS%time_slice(i1)%profiles_1d%magnetic_shear)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/magnetic_shear",1,&
   IDS%time_slice(i1)%profiles_1d%magnetic_shear,&
   size(IDS%time_slice(i1)%profiles_1d%magnetic_shear))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%magnetic_shear',IDS%time_slice(i1)%profiles_1d%magnetic_shear
endif

! Put time_slice/profiles_1d/r_inboard

if (associated(IDS%time_slice(i1)%profiles_1d%r_inboard)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/r_inboard",1,&
   IDS%time_slice(i1)%profiles_1d%r_inboard,&
   size(IDS%time_slice(i1)%profiles_1d%r_inboard))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%r_inboard',IDS%time_slice(i1)%profiles_1d%r_inboard
endif

! Put time_slice/profiles_1d/r_outboard

if (associated(IDS%time_slice(i1)%profiles_1d%r_outboard)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/r_outboard",1,&
   IDS%time_slice(i1)%profiles_1d%r_outboard,&
   size(IDS%time_slice(i1)%profiles_1d%r_outboard))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%r_outboard',IDS%time_slice(i1)%profiles_1d%r_outboard
endif

! Put time_slice/profiles_1d/rho_tor

if (associated(IDS%time_slice(i1)%profiles_1d%rho_tor)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/rho_tor",1,&
   IDS%time_slice(i1)%profiles_1d%rho_tor,&
   size(IDS%time_slice(i1)%profiles_1d%rho_tor))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%rho_tor',IDS%time_slice(i1)%profiles_1d%rho_tor
endif

! Put time_slice/profiles_1d/rho_tor_norm

if (associated(IDS%time_slice(i1)%profiles_1d%rho_tor_norm)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/rho_tor_norm",1,&
   IDS%time_slice(i1)%profiles_1d%rho_tor_norm,&
   size(IDS%time_slice(i1)%profiles_1d%rho_tor_norm))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%rho_tor_norm',IDS%time_slice(i1)%profiles_1d%rho_tor_norm
endif

! Put time_slice/profiles_1d/dpsi_drho_tor

if (associated(IDS%time_slice(i1)%profiles_1d%dpsi_drho_tor)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/dpsi_drho_tor",1,&
   IDS%time_slice(i1)%profiles_1d%dpsi_drho_tor,&
   size(IDS%time_slice(i1)%profiles_1d%dpsi_drho_tor))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%dpsi_drho_tor',IDS%time_slice(i1)%profiles_1d%dpsi_drho_tor
endif

! Put time_slice/profiles_1d/elongation

if (associated(IDS%time_slice(i1)%profiles_1d%elongation)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/elongation",1,&
   IDS%time_slice(i1)%profiles_1d%elongation,&
   size(IDS%time_slice(i1)%profiles_1d%elongation))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%elongation',IDS%time_slice(i1)%profiles_1d%elongation
endif

! Put time_slice/profiles_1d/triangularity_upper

if (associated(IDS%time_slice(i1)%profiles_1d%triangularity_upper)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/triangularity_upper",1,&
   IDS%time_slice(i1)%profiles_1d%triangularity_upper,&
   size(IDS%time_slice(i1)%profiles_1d%triangularity_upper))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%triangularity_upper',IDS%time_slice(i1)%profiles_1d%triangularity_upper
endif

! Put time_slice/profiles_1d/triangularity_lower

if (associated(IDS%time_slice(i1)%profiles_1d%triangularity_lower)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/triangularity_lower",1,&
   IDS%time_slice(i1)%profiles_1d%triangularity_lower,&
   size(IDS%time_slice(i1)%profiles_1d%triangularity_lower))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%triangularity_lower',IDS%time_slice(i1)%profiles_1d%triangularity_lower
endif

! Put time_slice/profiles_1d/volume

if (associated(IDS%time_slice(i1)%profiles_1d%volume)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/volume",1,&
   IDS%time_slice(i1)%profiles_1d%volume,&
   size(IDS%time_slice(i1)%profiles_1d%volume))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%volume',IDS%time_slice(i1)%profiles_1d%volume
endif

! Put time_slice/profiles_1d/dvolume_dpsi

if (associated(IDS%time_slice(i1)%profiles_1d%dvolume_dpsi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/dvolume_dpsi",1,&
   IDS%time_slice(i1)%profiles_1d%dvolume_dpsi,&
   size(IDS%time_slice(i1)%profiles_1d%dvolume_dpsi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%dvolume_dpsi',IDS%time_slice(i1)%profiles_1d%dvolume_dpsi
endif

! Put time_slice/profiles_1d/dvolume_drho_tor

if (associated(IDS%time_slice(i1)%profiles_1d%dvolume_drho_tor)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/dvolume_drho_tor",1,&
   IDS%time_slice(i1)%profiles_1d%dvolume_drho_tor,&
   size(IDS%time_slice(i1)%profiles_1d%dvolume_drho_tor))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%dvolume_drho_tor',IDS%time_slice(i1)%profiles_1d%dvolume_drho_tor
endif

! Put time_slice/profiles_1d/area

if (associated(IDS%time_slice(i1)%profiles_1d%area)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/area",1,&
   IDS%time_slice(i1)%profiles_1d%area,&
   size(IDS%time_slice(i1)%profiles_1d%area))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%area',IDS%time_slice(i1)%profiles_1d%area
endif

! Put time_slice/profiles_1d/darea_dpsi

if (associated(IDS%time_slice(i1)%profiles_1d%darea_dpsi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/darea_dpsi",1,&
   IDS%time_slice(i1)%profiles_1d%darea_dpsi,&
   size(IDS%time_slice(i1)%profiles_1d%darea_dpsi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%darea_dpsi',IDS%time_slice(i1)%profiles_1d%darea_dpsi
endif

! Put time_slice/profiles_1d/surface

if (associated(IDS%time_slice(i1)%profiles_1d%surface)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/surface",1,&
   IDS%time_slice(i1)%profiles_1d%surface,&
   size(IDS%time_slice(i1)%profiles_1d%surface))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%surface',IDS%time_slice(i1)%profiles_1d%surface
endif

! Put time_slice/profiles_1d/trapped_fraction

if (associated(IDS%time_slice(i1)%profiles_1d%trapped_fraction)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/trapped_fraction",1,&
   IDS%time_slice(i1)%profiles_1d%trapped_fraction,&
   size(IDS%time_slice(i1)%profiles_1d%trapped_fraction))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%trapped_fraction',IDS%time_slice(i1)%profiles_1d%trapped_fraction
endif

! Put time_slice/profiles_1d/gm1

if (associated(IDS%time_slice(i1)%profiles_1d%gm1)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm1",1,&
   IDS%time_slice(i1)%profiles_1d%gm1,&
   size(IDS%time_slice(i1)%profiles_1d%gm1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%gm1',IDS%time_slice(i1)%profiles_1d%gm1
endif

! Put time_slice/profiles_1d/gm2

if (associated(IDS%time_slice(i1)%profiles_1d%gm2)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm2",1,&
   IDS%time_slice(i1)%profiles_1d%gm2,&
   size(IDS%time_slice(i1)%profiles_1d%gm2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%gm2',IDS%time_slice(i1)%profiles_1d%gm2
endif

! Put time_slice/profiles_1d/gm3

if (associated(IDS%time_slice(i1)%profiles_1d%gm3)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm3",1,&
   IDS%time_slice(i1)%profiles_1d%gm3,&
   size(IDS%time_slice(i1)%profiles_1d%gm3))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%gm3',IDS%time_slice(i1)%profiles_1d%gm3
endif

! Put time_slice/profiles_1d/gm4

if (associated(IDS%time_slice(i1)%profiles_1d%gm4)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm4",1,&
   IDS%time_slice(i1)%profiles_1d%gm4,&
   size(IDS%time_slice(i1)%profiles_1d%gm4))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%gm4',IDS%time_slice(i1)%profiles_1d%gm4
endif

! Put time_slice/profiles_1d/gm5

if (associated(IDS%time_slice(i1)%profiles_1d%gm5)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm5",1,&
   IDS%time_slice(i1)%profiles_1d%gm5,&
   size(IDS%time_slice(i1)%profiles_1d%gm5))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%gm5',IDS%time_slice(i1)%profiles_1d%gm5
endif

! Put time_slice/profiles_1d/gm6

if (associated(IDS%time_slice(i1)%profiles_1d%gm6)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm6",1,&
   IDS%time_slice(i1)%profiles_1d%gm6,&
   size(IDS%time_slice(i1)%profiles_1d%gm6))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%gm6',IDS%time_slice(i1)%profiles_1d%gm6
endif

! Put time_slice/profiles_1d/gm7

if (associated(IDS%time_slice(i1)%profiles_1d%gm7)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm7",1,&
   IDS%time_slice(i1)%profiles_1d%gm7,&
   size(IDS%time_slice(i1)%profiles_1d%gm7))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%gm7',IDS%time_slice(i1)%profiles_1d%gm7
endif

! Put time_slice/profiles_1d/gm8

if (associated(IDS%time_slice(i1)%profiles_1d%gm8)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm8",1,&
   IDS%time_slice(i1)%profiles_1d%gm8,&
   size(IDS%time_slice(i1)%profiles_1d%gm8))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%gm8',IDS%time_slice(i1)%profiles_1d%gm8
endif

! Put time_slice/profiles_1d/gm9

if (associated(IDS%time_slice(i1)%profiles_1d%gm9)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm9",1,&
   IDS%time_slice(i1)%profiles_1d%gm9,&
   size(IDS%time_slice(i1)%profiles_1d%gm9))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%gm9',IDS%time_slice(i1)%profiles_1d%gm9
endif

! Put time_slice/profiles_1d/b_average

if (associated(IDS%time_slice(i1)%profiles_1d%b_average)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/b_average",1,&
   IDS%time_slice(i1)%profiles_1d%b_average,&
   size(IDS%time_slice(i1)%profiles_1d%b_average))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%b_average',IDS%time_slice(i1)%profiles_1d%b_average
endif

! Put time_slice/profiles_1d/b_min

if (associated(IDS%time_slice(i1)%profiles_1d%b_min)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/b_min",1,&
   IDS%time_slice(i1)%profiles_1d%b_min,&
   size(IDS%time_slice(i1)%profiles_1d%b_min))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%b_min',IDS%time_slice(i1)%profiles_1d%b_min
endif

! Put time_slice/profiles_1d/b_max

if (associated(IDS%time_slice(i1)%profiles_1d%b_max)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/b_max",1,&
   IDS%time_slice(i1)%profiles_1d%b_max,&
   size(IDS%time_slice(i1)%profiles_1d%b_max))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_1d%b_max',IDS%time_slice(i1)%profiles_1d%b_max
endif

! Put time_slice/profiles_2d
if (associated(IDS%time_slice(i1)%profiles_2d)) then
call begin_object(idx,obj1,1,"time_slice/profiles_2d",NON_TIMED,obj2)
! Start to declare a nested Type 2 Aos 
   do i2 = 1,size(IDS%time_slice(i1)%profiles_2d)
         
! Put time_slice/profiles_2d/grid_type/name
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%grid_type%name)) then
   longstring = ' '    
   lenstring = size(IDS%time_slice(i1)%profiles_2d(i2)%grid_type%name)
   if (lenstring.EQ.1) then             
      longstring = trim(IDS%time_slice(i1)%profiles_2d(i2)%grid_type%name(1))
   else
      do istring=1,lenstring
          longstring(1+(istring-1)*132 : istring*132) = IDS%time_slice(i1)%profiles_2d(i2)%grid_type%name(istring)
      enddo
   endif
   call put_string_in_object(idx,obj2,"profiles_2d/grid_type/name",i2,trim(longstring))       ! should clean up longstring after that, or send to the put only the right length, which has been updated
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%grid_type%name',IDS%time_slice(i1)%profiles_2d(i2)%grid_type%name
endif

! Put time_slice/profiles_2d/grid_type/index        
if (IDS%time_slice(i1)%profiles_2d(i2)%grid_type%index.NE.-999999999) then
   call put_int_in_object(idx,obj2,"profiles_2d/grid_type/index",i2,IDS%time_slice(i1)%profiles_2d(i2)%grid_type%index)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%grid_type%index',IDS%time_slice(i1)%profiles_2d(i2)%grid_type%index
endif

! Put time_slice/profiles_2d/grid_type/description
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%grid_type%description)) then
   longstring = ' '    
   lenstring = size(IDS%time_slice(i1)%profiles_2d(i2)%grid_type%description)
   if (lenstring.EQ.1) then             
      longstring = trim(IDS%time_slice(i1)%profiles_2d(i2)%grid_type%description(1))
   else
      do istring=1,lenstring
          longstring(1+(istring-1)*132 : istring*132) = IDS%time_slice(i1)%profiles_2d(i2)%grid_type%description(istring)
      enddo
   endif
   call put_string_in_object(idx,obj2,"profiles_2d/grid_type/description",i2,trim(longstring))       ! should clean up longstring after that, or send to the put only the right length, which has been updated
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%grid_type%description',IDS%time_slice(i1)%profiles_2d(i2)%grid_type%description
endif

! Put time_slice/profiles_2d/grid/dim1

if (associated(IDS%time_slice(i1)%profiles_2d(i2)%grid%dim1)) then
   call put_vect1d_double_in_object(idx,obj2,"profiles_2d/grid/dim1",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%grid%dim1,&
   size(IDS%time_slice(i1)%profiles_2d(i2)%grid%dim1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%grid%dim1',IDS%time_slice(i1)%profiles_2d(i2)%grid%dim1
endif

! Put time_slice/profiles_2d/grid/dim2

if (associated(IDS%time_slice(i1)%profiles_2d(i2)%grid%dim2)) then
   call put_vect1d_double_in_object(idx,obj2,"profiles_2d/grid/dim2",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%grid%dim2,&
   size(IDS%time_slice(i1)%profiles_2d(i2)%grid%dim2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%grid%dim2',IDS%time_slice(i1)%profiles_2d(i2)%grid%dim2
endif

! Put time_slice/profiles_2d/r        
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%r)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/r",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%r, &
   size(IDS%time_slice(i1)%profiles_2d(i2)%r,1),&
   size(IDS%time_slice(i1)%profiles_2d(i2)%r,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%r',IDS%time_slice(i1)%profiles_2d(i2)%r
endif

! Put time_slice/profiles_2d/z        
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%z)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/z",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%z, &
   size(IDS%time_slice(i1)%profiles_2d(i2)%z,1),&
   size(IDS%time_slice(i1)%profiles_2d(i2)%z,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%z',IDS%time_slice(i1)%profiles_2d(i2)%z
endif

! Put time_slice/profiles_2d/psi        
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%psi)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/psi",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%psi, &
   size(IDS%time_slice(i1)%profiles_2d(i2)%psi,1),&
   size(IDS%time_slice(i1)%profiles_2d(i2)%psi,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%psi',IDS%time_slice(i1)%profiles_2d(i2)%psi
endif

! Put time_slice/profiles_2d/theta        
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%theta)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/theta",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%theta, &
   size(IDS%time_slice(i1)%profiles_2d(i2)%theta,1),&
   size(IDS%time_slice(i1)%profiles_2d(i2)%theta,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%theta',IDS%time_slice(i1)%profiles_2d(i2)%theta
endif

! Put time_slice/profiles_2d/phi        
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%phi)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/phi",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%phi, &
   size(IDS%time_slice(i1)%profiles_2d(i2)%phi,1),&
   size(IDS%time_slice(i1)%profiles_2d(i2)%phi,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%phi',IDS%time_slice(i1)%profiles_2d(i2)%phi
endif

! Put time_slice/profiles_2d/j_tor        
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%j_tor)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/j_tor",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%j_tor, &
   size(IDS%time_slice(i1)%profiles_2d(i2)%j_tor,1),&
   size(IDS%time_slice(i1)%profiles_2d(i2)%j_tor,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%j_tor',IDS%time_slice(i1)%profiles_2d(i2)%j_tor
endif

! Put time_slice/profiles_2d/j_parallel        
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%j_parallel)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/j_parallel",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%j_parallel, &
   size(IDS%time_slice(i1)%profiles_2d(i2)%j_parallel,1),&
   size(IDS%time_slice(i1)%profiles_2d(i2)%j_parallel,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%j_parallel',IDS%time_slice(i1)%profiles_2d(i2)%j_parallel
endif

! Put time_slice/profiles_2d/b_r        
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%b_r)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/b_r",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%b_r, &
   size(IDS%time_slice(i1)%profiles_2d(i2)%b_r,1),&
   size(IDS%time_slice(i1)%profiles_2d(i2)%b_r,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%b_r',IDS%time_slice(i1)%profiles_2d(i2)%b_r
endif

! Put time_slice/profiles_2d/b_z        
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%b_z)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/b_z",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%b_z, &
   size(IDS%time_slice(i1)%profiles_2d(i2)%b_z,1),&
   size(IDS%time_slice(i1)%profiles_2d(i2)%b_z,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%b_z',IDS%time_slice(i1)%profiles_2d(i2)%b_z
endif

! Put time_slice/profiles_2d/b_tor        
if (associated(IDS%time_slice(i1)%profiles_2d(i2)%b_tor)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/b_tor",i2,&
   IDS%time_slice(i1)%profiles_2d(i2)%b_tor, &
   size(IDS%time_slice(i1)%profiles_2d(i2)%b_tor,1),&
   size(IDS%time_slice(i1)%profiles_2d(i2)%b_tor,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%profiles_2d(i2)%b_tor',IDS%time_slice(i1)%profiles_2d(i2)%b_tor
endif

   enddo
call put_object_in_object(idx,obj1, "time_slice/profiles_2d",1, obj2)
endif
    
! Put time_slice/coordinate_system/grid_type/name
if (associated(IDS%time_slice(i1)%coordinate_system%grid_type%name)) then
   longstring = ' '    
   lenstring = size(IDS%time_slice(i1)%coordinate_system%grid_type%name)
   if (lenstring.EQ.1) then             
      longstring = trim(IDS%time_slice(i1)%coordinate_system%grid_type%name(1))
   else
      do istring=1,lenstring
          longstring(1+(istring-1)*132 : istring*132) = IDS%time_slice(i1)%coordinate_system%grid_type%name(istring)
      enddo
   endif
   call put_string_in_object(idx,obj1,"time_slice/coordinate_system/grid_type/name",1,trim(longstring))       ! should clean up longstring after that, or send to the put only the right length, which has been updated
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%grid_type%name',IDS%time_slice(i1)%coordinate_system%grid_type%name
endif

! Put time_slice/coordinate_system/grid_type/index        
if (IDS%time_slice(i1)%coordinate_system%grid_type%index.NE.-999999999) then
   call put_int_in_object(idx,obj1,"time_slice/coordinate_system/grid_type/index",1,IDS%time_slice(i1)%coordinate_system%grid_type%index)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%grid_type%index',IDS%time_slice(i1)%coordinate_system%grid_type%index
endif

! Put time_slice/coordinate_system/grid_type/description
if (associated(IDS%time_slice(i1)%coordinate_system%grid_type%description)) then
   longstring = ' '    
   lenstring = size(IDS%time_slice(i1)%coordinate_system%grid_type%description)
   if (lenstring.EQ.1) then             
      longstring = trim(IDS%time_slice(i1)%coordinate_system%grid_type%description(1))
   else
      do istring=1,lenstring
          longstring(1+(istring-1)*132 : istring*132) = IDS%time_slice(i1)%coordinate_system%grid_type%description(istring)
      enddo
   endif
   call put_string_in_object(idx,obj1,"time_slice/coordinate_system/grid_type/description",1,trim(longstring))       ! should clean up longstring after that, or send to the put only the right length, which has been updated
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%grid_type%description',IDS%time_slice(i1)%coordinate_system%grid_type%description
endif

! Put time_slice/coordinate_system/grid/dim1

if (associated(IDS%time_slice(i1)%coordinate_system%grid%dim1)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/coordinate_system/grid/dim1",1,&
   IDS%time_slice(i1)%coordinate_system%grid%dim1,&
   size(IDS%time_slice(i1)%coordinate_system%grid%dim1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%grid%dim1',IDS%time_slice(i1)%coordinate_system%grid%dim1
endif

! Put time_slice/coordinate_system/grid/dim2

if (associated(IDS%time_slice(i1)%coordinate_system%grid%dim2)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/coordinate_system/grid/dim2",1,&
   IDS%time_slice(i1)%coordinate_system%grid%dim2,&
   size(IDS%time_slice(i1)%coordinate_system%grid%dim2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%grid%dim2',IDS%time_slice(i1)%coordinate_system%grid%dim2
endif

! Put time_slice/coordinate_system/r        
if (associated(IDS%time_slice(i1)%coordinate_system%r)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/r",1,&
   IDS%time_slice(i1)%coordinate_system%r, &
   size(IDS%time_slice(i1)%coordinate_system%r,1),&
   size(IDS%time_slice(i1)%coordinate_system%r,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%r',IDS%time_slice(i1)%coordinate_system%r
endif

! Put time_slice/coordinate_system/z        
if (associated(IDS%time_slice(i1)%coordinate_system%z)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/z",1,&
   IDS%time_slice(i1)%coordinate_system%z, &
   size(IDS%time_slice(i1)%coordinate_system%z,1),&
   size(IDS%time_slice(i1)%coordinate_system%z,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%z',IDS%time_slice(i1)%coordinate_system%z
endif

! Put time_slice/coordinate_system/jacobian        
if (associated(IDS%time_slice(i1)%coordinate_system%jacobian)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/jacobian",1,&
   IDS%time_slice(i1)%coordinate_system%jacobian, &
   size(IDS%time_slice(i1)%coordinate_system%jacobian,1),&
   size(IDS%time_slice(i1)%coordinate_system%jacobian,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%jacobian',IDS%time_slice(i1)%coordinate_system%jacobian
endif

! Put time_slice/coordinate_system/g_11        
if (associated(IDS%time_slice(i1)%coordinate_system%g_11)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_11",1,&
   IDS%time_slice(i1)%coordinate_system%g_11, &
   size(IDS%time_slice(i1)%coordinate_system%g_11,1),&
   size(IDS%time_slice(i1)%coordinate_system%g_11,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%g_11',IDS%time_slice(i1)%coordinate_system%g_11
endif

! Put time_slice/coordinate_system/g_12        
if (associated(IDS%time_slice(i1)%coordinate_system%g_12)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_12",1,&
   IDS%time_slice(i1)%coordinate_system%g_12, &
   size(IDS%time_slice(i1)%coordinate_system%g_12,1),&
   size(IDS%time_slice(i1)%coordinate_system%g_12,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%g_12',IDS%time_slice(i1)%coordinate_system%g_12
endif

! Put time_slice/coordinate_system/g_13        
if (associated(IDS%time_slice(i1)%coordinate_system%g_13)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_13",1,&
   IDS%time_slice(i1)%coordinate_system%g_13, &
   size(IDS%time_slice(i1)%coordinate_system%g_13,1),&
   size(IDS%time_slice(i1)%coordinate_system%g_13,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%g_13',IDS%time_slice(i1)%coordinate_system%g_13
endif

! Put time_slice/coordinate_system/g_22        
if (associated(IDS%time_slice(i1)%coordinate_system%g_22)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_22",1,&
   IDS%time_slice(i1)%coordinate_system%g_22, &
   size(IDS%time_slice(i1)%coordinate_system%g_22,1),&
   size(IDS%time_slice(i1)%coordinate_system%g_22,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%g_22',IDS%time_slice(i1)%coordinate_system%g_22
endif

! Put time_slice/coordinate_system/g_23        
if (associated(IDS%time_slice(i1)%coordinate_system%g_23)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_23",1,&
   IDS%time_slice(i1)%coordinate_system%g_23, &
   size(IDS%time_slice(i1)%coordinate_system%g_23,1),&
   size(IDS%time_slice(i1)%coordinate_system%g_23,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%g_23',IDS%time_slice(i1)%coordinate_system%g_23
endif

! Put time_slice/coordinate_system/g_33        
if (associated(IDS%time_slice(i1)%coordinate_system%g_33)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_33",1,&
   IDS%time_slice(i1)%coordinate_system%g_33, &
   size(IDS%time_slice(i1)%coordinate_system%g_33,1),&
   size(IDS%time_slice(i1)%coordinate_system%g_33,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%coordinate_system%g_33',IDS%time_slice(i1)%coordinate_system%g_33
endif

! Put time_slice/time
if (IDS%time_slice(i1)%time.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/time",1,IDS%time_slice(i1)%time)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(i1)%time',IDS%time_slice(i1)%time
endif

      call put_object_in_object(idx,obj_all_times,"ALLTIMES",i1,obj1)
   enddo
   ! Store time of the array of structure (hidden variable for the user, but used by the UAL for future get_slice operations)
    allocate(time(size(IDS%time_slice)))

   if (IDS%time_slice(1)%time.EQ.-9.D40) then ! Check the presence of a time vector at the root of the AoS (on the first index only)
      if (IDS%IDS_Properties%homogeneous_time.EQ.1) then
         time = ids%time ! Use the general time vector of the IDS to fill time
      else
         write(*,*) "ERROR : the time vector of the type 3 array of structure time_slice must be filled"
         return
      endif
   else 
      do i1 = 1,size(IDS%time_slice) ! the AoS time vector is there, fill time with it
         time(i1) = IDS%time_slice(i1)%time
      enddo
   endif

    timepath="time_slice/time"
    call begin_IDS_put_timed(idx, path,size(time),time)
    call put_vect1d_double(idx,path, trim(timepath),&
        trim(timepath),&
        time,&
        size(time),1)
    call end_IDS_put_timed(idx, path)
    if (ual_debug =='yes') write(*,*) & 
      'Put  IDS%time_slice(:)%time',time
    deallocate(time)

    call put_object(idx,path,"time_slice",obj_all_times,1)
endif

! Put code/name
if (associated(IDS%code%name)) then
   
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       
       timepath="time"
       call begin_IDS_put_timed(idx, path,size( IDS%time), IDS%time)
       
   else       
       timepath="time"
       call begin_IDS_put_timed(idx, path,size(IDS%time),IDS%time)
   endif   
      
   dim1 = size(IDS%code%name)
   allocate(dimtab(dim1))
   do i=1,dim1
      dimtab(i) = len_trim(IDS%code%name(i))
   enddo
   call put_Vect1d_String(idx,path, "code"//"/name", &
          trim(timepath),IDS%code%name,dim1,dimtab,1)
   deallocate(dimtab)
   
   call end_IDS_put_timed(idx, path)
   
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%code%name'
endif

! Put code/version
if (associated(IDS%code%version)) then
   
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       
       timepath="time"
       call begin_IDS_put_timed(idx, path,size( IDS%time), IDS%time)
       
   else       
       timepath="time"
       call begin_IDS_put_timed(idx, path,size(IDS%time),IDS%time)
   endif   
      
   dim1 = size(IDS%code%version)
   allocate(dimtab(dim1))
   do i=1,dim1
      dimtab(i) = len_trim(IDS%code%version(i))
   enddo
   call put_Vect1d_String(idx,path, "code"//"/version", &
          trim(timepath),IDS%code%version,dim1,dimtab,1)
   deallocate(dimtab)
   
   call end_IDS_put_timed(idx, path)
   
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%code%version'
endif

! Put code/parameters
if (associated(IDS%code%parameters)) then
   
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       
       timepath="time"
       call begin_IDS_put_timed(idx, path,size( IDS%time), IDS%time)
       
   else       
       timepath="time"
       call begin_IDS_put_timed(idx, path,size(IDS%time),IDS%time)
   endif   
      
   dim1 = size(IDS%code%parameters)
   allocate(dimtab(dim1))
   do i=1,dim1
      dimtab(i) = len_trim(IDS%code%parameters(i))
   enddo
   call put_Vect1d_String(idx,path, "code"//"/parameters", &
          trim(timepath),IDS%code%parameters,dim1,dimtab,1)
   deallocate(dimtab)
   
   call end_IDS_put_timed(idx, path)
   
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%code%parameters'
endif

! Put code/output_flag
if (associated(IDS%code%output_flag)) then         
   
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       
       timepath="time"
       call begin_IDS_put_timed(idx, path,size( IDS%time), IDS%time)
       
   else
       timepath="time"
       call begin_IDS_put_timed(idx, path,size(IDS%time),IDS%time)
   endif   
      
   call put_vect1d_int(idx,path, "code"//"/output_flag",&
   trim(timepath), &
   IDS%code%output_flag,&
   size(IDS%code%output_flag),1) 
   
   call end_IDS_put_timed(idx, path)
   
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%code%output_flag',IDS%code%output_flag
endif

! Put time
if (associated(IDS%time)) then
   
   if (IDS%IDS_Properties%Homogeneous_time.EQ.0) then 
       timepath="time"
       call begin_IDS_put_timed(idx, path,size(IDS%time),IDS%time)
   else
       timepath="time"
       call begin_IDS_put_timed(idx, path,size(IDS%time),IDS%time)
   endif   
   
   call put_vect1d_double(idx,path, "time",&
   trim(timepath),&
   IDS%time,&
   size(IDS%time),1)
   
   call end_IDS_put_timed(idx, path)
   
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time',IDS%time
endif


call end_ids_put(idx, path)

return
end subroutine ids_put_equilibrium   
!!!!!! Routines to PUT_SLICE one time slice of a time-dependent IDS (affects only time-dependent fields)

subroutine ids_put_slice_equilibrium(idx,path,IDS)

use ids_schemas
implicit none

character*(*) :: path
integer :: idx, lentime
character(len=3)::ual_debug

type(ids_equilibrium) :: IDS

! internal variables declaration
integer :: itime
integer :: int0D
integer,pointer :: vect1DInt(:), vect2DInt(:,:), vect3DInt(:,:,:) => null()
integer :: i,dim1,dim2,dim3,dim4,dim5,dim6,dim7, lenstring, istring
integer, pointer :: dimtab(:) => null()
real(DP) :: double0D
real(DP), pointer :: vect1DDouble(:), time(:), vect2DDouble(:,:), vect3DDouble(:,:,:), vect4DDouble(:,:,:,:) => null()
real(DP), pointer :: vect5DDouble(:,:,:,:,:), vect6DDouble(:,:,:,:,:,:) => null()
character(len=132), dimension(:), pointer :: stri => null()
character(len=100000)::longstring 
character(len=300)::timepath   
integer :: obj_single_time,obj1,obj2,obj3,obj4,obj5,obj6,obj7
integer :: i1,i2,i3,i4,i5,i6,i7

integer :: itime_slice
integer :: ix_point
integer :: istrike_point
integer :: iprofiles_2d

call getenv('ual_debug',ual_debug) ! Debug flag

if (IDS%IDS_Properties%homogeneous_time.NE.1) then
   write(*,*) "ERROR : the PUT_SLICE routine works only for homogeneous time IDS: check ids_properties%homogeneous_time"
   return
endif

if (.NOT.(associated(IDS%time))) then
   write(*,*) "ERROR : the ids%time vector of an homogeneous_time IDS must be associated"
   return
endif


timepath = "time"
call begin_IDS_put_slice(idx, path)

! Put vacuum_toroidal_field/b0
if (associated(IDS%vacuum_toroidal_field%b0)) then
   call put_double_slice(idx,path, "vacuum_toroidal_field"//"/b0",&
   trim(timepath),&
   IDS%vacuum_toroidal_field%b0(1),&
   IDS%time(1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%vacuum_toroidal_field%b0',IDS%vacuum_toroidal_field%b0
endif

! Structure array of type 3 : time_slice
! timed arrays of structures must be put inside a time container, even if there is a single time */
if (associated(IDS%time_slice)) then
   call begin_object(idx,-1,1,path//"/time_slice",TIMED,obj_single_time);
   call begin_object(idx,obj_single_time,1,"ALLTIMES",TIMED,obj1)

      
! Put time_slice/boundary/type        
if (IDS%time_slice(1)%boundary%type.NE.-999999999) then
   call put_int_in_object(idx,obj1,"time_slice/boundary/type",1,IDS%time_slice(1)%boundary%type)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%type',IDS%time_slice(1)%boundary%type
endif

! Put time_slice/boundary/lcfs/r

if (associated(IDS%time_slice(1)%boundary%lcfs%r)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/boundary/lcfs/r",1,&
   IDS%time_slice(1)%boundary%lcfs%r,&
   size(IDS%time_slice(1)%boundary%lcfs%r))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%lcfs%r',IDS%time_slice(1)%boundary%lcfs%r
endif

! Put time_slice/boundary/lcfs/z

if (associated(IDS%time_slice(1)%boundary%lcfs%z)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/boundary/lcfs/z",1,&
   IDS%time_slice(1)%boundary%lcfs%z,&
   size(IDS%time_slice(1)%boundary%lcfs%z))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%lcfs%z',IDS%time_slice(1)%boundary%lcfs%z
endif

! Put time_slice/boundary/geometric_axis/r
if (IDS%time_slice(1)%boundary%geometric_axis%r.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/geometric_axis/r",1,IDS%time_slice(1)%boundary%geometric_axis%r)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%geometric_axis%r',IDS%time_slice(1)%boundary%geometric_axis%r
endif

! Put time_slice/boundary/geometric_axis/z
if (IDS%time_slice(1)%boundary%geometric_axis%z.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/geometric_axis/z",1,IDS%time_slice(1)%boundary%geometric_axis%z)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%geometric_axis%z',IDS%time_slice(1)%boundary%geometric_axis%z
endif

! Put time_slice/boundary/a_minor
if (IDS%time_slice(1)%boundary%a_minor.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/a_minor",1,IDS%time_slice(1)%boundary%a_minor)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%a_minor',IDS%time_slice(1)%boundary%a_minor
endif

! Put time_slice/boundary/elongation
if (IDS%time_slice(1)%boundary%elongation.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/elongation",1,IDS%time_slice(1)%boundary%elongation)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%elongation',IDS%time_slice(1)%boundary%elongation
endif

! Put time_slice/boundary/elongation_upper
if (IDS%time_slice(1)%boundary%elongation_upper.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/elongation_upper",1,IDS%time_slice(1)%boundary%elongation_upper)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%elongation_upper',IDS%time_slice(1)%boundary%elongation_upper
endif

! Put time_slice/boundary/elongation_lower
if (IDS%time_slice(1)%boundary%elongation_lower.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/elongation_lower",1,IDS%time_slice(1)%boundary%elongation_lower)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%elongation_lower',IDS%time_slice(1)%boundary%elongation_lower
endif

! Put time_slice/boundary/triangularity
if (IDS%time_slice(1)%boundary%triangularity.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/triangularity",1,IDS%time_slice(1)%boundary%triangularity)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%triangularity',IDS%time_slice(1)%boundary%triangularity
endif

! Put time_slice/boundary/triangularity_upper
if (IDS%time_slice(1)%boundary%triangularity_upper.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/triangularity_upper",1,IDS%time_slice(1)%boundary%triangularity_upper)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%triangularity_upper',IDS%time_slice(1)%boundary%triangularity_upper
endif

! Put time_slice/boundary/triangularity_lower
if (IDS%time_slice(1)%boundary%triangularity_lower.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/triangularity_lower",1,IDS%time_slice(1)%boundary%triangularity_lower)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%triangularity_lower',IDS%time_slice(1)%boundary%triangularity_lower
endif

! Put time_slice/boundary/x_point
if (associated(IDS%time_slice(1)%boundary%x_point)) then
call begin_object(idx,obj1,1,"time_slice/boundary/x_point",NON_TIMED,obj2)
! Start to declare a nested Type 2 Aos 
   do i2 = 1,size(IDS%time_slice(1)%boundary%x_point)
         
! Put time_slice/boundary/x_point/r
if (IDS%time_slice(1)%boundary%x_point(i2)%r.NE.-9.D40) then
   call put_double_in_object(idx,obj2,"x_point/r",i2,IDS%time_slice(1)%boundary%x_point(i2)%r)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%x_point(i2)%r',IDS%time_slice(1)%boundary%x_point(i2)%r
endif

! Put time_slice/boundary/x_point/z
if (IDS%time_slice(1)%boundary%x_point(i2)%z.NE.-9.D40) then
   call put_double_in_object(idx,obj2,"x_point/z",i2,IDS%time_slice(1)%boundary%x_point(i2)%z)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%x_point(i2)%z',IDS%time_slice(1)%boundary%x_point(i2)%z
endif

   enddo
call put_object_in_object(idx,obj1, "time_slice/boundary/x_point",1, obj2)
endif
    
! Put time_slice/boundary/strike_point
if (associated(IDS%time_slice(1)%boundary%strike_point)) then
call begin_object(idx,obj1,1,"time_slice/boundary/strike_point",NON_TIMED,obj2)
! Start to declare a nested Type 2 Aos 
   do i2 = 1,size(IDS%time_slice(1)%boundary%strike_point)
         
! Put time_slice/boundary/strike_point/r
if (IDS%time_slice(1)%boundary%strike_point(i2)%r.NE.-9.D40) then
   call put_double_in_object(idx,obj2,"strike_point/r",i2,IDS%time_slice(1)%boundary%strike_point(i2)%r)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%strike_point(i2)%r',IDS%time_slice(1)%boundary%strike_point(i2)%r
endif

! Put time_slice/boundary/strike_point/z
if (IDS%time_slice(1)%boundary%strike_point(i2)%z.NE.-9.D40) then
   call put_double_in_object(idx,obj2,"strike_point/z",i2,IDS%time_slice(1)%boundary%strike_point(i2)%z)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%strike_point(i2)%z',IDS%time_slice(1)%boundary%strike_point(i2)%z
endif

   enddo
call put_object_in_object(idx,obj1, "time_slice/boundary/strike_point",1, obj2)
endif
    
! Put time_slice/boundary/active_limiter_point/r
if (IDS%time_slice(1)%boundary%active_limiter_point%r.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/active_limiter_point/r",1,IDS%time_slice(1)%boundary%active_limiter_point%r)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%active_limiter_point%r',IDS%time_slice(1)%boundary%active_limiter_point%r
endif

! Put time_slice/boundary/active_limiter_point/z
if (IDS%time_slice(1)%boundary%active_limiter_point%z.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/boundary/active_limiter_point/z",1,IDS%time_slice(1)%boundary%active_limiter_point%z)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%boundary%active_limiter_point%z',IDS%time_slice(1)%boundary%active_limiter_point%z
endif

! Put time_slice/global_quantities/beta_pol
if (IDS%time_slice(1)%global_quantities%beta_pol.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/beta_pol",1,IDS%time_slice(1)%global_quantities%beta_pol)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%beta_pol',IDS%time_slice(1)%global_quantities%beta_pol
endif

! Put time_slice/global_quantities/beta_tor
if (IDS%time_slice(1)%global_quantities%beta_tor.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/beta_tor",1,IDS%time_slice(1)%global_quantities%beta_tor)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%beta_tor',IDS%time_slice(1)%global_quantities%beta_tor
endif

! Put time_slice/global_quantities/beta_normal
if (IDS%time_slice(1)%global_quantities%beta_normal.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/beta_normal",1,IDS%time_slice(1)%global_quantities%beta_normal)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%beta_normal',IDS%time_slice(1)%global_quantities%beta_normal
endif

! Put time_slice/global_quantities/ip
if (IDS%time_slice(1)%global_quantities%ip.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/ip",1,IDS%time_slice(1)%global_quantities%ip)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%ip',IDS%time_slice(1)%global_quantities%ip
endif

! Put time_slice/global_quantities/li_3
if (IDS%time_slice(1)%global_quantities%li_3.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/li_3",1,IDS%time_slice(1)%global_quantities%li_3)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%li_3',IDS%time_slice(1)%global_quantities%li_3
endif

! Put time_slice/global_quantities/volume
if (IDS%time_slice(1)%global_quantities%volume.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/volume",1,IDS%time_slice(1)%global_quantities%volume)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%volume',IDS%time_slice(1)%global_quantities%volume
endif

! Put time_slice/global_quantities/area
if (IDS%time_slice(1)%global_quantities%area.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/area",1,IDS%time_slice(1)%global_quantities%area)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%area',IDS%time_slice(1)%global_quantities%area
endif

! Put time_slice/global_quantities/surface
if (IDS%time_slice(1)%global_quantities%surface.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/surface",1,IDS%time_slice(1)%global_quantities%surface)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%surface',IDS%time_slice(1)%global_quantities%surface
endif

! Put time_slice/global_quantities/length_pol
if (IDS%time_slice(1)%global_quantities%length_pol.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/length_pol",1,IDS%time_slice(1)%global_quantities%length_pol)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%length_pol',IDS%time_slice(1)%global_quantities%length_pol
endif

! Put time_slice/global_quantities/psi_axis
if (IDS%time_slice(1)%global_quantities%psi_axis.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/psi_axis",1,IDS%time_slice(1)%global_quantities%psi_axis)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%psi_axis',IDS%time_slice(1)%global_quantities%psi_axis
endif

! Put time_slice/global_quantities/psi_boundary
if (IDS%time_slice(1)%global_quantities%psi_boundary.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/psi_boundary",1,IDS%time_slice(1)%global_quantities%psi_boundary)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%psi_boundary',IDS%time_slice(1)%global_quantities%psi_boundary
endif

! Put time_slice/global_quantities/magnetic_axis/r
if (IDS%time_slice(1)%global_quantities%magnetic_axis%r.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/r",1,IDS%time_slice(1)%global_quantities%magnetic_axis%r)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%magnetic_axis%r',IDS%time_slice(1)%global_quantities%magnetic_axis%r
endif

! Put time_slice/global_quantities/magnetic_axis/z
if (IDS%time_slice(1)%global_quantities%magnetic_axis%z.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/z",1,IDS%time_slice(1)%global_quantities%magnetic_axis%z)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%magnetic_axis%z',IDS%time_slice(1)%global_quantities%magnetic_axis%z
endif

! Put time_slice/global_quantities/magnetic_axis/b_tor
if (IDS%time_slice(1)%global_quantities%magnetic_axis%b_tor.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/magnetic_axis/b_tor",1,IDS%time_slice(1)%global_quantities%magnetic_axis%b_tor)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%magnetic_axis%b_tor',IDS%time_slice(1)%global_quantities%magnetic_axis%b_tor
endif

! Put time_slice/global_quantities/q_axis
if (IDS%time_slice(1)%global_quantities%q_axis.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/q_axis",1,IDS%time_slice(1)%global_quantities%q_axis)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%q_axis',IDS%time_slice(1)%global_quantities%q_axis
endif

! Put time_slice/global_quantities/q_95
if (IDS%time_slice(1)%global_quantities%q_95.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/q_95",1,IDS%time_slice(1)%global_quantities%q_95)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%q_95',IDS%time_slice(1)%global_quantities%q_95
endif

! Put time_slice/global_quantities/q_min/value
if (IDS%time_slice(1)%global_quantities%q_min%value.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/q_min/value",1,IDS%time_slice(1)%global_quantities%q_min%value)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%q_min%value',IDS%time_slice(1)%global_quantities%q_min%value
endif

! Put time_slice/global_quantities/q_min/rho_tor_norm
if (IDS%time_slice(1)%global_quantities%q_min%rho_tor_norm.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/q_min/rho_tor_norm",1,IDS%time_slice(1)%global_quantities%q_min%rho_tor_norm)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%q_min%rho_tor_norm',IDS%time_slice(1)%global_quantities%q_min%rho_tor_norm
endif

! Put time_slice/global_quantities/w_mhd
if (IDS%time_slice(1)%global_quantities%w_mhd.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/global_quantities/w_mhd",1,IDS%time_slice(1)%global_quantities%w_mhd)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%global_quantities%w_mhd',IDS%time_slice(1)%global_quantities%w_mhd
endif

! Put time_slice/profiles_1d/psi

if (associated(IDS%time_slice(1)%profiles_1d%psi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/psi",1,&
   IDS%time_slice(1)%profiles_1d%psi,&
   size(IDS%time_slice(1)%profiles_1d%psi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%psi',IDS%time_slice(1)%profiles_1d%psi
endif

! Put time_slice/profiles_1d/phi

if (associated(IDS%time_slice(1)%profiles_1d%phi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/phi",1,&
   IDS%time_slice(1)%profiles_1d%phi,&
   size(IDS%time_slice(1)%profiles_1d%phi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%phi',IDS%time_slice(1)%profiles_1d%phi
endif

! Put time_slice/profiles_1d/pressure

if (associated(IDS%time_slice(1)%profiles_1d%pressure)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/pressure",1,&
   IDS%time_slice(1)%profiles_1d%pressure,&
   size(IDS%time_slice(1)%profiles_1d%pressure))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%pressure',IDS%time_slice(1)%profiles_1d%pressure
endif

! Put time_slice/profiles_1d/f

if (associated(IDS%time_slice(1)%profiles_1d%f)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/f",1,&
   IDS%time_slice(1)%profiles_1d%f,&
   size(IDS%time_slice(1)%profiles_1d%f))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%f',IDS%time_slice(1)%profiles_1d%f
endif

! Put time_slice/profiles_1d/dpressure_dpsi

if (associated(IDS%time_slice(1)%profiles_1d%dpressure_dpsi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/dpressure_dpsi",1,&
   IDS%time_slice(1)%profiles_1d%dpressure_dpsi,&
   size(IDS%time_slice(1)%profiles_1d%dpressure_dpsi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%dpressure_dpsi',IDS%time_slice(1)%profiles_1d%dpressure_dpsi
endif

! Put time_slice/profiles_1d/f_df_dpsi

if (associated(IDS%time_slice(1)%profiles_1d%f_df_dpsi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/f_df_dpsi",1,&
   IDS%time_slice(1)%profiles_1d%f_df_dpsi,&
   size(IDS%time_slice(1)%profiles_1d%f_df_dpsi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%f_df_dpsi',IDS%time_slice(1)%profiles_1d%f_df_dpsi
endif

! Put time_slice/profiles_1d/j_tor

if (associated(IDS%time_slice(1)%profiles_1d%j_tor)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/j_tor",1,&
   IDS%time_slice(1)%profiles_1d%j_tor,&
   size(IDS%time_slice(1)%profiles_1d%j_tor))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%j_tor',IDS%time_slice(1)%profiles_1d%j_tor
endif

! Put time_slice/profiles_1d/j_parallel

if (associated(IDS%time_slice(1)%profiles_1d%j_parallel)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/j_parallel",1,&
   IDS%time_slice(1)%profiles_1d%j_parallel,&
   size(IDS%time_slice(1)%profiles_1d%j_parallel))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%j_parallel',IDS%time_slice(1)%profiles_1d%j_parallel
endif

! Put time_slice/profiles_1d/q

if (associated(IDS%time_slice(1)%profiles_1d%q)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/q",1,&
   IDS%time_slice(1)%profiles_1d%q,&
   size(IDS%time_slice(1)%profiles_1d%q))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%q',IDS%time_slice(1)%profiles_1d%q
endif

! Put time_slice/profiles_1d/magnetic_shear

if (associated(IDS%time_slice(1)%profiles_1d%magnetic_shear)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/magnetic_shear",1,&
   IDS%time_slice(1)%profiles_1d%magnetic_shear,&
   size(IDS%time_slice(1)%profiles_1d%magnetic_shear))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%magnetic_shear',IDS%time_slice(1)%profiles_1d%magnetic_shear
endif

! Put time_slice/profiles_1d/r_inboard

if (associated(IDS%time_slice(1)%profiles_1d%r_inboard)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/r_inboard",1,&
   IDS%time_slice(1)%profiles_1d%r_inboard,&
   size(IDS%time_slice(1)%profiles_1d%r_inboard))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%r_inboard',IDS%time_slice(1)%profiles_1d%r_inboard
endif

! Put time_slice/profiles_1d/r_outboard

if (associated(IDS%time_slice(1)%profiles_1d%r_outboard)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/r_outboard",1,&
   IDS%time_slice(1)%profiles_1d%r_outboard,&
   size(IDS%time_slice(1)%profiles_1d%r_outboard))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%r_outboard',IDS%time_slice(1)%profiles_1d%r_outboard
endif

! Put time_slice/profiles_1d/rho_tor

if (associated(IDS%time_slice(1)%profiles_1d%rho_tor)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/rho_tor",1,&
   IDS%time_slice(1)%profiles_1d%rho_tor,&
   size(IDS%time_slice(1)%profiles_1d%rho_tor))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%rho_tor',IDS%time_slice(1)%profiles_1d%rho_tor
endif

! Put time_slice/profiles_1d/rho_tor_norm

if (associated(IDS%time_slice(1)%profiles_1d%rho_tor_norm)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/rho_tor_norm",1,&
   IDS%time_slice(1)%profiles_1d%rho_tor_norm,&
   size(IDS%time_slice(1)%profiles_1d%rho_tor_norm))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%rho_tor_norm',IDS%time_slice(1)%profiles_1d%rho_tor_norm
endif

! Put time_slice/profiles_1d/dpsi_drho_tor

if (associated(IDS%time_slice(1)%profiles_1d%dpsi_drho_tor)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/dpsi_drho_tor",1,&
   IDS%time_slice(1)%profiles_1d%dpsi_drho_tor,&
   size(IDS%time_slice(1)%profiles_1d%dpsi_drho_tor))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%dpsi_drho_tor',IDS%time_slice(1)%profiles_1d%dpsi_drho_tor
endif

! Put time_slice/profiles_1d/elongation

if (associated(IDS%time_slice(1)%profiles_1d%elongation)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/elongation",1,&
   IDS%time_slice(1)%profiles_1d%elongation,&
   size(IDS%time_slice(1)%profiles_1d%elongation))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%elongation',IDS%time_slice(1)%profiles_1d%elongation
endif

! Put time_slice/profiles_1d/triangularity_upper

if (associated(IDS%time_slice(1)%profiles_1d%triangularity_upper)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/triangularity_upper",1,&
   IDS%time_slice(1)%profiles_1d%triangularity_upper,&
   size(IDS%time_slice(1)%profiles_1d%triangularity_upper))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%triangularity_upper',IDS%time_slice(1)%profiles_1d%triangularity_upper
endif

! Put time_slice/profiles_1d/triangularity_lower

if (associated(IDS%time_slice(1)%profiles_1d%triangularity_lower)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/triangularity_lower",1,&
   IDS%time_slice(1)%profiles_1d%triangularity_lower,&
   size(IDS%time_slice(1)%profiles_1d%triangularity_lower))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%triangularity_lower',IDS%time_slice(1)%profiles_1d%triangularity_lower
endif

! Put time_slice/profiles_1d/volume

if (associated(IDS%time_slice(1)%profiles_1d%volume)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/volume",1,&
   IDS%time_slice(1)%profiles_1d%volume,&
   size(IDS%time_slice(1)%profiles_1d%volume))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%volume',IDS%time_slice(1)%profiles_1d%volume
endif

! Put time_slice/profiles_1d/dvolume_dpsi

if (associated(IDS%time_slice(1)%profiles_1d%dvolume_dpsi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/dvolume_dpsi",1,&
   IDS%time_slice(1)%profiles_1d%dvolume_dpsi,&
   size(IDS%time_slice(1)%profiles_1d%dvolume_dpsi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%dvolume_dpsi',IDS%time_slice(1)%profiles_1d%dvolume_dpsi
endif

! Put time_slice/profiles_1d/dvolume_drho_tor

if (associated(IDS%time_slice(1)%profiles_1d%dvolume_drho_tor)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/dvolume_drho_tor",1,&
   IDS%time_slice(1)%profiles_1d%dvolume_drho_tor,&
   size(IDS%time_slice(1)%profiles_1d%dvolume_drho_tor))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%dvolume_drho_tor',IDS%time_slice(1)%profiles_1d%dvolume_drho_tor
endif

! Put time_slice/profiles_1d/area

if (associated(IDS%time_slice(1)%profiles_1d%area)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/area",1,&
   IDS%time_slice(1)%profiles_1d%area,&
   size(IDS%time_slice(1)%profiles_1d%area))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%area',IDS%time_slice(1)%profiles_1d%area
endif

! Put time_slice/profiles_1d/darea_dpsi

if (associated(IDS%time_slice(1)%profiles_1d%darea_dpsi)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/darea_dpsi",1,&
   IDS%time_slice(1)%profiles_1d%darea_dpsi,&
   size(IDS%time_slice(1)%profiles_1d%darea_dpsi))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%darea_dpsi',IDS%time_slice(1)%profiles_1d%darea_dpsi
endif

! Put time_slice/profiles_1d/surface

if (associated(IDS%time_slice(1)%profiles_1d%surface)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/surface",1,&
   IDS%time_slice(1)%profiles_1d%surface,&
   size(IDS%time_slice(1)%profiles_1d%surface))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%surface',IDS%time_slice(1)%profiles_1d%surface
endif

! Put time_slice/profiles_1d/trapped_fraction

if (associated(IDS%time_slice(1)%profiles_1d%trapped_fraction)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/trapped_fraction",1,&
   IDS%time_slice(1)%profiles_1d%trapped_fraction,&
   size(IDS%time_slice(1)%profiles_1d%trapped_fraction))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%trapped_fraction',IDS%time_slice(1)%profiles_1d%trapped_fraction
endif

! Put time_slice/profiles_1d/gm1

if (associated(IDS%time_slice(1)%profiles_1d%gm1)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm1",1,&
   IDS%time_slice(1)%profiles_1d%gm1,&
   size(IDS%time_slice(1)%profiles_1d%gm1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%gm1',IDS%time_slice(1)%profiles_1d%gm1
endif

! Put time_slice/profiles_1d/gm2

if (associated(IDS%time_slice(1)%profiles_1d%gm2)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm2",1,&
   IDS%time_slice(1)%profiles_1d%gm2,&
   size(IDS%time_slice(1)%profiles_1d%gm2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%gm2',IDS%time_slice(1)%profiles_1d%gm2
endif

! Put time_slice/profiles_1d/gm3

if (associated(IDS%time_slice(1)%profiles_1d%gm3)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm3",1,&
   IDS%time_slice(1)%profiles_1d%gm3,&
   size(IDS%time_slice(1)%profiles_1d%gm3))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%gm3',IDS%time_slice(1)%profiles_1d%gm3
endif

! Put time_slice/profiles_1d/gm4

if (associated(IDS%time_slice(1)%profiles_1d%gm4)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm4",1,&
   IDS%time_slice(1)%profiles_1d%gm4,&
   size(IDS%time_slice(1)%profiles_1d%gm4))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%gm4',IDS%time_slice(1)%profiles_1d%gm4
endif

! Put time_slice/profiles_1d/gm5

if (associated(IDS%time_slice(1)%profiles_1d%gm5)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm5",1,&
   IDS%time_slice(1)%profiles_1d%gm5,&
   size(IDS%time_slice(1)%profiles_1d%gm5))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%gm5',IDS%time_slice(1)%profiles_1d%gm5
endif

! Put time_slice/profiles_1d/gm6

if (associated(IDS%time_slice(1)%profiles_1d%gm6)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm6",1,&
   IDS%time_slice(1)%profiles_1d%gm6,&
   size(IDS%time_slice(1)%profiles_1d%gm6))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%gm6',IDS%time_slice(1)%profiles_1d%gm6
endif

! Put time_slice/profiles_1d/gm7

if (associated(IDS%time_slice(1)%profiles_1d%gm7)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm7",1,&
   IDS%time_slice(1)%profiles_1d%gm7,&
   size(IDS%time_slice(1)%profiles_1d%gm7))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%gm7',IDS%time_slice(1)%profiles_1d%gm7
endif

! Put time_slice/profiles_1d/gm8

if (associated(IDS%time_slice(1)%profiles_1d%gm8)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm8",1,&
   IDS%time_slice(1)%profiles_1d%gm8,&
   size(IDS%time_slice(1)%profiles_1d%gm8))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%gm8',IDS%time_slice(1)%profiles_1d%gm8
endif

! Put time_slice/profiles_1d/gm9

if (associated(IDS%time_slice(1)%profiles_1d%gm9)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/gm9",1,&
   IDS%time_slice(1)%profiles_1d%gm9,&
   size(IDS%time_slice(1)%profiles_1d%gm9))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%gm9',IDS%time_slice(1)%profiles_1d%gm9
endif

! Put time_slice/profiles_1d/b_average

if (associated(IDS%time_slice(1)%profiles_1d%b_average)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/b_average",1,&
   IDS%time_slice(1)%profiles_1d%b_average,&
   size(IDS%time_slice(1)%profiles_1d%b_average))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%b_average',IDS%time_slice(1)%profiles_1d%b_average
endif

! Put time_slice/profiles_1d/b_min

if (associated(IDS%time_slice(1)%profiles_1d%b_min)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/b_min",1,&
   IDS%time_slice(1)%profiles_1d%b_min,&
   size(IDS%time_slice(1)%profiles_1d%b_min))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%b_min',IDS%time_slice(1)%profiles_1d%b_min
endif

! Put time_slice/profiles_1d/b_max

if (associated(IDS%time_slice(1)%profiles_1d%b_max)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/profiles_1d/b_max",1,&
   IDS%time_slice(1)%profiles_1d%b_max,&
   size(IDS%time_slice(1)%profiles_1d%b_max))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_1d%b_max',IDS%time_slice(1)%profiles_1d%b_max
endif

! Put time_slice/profiles_2d
if (associated(IDS%time_slice(1)%profiles_2d)) then
call begin_object(idx,obj1,1,"time_slice/profiles_2d",NON_TIMED,obj2)
! Start to declare a nested Type 2 Aos 
   do i2 = 1,size(IDS%time_slice(1)%profiles_2d)
         
! Put time_slice/profiles_2d/grid_type/name
if (associated(IDS%time_slice(1)%profiles_2d(i2)%grid_type%name)) then
   longstring = ' '    
   lenstring = size(IDS%time_slice(1)%profiles_2d(i2)%grid_type%name)
   if (lenstring.EQ.1) then             
      longstring = trim(IDS%time_slice(1)%profiles_2d(i2)%grid_type%name(1))
   else
      do istring=1,lenstring
          longstring(1+(istring-1)*132 : istring*132) = IDS%time_slice(1)%profiles_2d(i2)%grid_type%name(istring)
      enddo
   endif
   call put_string_in_object(idx,obj2,"profiles_2d/grid_type/name",i2,trim(longstring))       ! should clean up longstring after that, or send to the put only the right length, which has been updated
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%grid_type%name',IDS%time_slice(1)%profiles_2d(i2)%grid_type%name
endif

! Put time_slice/profiles_2d/grid_type/index        
if (IDS%time_slice(1)%profiles_2d(i2)%grid_type%index.NE.-999999999) then
   call put_int_in_object(idx,obj2,"profiles_2d/grid_type/index",i2,IDS%time_slice(1)%profiles_2d(i2)%grid_type%index)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%grid_type%index',IDS%time_slice(1)%profiles_2d(i2)%grid_type%index
endif

! Put time_slice/profiles_2d/grid_type/description
if (associated(IDS%time_slice(1)%profiles_2d(i2)%grid_type%description)) then
   longstring = ' '    
   lenstring = size(IDS%time_slice(1)%profiles_2d(i2)%grid_type%description)
   if (lenstring.EQ.1) then             
      longstring = trim(IDS%time_slice(1)%profiles_2d(i2)%grid_type%description(1))
   else
      do istring=1,lenstring
          longstring(1+(istring-1)*132 : istring*132) = IDS%time_slice(1)%profiles_2d(i2)%grid_type%description(istring)
      enddo
   endif
   call put_string_in_object(idx,obj2,"profiles_2d/grid_type/description",i2,trim(longstring))       ! should clean up longstring after that, or send to the put only the right length, which has been updated
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%grid_type%description',IDS%time_slice(1)%profiles_2d(i2)%grid_type%description
endif

! Put time_slice/profiles_2d/grid/dim1

if (associated(IDS%time_slice(1)%profiles_2d(i2)%grid%dim1)) then
   call put_vect1d_double_in_object(idx,obj2,"profiles_2d/grid/dim1",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%grid%dim1,&
   size(IDS%time_slice(1)%profiles_2d(i2)%grid%dim1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%grid%dim1',IDS%time_slice(1)%profiles_2d(i2)%grid%dim1
endif

! Put time_slice/profiles_2d/grid/dim2

if (associated(IDS%time_slice(1)%profiles_2d(i2)%grid%dim2)) then
   call put_vect1d_double_in_object(idx,obj2,"profiles_2d/grid/dim2",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%grid%dim2,&
   size(IDS%time_slice(1)%profiles_2d(i2)%grid%dim2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%grid%dim2',IDS%time_slice(1)%profiles_2d(i2)%grid%dim2
endif

! Put time_slice/profiles_2d/r        
if (associated(IDS%time_slice(1)%profiles_2d(i2)%r)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/r",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%r, &
   size(IDS%time_slice(1)%profiles_2d(i2)%r,1),&
   size(IDS%time_slice(1)%profiles_2d(i2)%r,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%r',IDS%time_slice(1)%profiles_2d(i2)%r
endif

! Put time_slice/profiles_2d/z        
if (associated(IDS%time_slice(1)%profiles_2d(i2)%z)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/z",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%z, &
   size(IDS%time_slice(1)%profiles_2d(i2)%z,1),&
   size(IDS%time_slice(1)%profiles_2d(i2)%z,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%z',IDS%time_slice(1)%profiles_2d(i2)%z
endif

! Put time_slice/profiles_2d/psi        
if (associated(IDS%time_slice(1)%profiles_2d(i2)%psi)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/psi",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%psi, &
   size(IDS%time_slice(1)%profiles_2d(i2)%psi,1),&
   size(IDS%time_slice(1)%profiles_2d(i2)%psi,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%psi',IDS%time_slice(1)%profiles_2d(i2)%psi
endif

! Put time_slice/profiles_2d/theta        
if (associated(IDS%time_slice(1)%profiles_2d(i2)%theta)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/theta",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%theta, &
   size(IDS%time_slice(1)%profiles_2d(i2)%theta,1),&
   size(IDS%time_slice(1)%profiles_2d(i2)%theta,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%theta',IDS%time_slice(1)%profiles_2d(i2)%theta
endif

! Put time_slice/profiles_2d/phi        
if (associated(IDS%time_slice(1)%profiles_2d(i2)%phi)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/phi",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%phi, &
   size(IDS%time_slice(1)%profiles_2d(i2)%phi,1),&
   size(IDS%time_slice(1)%profiles_2d(i2)%phi,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%phi',IDS%time_slice(1)%profiles_2d(i2)%phi
endif

! Put time_slice/profiles_2d/j_tor        
if (associated(IDS%time_slice(1)%profiles_2d(i2)%j_tor)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/j_tor",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%j_tor, &
   size(IDS%time_slice(1)%profiles_2d(i2)%j_tor,1),&
   size(IDS%time_slice(1)%profiles_2d(i2)%j_tor,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%j_tor',IDS%time_slice(1)%profiles_2d(i2)%j_tor
endif

! Put time_slice/profiles_2d/j_parallel        
if (associated(IDS%time_slice(1)%profiles_2d(i2)%j_parallel)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/j_parallel",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%j_parallel, &
   size(IDS%time_slice(1)%profiles_2d(i2)%j_parallel,1),&
   size(IDS%time_slice(1)%profiles_2d(i2)%j_parallel,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%j_parallel',IDS%time_slice(1)%profiles_2d(i2)%j_parallel
endif

! Put time_slice/profiles_2d/b_r        
if (associated(IDS%time_slice(1)%profiles_2d(i2)%b_r)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/b_r",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%b_r, &
   size(IDS%time_slice(1)%profiles_2d(i2)%b_r,1),&
   size(IDS%time_slice(1)%profiles_2d(i2)%b_r,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%b_r',IDS%time_slice(1)%profiles_2d(i2)%b_r
endif

! Put time_slice/profiles_2d/b_z        
if (associated(IDS%time_slice(1)%profiles_2d(i2)%b_z)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/b_z",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%b_z, &
   size(IDS%time_slice(1)%profiles_2d(i2)%b_z,1),&
   size(IDS%time_slice(1)%profiles_2d(i2)%b_z,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%b_z',IDS%time_slice(1)%profiles_2d(i2)%b_z
endif

! Put time_slice/profiles_2d/b_tor        
if (associated(IDS%time_slice(1)%profiles_2d(i2)%b_tor)) then
   call put_vect2d_double_in_object(idx,obj2,"profiles_2d/b_tor",i2,&
   IDS%time_slice(1)%profiles_2d(i2)%b_tor, &
   size(IDS%time_slice(1)%profiles_2d(i2)%b_tor,1),&
   size(IDS%time_slice(1)%profiles_2d(i2)%b_tor,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%profiles_2d(i2)%b_tor',IDS%time_slice(1)%profiles_2d(i2)%b_tor
endif

   enddo
call put_object_in_object(idx,obj1, "time_slice/profiles_2d",1, obj2)
endif
    
! Put time_slice/coordinate_system/grid_type/name
if (associated(IDS%time_slice(1)%coordinate_system%grid_type%name)) then
   longstring = ' '    
   lenstring = size(IDS%time_slice(1)%coordinate_system%grid_type%name)
   if (lenstring.EQ.1) then             
      longstring = trim(IDS%time_slice(1)%coordinate_system%grid_type%name(1))
   else
      do istring=1,lenstring
          longstring(1+(istring-1)*132 : istring*132) = IDS%time_slice(1)%coordinate_system%grid_type%name(istring)
      enddo
   endif
   call put_string_in_object(idx,obj1,"time_slice/coordinate_system/grid_type/name",1,trim(longstring))       ! should clean up longstring after that, or send to the put only the right length, which has been updated
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%grid_type%name',IDS%time_slice(1)%coordinate_system%grid_type%name
endif

! Put time_slice/coordinate_system/grid_type/index        
if (IDS%time_slice(1)%coordinate_system%grid_type%index.NE.-999999999) then
   call put_int_in_object(idx,obj1,"time_slice/coordinate_system/grid_type/index",1,IDS%time_slice(1)%coordinate_system%grid_type%index)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%grid_type%index',IDS%time_slice(1)%coordinate_system%grid_type%index
endif

! Put time_slice/coordinate_system/grid_type/description
if (associated(IDS%time_slice(1)%coordinate_system%grid_type%description)) then
   longstring = ' '    
   lenstring = size(IDS%time_slice(1)%coordinate_system%grid_type%description)
   if (lenstring.EQ.1) then             
      longstring = trim(IDS%time_slice(1)%coordinate_system%grid_type%description(1))
   else
      do istring=1,lenstring
          longstring(1+(istring-1)*132 : istring*132) = IDS%time_slice(1)%coordinate_system%grid_type%description(istring)
      enddo
   endif
   call put_string_in_object(idx,obj1,"time_slice/coordinate_system/grid_type/description",1,trim(longstring))       ! should clean up longstring after that, or send to the put only the right length, which has been updated
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%grid_type%description',IDS%time_slice(1)%coordinate_system%grid_type%description
endif

! Put time_slice/coordinate_system/grid/dim1

if (associated(IDS%time_slice(1)%coordinate_system%grid%dim1)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/coordinate_system/grid/dim1",1,&
   IDS%time_slice(1)%coordinate_system%grid%dim1,&
   size(IDS%time_slice(1)%coordinate_system%grid%dim1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%grid%dim1',IDS%time_slice(1)%coordinate_system%grid%dim1
endif

! Put time_slice/coordinate_system/grid/dim2

if (associated(IDS%time_slice(1)%coordinate_system%grid%dim2)) then
   call put_vect1d_double_in_object(idx,obj1,"time_slice/coordinate_system/grid/dim2",1,&
   IDS%time_slice(1)%coordinate_system%grid%dim2,&
   size(IDS%time_slice(1)%coordinate_system%grid%dim2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%grid%dim2',IDS%time_slice(1)%coordinate_system%grid%dim2
endif

! Put time_slice/coordinate_system/r        
if (associated(IDS%time_slice(1)%coordinate_system%r)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/r",1,&
   IDS%time_slice(1)%coordinate_system%r, &
   size(IDS%time_slice(1)%coordinate_system%r,1),&
   size(IDS%time_slice(1)%coordinate_system%r,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%r',IDS%time_slice(1)%coordinate_system%r
endif

! Put time_slice/coordinate_system/z        
if (associated(IDS%time_slice(1)%coordinate_system%z)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/z",1,&
   IDS%time_slice(1)%coordinate_system%z, &
   size(IDS%time_slice(1)%coordinate_system%z,1),&
   size(IDS%time_slice(1)%coordinate_system%z,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%z',IDS%time_slice(1)%coordinate_system%z
endif

! Put time_slice/coordinate_system/jacobian        
if (associated(IDS%time_slice(1)%coordinate_system%jacobian)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/jacobian",1,&
   IDS%time_slice(1)%coordinate_system%jacobian, &
   size(IDS%time_slice(1)%coordinate_system%jacobian,1),&
   size(IDS%time_slice(1)%coordinate_system%jacobian,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%jacobian',IDS%time_slice(1)%coordinate_system%jacobian
endif

! Put time_slice/coordinate_system/g_11        
if (associated(IDS%time_slice(1)%coordinate_system%g_11)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_11",1,&
   IDS%time_slice(1)%coordinate_system%g_11, &
   size(IDS%time_slice(1)%coordinate_system%g_11,1),&
   size(IDS%time_slice(1)%coordinate_system%g_11,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%g_11',IDS%time_slice(1)%coordinate_system%g_11
endif

! Put time_slice/coordinate_system/g_12        
if (associated(IDS%time_slice(1)%coordinate_system%g_12)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_12",1,&
   IDS%time_slice(1)%coordinate_system%g_12, &
   size(IDS%time_slice(1)%coordinate_system%g_12,1),&
   size(IDS%time_slice(1)%coordinate_system%g_12,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%g_12',IDS%time_slice(1)%coordinate_system%g_12
endif

! Put time_slice/coordinate_system/g_13        
if (associated(IDS%time_slice(1)%coordinate_system%g_13)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_13",1,&
   IDS%time_slice(1)%coordinate_system%g_13, &
   size(IDS%time_slice(1)%coordinate_system%g_13,1),&
   size(IDS%time_slice(1)%coordinate_system%g_13,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%g_13',IDS%time_slice(1)%coordinate_system%g_13
endif

! Put time_slice/coordinate_system/g_22        
if (associated(IDS%time_slice(1)%coordinate_system%g_22)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_22",1,&
   IDS%time_slice(1)%coordinate_system%g_22, &
   size(IDS%time_slice(1)%coordinate_system%g_22,1),&
   size(IDS%time_slice(1)%coordinate_system%g_22,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%g_22',IDS%time_slice(1)%coordinate_system%g_22
endif

! Put time_slice/coordinate_system/g_23        
if (associated(IDS%time_slice(1)%coordinate_system%g_23)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_23",1,&
   IDS%time_slice(1)%coordinate_system%g_23, &
   size(IDS%time_slice(1)%coordinate_system%g_23,1),&
   size(IDS%time_slice(1)%coordinate_system%g_23,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%g_23',IDS%time_slice(1)%coordinate_system%g_23
endif

! Put time_slice/coordinate_system/g_33        
if (associated(IDS%time_slice(1)%coordinate_system%g_33)) then
   call put_vect2d_double_in_object(idx,obj1,"time_slice/coordinate_system/g_33",1,&
   IDS%time_slice(1)%coordinate_system%g_33, &
   size(IDS%time_slice(1)%coordinate_system%g_33,1),&
   size(IDS%time_slice(1)%coordinate_system%g_33,2))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%coordinate_system%g_33',IDS%time_slice(1)%coordinate_system%g_33
endif

! Put time_slice/time
if (IDS%time_slice(1)%time.NE.-9.D40) then
   call put_double_in_object(idx,obj1,"time_slice/time",1,IDS%time_slice(1)%time)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice(1)%time',IDS%time_slice(1)%time
endif


   call put_object_in_object(idx,obj_single_time,"ALLTIMES",1,obj1);
   call put_object_slice(idx,path,"time_slice",IDS%time,obj_single_time);

   ! Store time of the array of structure (hidden variable for the user, but used by the UAL for future get_slice operations)
   allocate(time(1))
   if (IDS%time_slice(1)%time.EQ.-9.D40) then ! Check the presence of a time vector at the root of the AoS (on the first index only)
      if (IDS%IDS_Properties%homogeneous_time.EQ.1) then
         time = ids%time ! Use the general time vector of the IDS to fill time
      else
         write(*,*) "ERROR : the time vector of the type 3 array of structure time_slice must be filled"
         return
      endif
   else 
      do i1 = 1,size(IDS%time_slice) ! the AoS time vector is there, fill time with it
         time(i1) = IDS%time_slice(i1)%time
      enddo
   endif

   timepath="time_slice/time"
   call put_double_slice(idx,path,trim(timepath),&
      trim(timepath),&
      time,&
      time)
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time_slice%time',time

endif

! Put code/name
if (associated(IDS%code%name)) then
   call put_string_slice(idx,path, "code"//"/name", &
          trim(timepath),IDS%code%name(1),IDS%time(1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%code%name'
endif

! Put code/version
if (associated(IDS%code%version)) then
   call put_string_slice(idx,path, "code"//"/version", &
          trim(timepath),IDS%code%version(1),IDS%time(1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%code%version'
endif

! Put code/parameters
if (associated(IDS%code%parameters)) then
   call put_string_slice(idx,path, "code"//"/parameters", &
          trim(timepath),IDS%code%parameters(1),IDS%time(1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%code%parameters'
endif

! Put code/output_flag
if (associated(IDS%code%output_flag)) then         
   call put_int_slice(idx,path, "code"//"/output_flag",&
   trim(timepath), &
   IDS%code%output_flag(1),&
   IDS%time(1)) 
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%code%output_flag',IDS%code%output_flag
endif

! Put time
if (associated(IDS%time)) then
   call put_double_slice(idx,path, "time",&
   trim(timepath),&
   IDS%time(1),&
   IDS%time(1))
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%time',IDS%time
endif

call end_IDS_put_slice(idx, path)

 
return
end subroutine ids_put_slice_equilibrium


!!!!!! Routines to PUT_NON_TIMED the time INdependent data of time dependent IDSs 

subroutine ids_put_non_timed_equilibrium(idx, path,  IDS)

use ids_schemas
implicit none

character*(*) :: path
integer :: idx
integer :: i,dim1,dim2,dim3,dim4,dim5,dim6,dim7, lenstring, istring
integer, pointer :: dimtab(:) => null()
character(len=100000)::longstring    
character(len=3)::ual_debug
character(len=300) :: timepath    
integer :: obj1,obj2,obj3,obj4,obj5,obj6,obj7
integer :: i1,i2,i3,i4,i5,i6,i7

integer :: itime_slice
integer :: ix_point
integer :: istrike_point
integer :: iprofiles_2d

type(ids_equilibrium) :: IDS       ! real declaration of the IDS for the put

call getenv('ual_debug',ual_debug) ! Debug flag

! Systematic delete of the previous IDS, in case it existed; guarantees the time-dependent data is deleted
call ids_delete(idx,path,IDS)

! And systematic erase of the previous changes in cache
! The ids_discard routines are obsolete, do not call them anymore
! call ids_discard(idx,path,IDS)


call begin_IDS_put_non_timed(idx, path) 
		
! Put ids_properties/comment
if (associated(IDS%ids_properties%comment)) then
   longstring = ' '    
   lenstring = size(IDS%ids_properties%comment)      
   if (lenstring.EQ.1) then             
      longstring = trim(IDS%ids_properties%comment(1))
   else
      do istring=1,lenstring
          longstring(1+(istring-1)*132 : istring*132) = IDS%ids_properties%comment(istring)
      enddo
   endif
   call put_string(idx,path, "ids_properties"//"/comment",trim(longstring))       ! should clean up longstring after that, or send to the put only the right length, which has been updated
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%ids_properties%comment',IDS%ids_properties%comment
endif

! Put ids_properties/homogeneous_time
if (IDS%ids_properties%homogeneous_time.NE.-999999999) then
   call put_int(idx,path, "ids_properties"//"/homogeneous_time",&
       IDS%ids_properties%homogeneous_time)        
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%ids_properties%homogeneous_time',IDS%ids_properties%homogeneous_time
endif

! Put ids_properties/cocos
if (IDS%ids_properties%cocos.NE.-999999999) then
   call put_int(idx,path, "ids_properties"//"/cocos",&
       IDS%ids_properties%cocos)        
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%ids_properties%cocos',IDS%ids_properties%cocos
endif

! Put vacuum_toroidal_field/r0
if (IDS%vacuum_toroidal_field%r0.NE.-9.D40) then 
   call put_double(idx,path, "vacuum_toroidal_field"//"/r0",IDS%vacuum_toroidal_field%r0)  
   if (ual_debug =='yes') write(*,*) & 
      'Put IDS%vacuum_toroidal_field%r0',IDS%vacuum_toroidal_field%r0
endif

call end_IDS_put_non_timed(idx, path)

return
end subroutine ids_put_non_timed_equilibrium

!!!!!! Routine to DELETE the IDS 

subroutine ids_delete_equilibrium(idx,IDSpath,IDS)  

use ids_schemas
implicit none
character*(*) :: IDSpath
integer :: idx
character(len=3)::ual_debug

type(ids_equilibrium) :: IDS       

integer :: itime_slice
integer :: ix_point
integer :: istrike_point
integer :: iprofiles_2d		

call getenv('ual_debug',ual_debug) ! Debug flag
if (ual_debug =='yes') write(*,*) 'Deleting IDS ',IDSpath

call delete_data(idx,IDSpath,"ids_properties"//"/comment")         
call delete_data(idx,IDSpath,"ids_properties"//"/homogeneous_time")         
call delete_data(idx,IDSpath,"ids_properties"//"/cocos")         
call delete_data(idx,IDSpath,"vacuum_toroidal_field"//"/r0")         
call delete_data(idx,IDSpath,"vacuum_toroidal_field"//"/b0")         
call delete_data(idx,IDSpath,"time_slice")         
call delete_data(idx,IDSpath,"code"//"/name")         
call delete_data(idx,IDSpath,"code"//"/version")         
call delete_data(idx,IDSpath,"code"//"/parameters")         
call delete_data(idx,IDSpath,"code"//"/output_flag")         
call delete_data(idx,IDSpath,"time")         
if (ual_debug =='yes') write(*,*) 'Delete IDS ',IDSpath,' done'
end subroutine ids_delete_equilibrium


!!!!!! Routines to DEALLOCATE IDSs 

subroutine ids_deallocate_equilibrium(IDS)  

use ids_schemas
implicit none

character(len=3)::ual_debug
integer :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium) :: IDS       
    
   ! deallocate ids_properties/comment
   if (associated(IDS%ids_properties%comment)) then
        deallocate(IDS%ids_properties%comment)
   endif
   			
   ! deallocate vacuum_toroidal_field/b0
   if (associated(IDS%vacuum_toroidal_field%b0)) then
        deallocate(IDS%vacuum_toroidal_field%b0)
   endif
   			
    ! deallocate time_slice
    if (associated(IDS%time_slice)) then
        do i1 = 1,size(IDS%time_slice)
             
   ! deallocate time_slice/boundary/lcfs/r
   if (associated(IDS%time_slice(i1)%boundary%lcfs%r)) then
        deallocate(IDS%time_slice(i1)%boundary%lcfs%r)
   endif
   			
   ! deallocate time_slice/boundary/lcfs/z
   if (associated(IDS%time_slice(i1)%boundary%lcfs%z)) then
        deallocate(IDS%time_slice(i1)%boundary%lcfs%z)
   endif
   			
    ! deallocate time_slice/boundary/x_point
    if (associated(IDS%time_slice(i1)%boundary%x_point)) then
        do i2 = 1,size(IDS%time_slice(i1)%boundary%x_point)
             
        enddo
        deallocate(IDS%time_slice(i1)%boundary%x_point)
    endif
         
    ! deallocate time_slice/boundary/strike_point
    if (associated(IDS%time_slice(i1)%boundary%strike_point)) then
        do i2 = 1,size(IDS%time_slice(i1)%boundary%strike_point)
             
        enddo
        deallocate(IDS%time_slice(i1)%boundary%strike_point)
    endif
         
   ! deallocate time_slice/profiles_1d/psi
   if (associated(IDS%time_slice(i1)%profiles_1d%psi)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%psi)
   endif
   			
   ! deallocate time_slice/profiles_1d/phi
   if (associated(IDS%time_slice(i1)%profiles_1d%phi)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%phi)
   endif
   			
   ! deallocate time_slice/profiles_1d/pressure
   if (associated(IDS%time_slice(i1)%profiles_1d%pressure)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%pressure)
   endif
   			
   ! deallocate time_slice/profiles_1d/f
   if (associated(IDS%time_slice(i1)%profiles_1d%f)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%f)
   endif
   			
   ! deallocate time_slice/profiles_1d/dpressure_dpsi
   if (associated(IDS%time_slice(i1)%profiles_1d%dpressure_dpsi)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%dpressure_dpsi)
   endif
   			
   ! deallocate time_slice/profiles_1d/f_df_dpsi
   if (associated(IDS%time_slice(i1)%profiles_1d%f_df_dpsi)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%f_df_dpsi)
   endif
   			
   ! deallocate time_slice/profiles_1d/j_tor
   if (associated(IDS%time_slice(i1)%profiles_1d%j_tor)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%j_tor)
   endif
   			
   ! deallocate time_slice/profiles_1d/j_parallel
   if (associated(IDS%time_slice(i1)%profiles_1d%j_parallel)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%j_parallel)
   endif
   			
   ! deallocate time_slice/profiles_1d/q
   if (associated(IDS%time_slice(i1)%profiles_1d%q)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%q)
   endif
   			
   ! deallocate time_slice/profiles_1d/magnetic_shear
   if (associated(IDS%time_slice(i1)%profiles_1d%magnetic_shear)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%magnetic_shear)
   endif
   			
   ! deallocate time_slice/profiles_1d/r_inboard
   if (associated(IDS%time_slice(i1)%profiles_1d%r_inboard)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%r_inboard)
   endif
   			
   ! deallocate time_slice/profiles_1d/r_outboard
   if (associated(IDS%time_slice(i1)%profiles_1d%r_outboard)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%r_outboard)
   endif
   			
   ! deallocate time_slice/profiles_1d/rho_tor
   if (associated(IDS%time_slice(i1)%profiles_1d%rho_tor)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%rho_tor)
   endif
   			
   ! deallocate time_slice/profiles_1d/rho_tor_norm
   if (associated(IDS%time_slice(i1)%profiles_1d%rho_tor_norm)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%rho_tor_norm)
   endif
   			
   ! deallocate time_slice/profiles_1d/dpsi_drho_tor
   if (associated(IDS%time_slice(i1)%profiles_1d%dpsi_drho_tor)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%dpsi_drho_tor)
   endif
   			
   ! deallocate time_slice/profiles_1d/elongation
   if (associated(IDS%time_slice(i1)%profiles_1d%elongation)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%elongation)
   endif
   			
   ! deallocate time_slice/profiles_1d/triangularity_upper
   if (associated(IDS%time_slice(i1)%profiles_1d%triangularity_upper)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%triangularity_upper)
   endif
   			
   ! deallocate time_slice/profiles_1d/triangularity_lower
   if (associated(IDS%time_slice(i1)%profiles_1d%triangularity_lower)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%triangularity_lower)
   endif
   			
   ! deallocate time_slice/profiles_1d/volume
   if (associated(IDS%time_slice(i1)%profiles_1d%volume)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%volume)
   endif
   			
   ! deallocate time_slice/profiles_1d/dvolume_dpsi
   if (associated(IDS%time_slice(i1)%profiles_1d%dvolume_dpsi)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%dvolume_dpsi)
   endif
   			
   ! deallocate time_slice/profiles_1d/dvolume_drho_tor
   if (associated(IDS%time_slice(i1)%profiles_1d%dvolume_drho_tor)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%dvolume_drho_tor)
   endif
   			
   ! deallocate time_slice/profiles_1d/area
   if (associated(IDS%time_slice(i1)%profiles_1d%area)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%area)
   endif
   			
   ! deallocate time_slice/profiles_1d/darea_dpsi
   if (associated(IDS%time_slice(i1)%profiles_1d%darea_dpsi)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%darea_dpsi)
   endif
   			
   ! deallocate time_slice/profiles_1d/surface
   if (associated(IDS%time_slice(i1)%profiles_1d%surface)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%surface)
   endif
   			
   ! deallocate time_slice/profiles_1d/trapped_fraction
   if (associated(IDS%time_slice(i1)%profiles_1d%trapped_fraction)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%trapped_fraction)
   endif
   			
   ! deallocate time_slice/profiles_1d/gm1
   if (associated(IDS%time_slice(i1)%profiles_1d%gm1)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%gm1)
   endif
   			
   ! deallocate time_slice/profiles_1d/gm2
   if (associated(IDS%time_slice(i1)%profiles_1d%gm2)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%gm2)
   endif
   			
   ! deallocate time_slice/profiles_1d/gm3
   if (associated(IDS%time_slice(i1)%profiles_1d%gm3)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%gm3)
   endif
   			
   ! deallocate time_slice/profiles_1d/gm4
   if (associated(IDS%time_slice(i1)%profiles_1d%gm4)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%gm4)
   endif
   			
   ! deallocate time_slice/profiles_1d/gm5
   if (associated(IDS%time_slice(i1)%profiles_1d%gm5)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%gm5)
   endif
   			
   ! deallocate time_slice/profiles_1d/gm6
   if (associated(IDS%time_slice(i1)%profiles_1d%gm6)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%gm6)
   endif
   			
   ! deallocate time_slice/profiles_1d/gm7
   if (associated(IDS%time_slice(i1)%profiles_1d%gm7)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%gm7)
   endif
   			
   ! deallocate time_slice/profiles_1d/gm8
   if (associated(IDS%time_slice(i1)%profiles_1d%gm8)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%gm8)
   endif
   			
   ! deallocate time_slice/profiles_1d/gm9
   if (associated(IDS%time_slice(i1)%profiles_1d%gm9)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%gm9)
   endif
   			
   ! deallocate time_slice/profiles_1d/b_average
   if (associated(IDS%time_slice(i1)%profiles_1d%b_average)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%b_average)
   endif
   			
   ! deallocate time_slice/profiles_1d/b_min
   if (associated(IDS%time_slice(i1)%profiles_1d%b_min)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%b_min)
   endif
   			
   ! deallocate time_slice/profiles_1d/b_max
   if (associated(IDS%time_slice(i1)%profiles_1d%b_max)) then
        deallocate(IDS%time_slice(i1)%profiles_1d%b_max)
   endif
   			
    ! deallocate time_slice/profiles_2d
    if (associated(IDS%time_slice(i1)%profiles_2d)) then
        do i2 = 1,size(IDS%time_slice(i1)%profiles_2d)
             
   ! deallocate time_slice/profiles_2d/grid_type/name
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%grid_type%name)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%grid_type%name)
   endif
   			
   ! deallocate time_slice/profiles_2d/grid_type/description
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%grid_type%description)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%grid_type%description)
   endif
   			
   ! deallocate time_slice/profiles_2d/grid/dim1
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%grid%dim1)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%grid%dim1)
   endif
   			
   ! deallocate time_slice/profiles_2d/grid/dim2
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%grid%dim2)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%grid%dim2)
   endif
   			
   ! deallocate time_slice/profiles_2d/r
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%r)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%r)
   endif
   			
   ! deallocate time_slice/profiles_2d/z
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%z)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%z)
   endif
   			
   ! deallocate time_slice/profiles_2d/psi
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%psi)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%psi)
   endif
   			
   ! deallocate time_slice/profiles_2d/theta
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%theta)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%theta)
   endif
   			
   ! deallocate time_slice/profiles_2d/phi
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%phi)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%phi)
   endif
   			
   ! deallocate time_slice/profiles_2d/j_tor
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%j_tor)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%j_tor)
   endif
   			
   ! deallocate time_slice/profiles_2d/j_parallel
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%j_parallel)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%j_parallel)
   endif
   			
   ! deallocate time_slice/profiles_2d/b_r
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%b_r)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%b_r)
   endif
   			
   ! deallocate time_slice/profiles_2d/b_z
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%b_z)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%b_z)
   endif
   			
   ! deallocate time_slice/profiles_2d/b_tor
   if (associated(IDS%time_slice(i1)%profiles_2d(i2)%b_tor)) then
        deallocate(IDS%time_slice(i1)%profiles_2d(i2)%b_tor)
   endif
   			
        enddo
        deallocate(IDS%time_slice(i1)%profiles_2d)
    endif
         
   ! deallocate time_slice/coordinate_system/grid_type/name
   if (associated(IDS%time_slice(i1)%coordinate_system%grid_type%name)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%grid_type%name)
   endif
   			
   ! deallocate time_slice/coordinate_system/grid_type/description
   if (associated(IDS%time_slice(i1)%coordinate_system%grid_type%description)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%grid_type%description)
   endif
   			
   ! deallocate time_slice/coordinate_system/grid/dim1
   if (associated(IDS%time_slice(i1)%coordinate_system%grid%dim1)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%grid%dim1)
   endif
   			
   ! deallocate time_slice/coordinate_system/grid/dim2
   if (associated(IDS%time_slice(i1)%coordinate_system%grid%dim2)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%grid%dim2)
   endif
   			
   ! deallocate time_slice/coordinate_system/r
   if (associated(IDS%time_slice(i1)%coordinate_system%r)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%r)
   endif
   			
   ! deallocate time_slice/coordinate_system/z
   if (associated(IDS%time_slice(i1)%coordinate_system%z)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%z)
   endif
   			
   ! deallocate time_slice/coordinate_system/jacobian
   if (associated(IDS%time_slice(i1)%coordinate_system%jacobian)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%jacobian)
   endif
   			
   ! deallocate time_slice/coordinate_system/g_11
   if (associated(IDS%time_slice(i1)%coordinate_system%g_11)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%g_11)
   endif
   			
   ! deallocate time_slice/coordinate_system/g_12
   if (associated(IDS%time_slice(i1)%coordinate_system%g_12)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%g_12)
   endif
   			
   ! deallocate time_slice/coordinate_system/g_13
   if (associated(IDS%time_slice(i1)%coordinate_system%g_13)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%g_13)
   endif
   			
   ! deallocate time_slice/coordinate_system/g_22
   if (associated(IDS%time_slice(i1)%coordinate_system%g_22)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%g_22)
   endif
   			
   ! deallocate time_slice/coordinate_system/g_23
   if (associated(IDS%time_slice(i1)%coordinate_system%g_23)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%g_23)
   endif
   			
   ! deallocate time_slice/coordinate_system/g_33
   if (associated(IDS%time_slice(i1)%coordinate_system%g_33)) then
        deallocate(IDS%time_slice(i1)%coordinate_system%g_33)
   endif
   			
        enddo
        deallocate(IDS%time_slice)
    endif
         
   ! deallocate code/name
   if (associated(IDS%code%name)) then
        deallocate(IDS%code%name)
   endif
   			
   ! deallocate code/version
   if (associated(IDS%code%version)) then
        deallocate(IDS%code%version)
   endif
   			
   ! deallocate code/parameters
   if (associated(IDS%code%parameters)) then
        deallocate(IDS%code%parameters)
   endif
   			
   ! deallocate code/output_flag
   if (associated(IDS%code%output_flag)) then
        deallocate(IDS%code%output_flag)
   endif
   			
   ! deallocate time
   if (associated(IDS%time)) then
        deallocate(IDS%time)
   endif
   			

if (ual_debug =='yes') write(*,*) 'Deallocate an equilibrium IDS : done'
end subroutine ids_deallocate_equilibrium


!!!!!! Routines to COPY IDSs 
subroutine ids_copy_equilibrium(IDSin,  IDSout)
! Copies all fields of IDSin to IDSout

use ids_schemas
implicit none
!integer, parameter :: DP=kind(1.0D0)
character(len=3)::ual_debug

integer :: itime, lentime, lenstring, istring
integer :: i1,i2,i3,i4,i5,i6,i7

type(ids_equilibrium) :: IDSin, IDSout      

call getenv('ual_debug',ual_debug) ! Debug flag

      
! Copy ids_properties/comment
if (associated(IDSin%ids_properties%comment)) then
   allocate(IDSout%ids_properties%comment&
      (size(IDSin%ids_properties%comment,1)))
   IDSout%ids_properties%comment = &
   IDSin%ids_properties%comment
endif

! Copy ids_properties/homogeneous_time
if (IDSin%ids_properties%homogeneous_time/=-999999999)  then
   IDSout%ids_properties%homogeneous_time = &
   IDSin%ids_properties%homogeneous_time
endif   

! Copy ids_properties/cocos
if (IDSin%ids_properties%cocos/=-999999999)  then
   IDSout%ids_properties%cocos = &
   IDSin%ids_properties%cocos
endif   

! Copy vacuum_toroidal_field/r0
if (IDSin%vacuum_toroidal_field%r0.NE.-9.D40) then
   IDSout%vacuum_toroidal_field%r0 = &
   IDSin%vacuum_toroidal_field%r0
endif

! Copy vacuum_toroidal_field/b0
if (associated(IDSin%vacuum_toroidal_field%b0)) then
   allocate(IDSout%vacuum_toroidal_field%b0&
      (size(IDSin%vacuum_toroidal_field%b0,1)))
   IDSout%vacuum_toroidal_field%b0 = &
   IDSin%vacuum_toroidal_field%b0
endif

! Copy time_slice
if (associated(IDSin%time_slice)) then  
   allocate(IDSout%time_slice(size(IDSin%time_slice)))
   do i1 = 1,size(IDSin%time_slice)
      
! Copy time_slice/boundary/type
if (IDSin%time_slice(i1)%boundary%type/=-999999999)  then
   IDSout%time_slice(i1)%boundary%type = &
   IDSin%time_slice(i1)%boundary%type
endif   

! Copy time_slice/boundary/lcfs/r
if (associated(IDSin%time_slice(i1)%boundary%lcfs%r)) then
   allocate(IDSout%time_slice(i1)%boundary%lcfs%r&
      (size(IDSin%time_slice(i1)%boundary%lcfs%r,1)))
   IDSout%time_slice(i1)%boundary%lcfs%r = &
   IDSin%time_slice(i1)%boundary%lcfs%r
endif

! Copy time_slice/boundary/lcfs/z
if (associated(IDSin%time_slice(i1)%boundary%lcfs%z)) then
   allocate(IDSout%time_slice(i1)%boundary%lcfs%z&
      (size(IDSin%time_slice(i1)%boundary%lcfs%z,1)))
   IDSout%time_slice(i1)%boundary%lcfs%z = &
   IDSin%time_slice(i1)%boundary%lcfs%z
endif

! Copy time_slice/boundary/geometric_axis/r
if (IDSin%time_slice(i1)%boundary%geometric_axis%r.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%geometric_axis%r = &
   IDSin%time_slice(i1)%boundary%geometric_axis%r
endif

! Copy time_slice/boundary/geometric_axis/z
if (IDSin%time_slice(i1)%boundary%geometric_axis%z.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%geometric_axis%z = &
   IDSin%time_slice(i1)%boundary%geometric_axis%z
endif

! Copy time_slice/boundary/a_minor
if (IDSin%time_slice(i1)%boundary%a_minor.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%a_minor = &
   IDSin%time_slice(i1)%boundary%a_minor
endif

! Copy time_slice/boundary/elongation
if (IDSin%time_slice(i1)%boundary%elongation.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%elongation = &
   IDSin%time_slice(i1)%boundary%elongation
endif

! Copy time_slice/boundary/elongation_upper
if (IDSin%time_slice(i1)%boundary%elongation_upper.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%elongation_upper = &
   IDSin%time_slice(i1)%boundary%elongation_upper
endif

! Copy time_slice/boundary/elongation_lower
if (IDSin%time_slice(i1)%boundary%elongation_lower.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%elongation_lower = &
   IDSin%time_slice(i1)%boundary%elongation_lower
endif

! Copy time_slice/boundary/triangularity
if (IDSin%time_slice(i1)%boundary%triangularity.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%triangularity = &
   IDSin%time_slice(i1)%boundary%triangularity
endif

! Copy time_slice/boundary/triangularity_upper
if (IDSin%time_slice(i1)%boundary%triangularity_upper.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%triangularity_upper = &
   IDSin%time_slice(i1)%boundary%triangularity_upper
endif

! Copy time_slice/boundary/triangularity_lower
if (IDSin%time_slice(i1)%boundary%triangularity_lower.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%triangularity_lower = &
   IDSin%time_slice(i1)%boundary%triangularity_lower
endif

! Copy time_slice/boundary/x_point
if (associated(IDSin%time_slice(i1)%boundary%x_point)) then  
   allocate(IDSout%time_slice(i1)%boundary%x_point(size(IDSin%time_slice(i1)%boundary%x_point)))
   do i2 = 1,size(IDSin%time_slice(i1)%boundary%x_point)
      
! Copy time_slice/boundary/x_point/r
if (IDSin%time_slice(i1)%boundary%x_point(i2)%r.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%x_point(i2)%r = &
   IDSin%time_slice(i1)%boundary%x_point(i2)%r
endif

! Copy time_slice/boundary/x_point/z
if (IDSin%time_slice(i1)%boundary%x_point(i2)%z.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%x_point(i2)%z = &
   IDSin%time_slice(i1)%boundary%x_point(i2)%z
endif

   enddo
endif
         
! Copy time_slice/boundary/strike_point
if (associated(IDSin%time_slice(i1)%boundary%strike_point)) then  
   allocate(IDSout%time_slice(i1)%boundary%strike_point(size(IDSin%time_slice(i1)%boundary%strike_point)))
   do i2 = 1,size(IDSin%time_slice(i1)%boundary%strike_point)
      
! Copy time_slice/boundary/strike_point/r
if (IDSin%time_slice(i1)%boundary%strike_point(i2)%r.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%strike_point(i2)%r = &
   IDSin%time_slice(i1)%boundary%strike_point(i2)%r
endif

! Copy time_slice/boundary/strike_point/z
if (IDSin%time_slice(i1)%boundary%strike_point(i2)%z.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%strike_point(i2)%z = &
   IDSin%time_slice(i1)%boundary%strike_point(i2)%z
endif

   enddo
endif
         
! Copy time_slice/boundary/active_limiter_point/r
if (IDSin%time_slice(i1)%boundary%active_limiter_point%r.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%active_limiter_point%r = &
   IDSin%time_slice(i1)%boundary%active_limiter_point%r
endif

! Copy time_slice/boundary/active_limiter_point/z
if (IDSin%time_slice(i1)%boundary%active_limiter_point%z.NE.-9.D40) then
   IDSout%time_slice(i1)%boundary%active_limiter_point%z = &
   IDSin%time_slice(i1)%boundary%active_limiter_point%z
endif

! Copy time_slice/global_quantities/beta_pol
if (IDSin%time_slice(i1)%global_quantities%beta_pol.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%beta_pol = &
   IDSin%time_slice(i1)%global_quantities%beta_pol
endif

! Copy time_slice/global_quantities/beta_tor
if (IDSin%time_slice(i1)%global_quantities%beta_tor.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%beta_tor = &
   IDSin%time_slice(i1)%global_quantities%beta_tor
endif

! Copy time_slice/global_quantities/beta_normal
if (IDSin%time_slice(i1)%global_quantities%beta_normal.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%beta_normal = &
   IDSin%time_slice(i1)%global_quantities%beta_normal
endif

! Copy time_slice/global_quantities/ip
if (IDSin%time_slice(i1)%global_quantities%ip.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%ip = &
   IDSin%time_slice(i1)%global_quantities%ip
endif

! Copy time_slice/global_quantities/li_3
if (IDSin%time_slice(i1)%global_quantities%li_3.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%li_3 = &
   IDSin%time_slice(i1)%global_quantities%li_3
endif

! Copy time_slice/global_quantities/volume
if (IDSin%time_slice(i1)%global_quantities%volume.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%volume = &
   IDSin%time_slice(i1)%global_quantities%volume
endif

! Copy time_slice/global_quantities/area
if (IDSin%time_slice(i1)%global_quantities%area.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%area = &
   IDSin%time_slice(i1)%global_quantities%area
endif

! Copy time_slice/global_quantities/surface
if (IDSin%time_slice(i1)%global_quantities%surface.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%surface = &
   IDSin%time_slice(i1)%global_quantities%surface
endif

! Copy time_slice/global_quantities/length_pol
if (IDSin%time_slice(i1)%global_quantities%length_pol.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%length_pol = &
   IDSin%time_slice(i1)%global_quantities%length_pol
endif

! Copy time_slice/global_quantities/psi_axis
if (IDSin%time_slice(i1)%global_quantities%psi_axis.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%psi_axis = &
   IDSin%time_slice(i1)%global_quantities%psi_axis
endif

! Copy time_slice/global_quantities/psi_boundary
if (IDSin%time_slice(i1)%global_quantities%psi_boundary.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%psi_boundary = &
   IDSin%time_slice(i1)%global_quantities%psi_boundary
endif

! Copy time_slice/global_quantities/magnetic_axis/r
if (IDSin%time_slice(i1)%global_quantities%magnetic_axis%r.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%magnetic_axis%r = &
   IDSin%time_slice(i1)%global_quantities%magnetic_axis%r
endif

! Copy time_slice/global_quantities/magnetic_axis/z
if (IDSin%time_slice(i1)%global_quantities%magnetic_axis%z.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%magnetic_axis%z = &
   IDSin%time_slice(i1)%global_quantities%magnetic_axis%z
endif

! Copy time_slice/global_quantities/magnetic_axis/b_tor
if (IDSin%time_slice(i1)%global_quantities%magnetic_axis%b_tor.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%magnetic_axis%b_tor = &
   IDSin%time_slice(i1)%global_quantities%magnetic_axis%b_tor
endif

! Copy time_slice/global_quantities/q_axis
if (IDSin%time_slice(i1)%global_quantities%q_axis.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%q_axis = &
   IDSin%time_slice(i1)%global_quantities%q_axis
endif

! Copy time_slice/global_quantities/q_95
if (IDSin%time_slice(i1)%global_quantities%q_95.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%q_95 = &
   IDSin%time_slice(i1)%global_quantities%q_95
endif

! Copy time_slice/global_quantities/q_min/value
if (IDSin%time_slice(i1)%global_quantities%q_min%value.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%q_min%value = &
   IDSin%time_slice(i1)%global_quantities%q_min%value
endif

! Copy time_slice/global_quantities/q_min/rho_tor_norm
if (IDSin%time_slice(i1)%global_quantities%q_min%rho_tor_norm.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%q_min%rho_tor_norm = &
   IDSin%time_slice(i1)%global_quantities%q_min%rho_tor_norm
endif

! Copy time_slice/global_quantities/w_mhd
if (IDSin%time_slice(i1)%global_quantities%w_mhd.NE.-9.D40) then
   IDSout%time_slice(i1)%global_quantities%w_mhd = &
   IDSin%time_slice(i1)%global_quantities%w_mhd
endif

! Copy time_slice/profiles_1d/psi
if (associated(IDSin%time_slice(i1)%profiles_1d%psi)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%psi&
      (size(IDSin%time_slice(i1)%profiles_1d%psi,1)))
   IDSout%time_slice(i1)%profiles_1d%psi = &
   IDSin%time_slice(i1)%profiles_1d%psi
endif

! Copy time_slice/profiles_1d/phi
if (associated(IDSin%time_slice(i1)%profiles_1d%phi)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%phi&
      (size(IDSin%time_slice(i1)%profiles_1d%phi,1)))
   IDSout%time_slice(i1)%profiles_1d%phi = &
   IDSin%time_slice(i1)%profiles_1d%phi
endif

! Copy time_slice/profiles_1d/pressure
if (associated(IDSin%time_slice(i1)%profiles_1d%pressure)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%pressure&
      (size(IDSin%time_slice(i1)%profiles_1d%pressure,1)))
   IDSout%time_slice(i1)%profiles_1d%pressure = &
   IDSin%time_slice(i1)%profiles_1d%pressure
endif

! Copy time_slice/profiles_1d/f
if (associated(IDSin%time_slice(i1)%profiles_1d%f)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%f&
      (size(IDSin%time_slice(i1)%profiles_1d%f,1)))
   IDSout%time_slice(i1)%profiles_1d%f = &
   IDSin%time_slice(i1)%profiles_1d%f
endif

! Copy time_slice/profiles_1d/dpressure_dpsi
if (associated(IDSin%time_slice(i1)%profiles_1d%dpressure_dpsi)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%dpressure_dpsi&
      (size(IDSin%time_slice(i1)%profiles_1d%dpressure_dpsi,1)))
   IDSout%time_slice(i1)%profiles_1d%dpressure_dpsi = &
   IDSin%time_slice(i1)%profiles_1d%dpressure_dpsi
endif

! Copy time_slice/profiles_1d/f_df_dpsi
if (associated(IDSin%time_slice(i1)%profiles_1d%f_df_dpsi)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%f_df_dpsi&
      (size(IDSin%time_slice(i1)%profiles_1d%f_df_dpsi,1)))
   IDSout%time_slice(i1)%profiles_1d%f_df_dpsi = &
   IDSin%time_slice(i1)%profiles_1d%f_df_dpsi
endif

! Copy time_slice/profiles_1d/j_tor
if (associated(IDSin%time_slice(i1)%profiles_1d%j_tor)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%j_tor&
      (size(IDSin%time_slice(i1)%profiles_1d%j_tor,1)))
   IDSout%time_slice(i1)%profiles_1d%j_tor = &
   IDSin%time_slice(i1)%profiles_1d%j_tor
endif

! Copy time_slice/profiles_1d/j_parallel
if (associated(IDSin%time_slice(i1)%profiles_1d%j_parallel)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%j_parallel&
      (size(IDSin%time_slice(i1)%profiles_1d%j_parallel,1)))
   IDSout%time_slice(i1)%profiles_1d%j_parallel = &
   IDSin%time_slice(i1)%profiles_1d%j_parallel
endif

! Copy time_slice/profiles_1d/q
if (associated(IDSin%time_slice(i1)%profiles_1d%q)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%q&
      (size(IDSin%time_slice(i1)%profiles_1d%q,1)))
   IDSout%time_slice(i1)%profiles_1d%q = &
   IDSin%time_slice(i1)%profiles_1d%q
endif

! Copy time_slice/profiles_1d/magnetic_shear
if (associated(IDSin%time_slice(i1)%profiles_1d%magnetic_shear)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%magnetic_shear&
      (size(IDSin%time_slice(i1)%profiles_1d%magnetic_shear,1)))
   IDSout%time_slice(i1)%profiles_1d%magnetic_shear = &
   IDSin%time_slice(i1)%profiles_1d%magnetic_shear
endif

! Copy time_slice/profiles_1d/r_inboard
if (associated(IDSin%time_slice(i1)%profiles_1d%r_inboard)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%r_inboard&
      (size(IDSin%time_slice(i1)%profiles_1d%r_inboard,1)))
   IDSout%time_slice(i1)%profiles_1d%r_inboard = &
   IDSin%time_slice(i1)%profiles_1d%r_inboard
endif

! Copy time_slice/profiles_1d/r_outboard
if (associated(IDSin%time_slice(i1)%profiles_1d%r_outboard)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%r_outboard&
      (size(IDSin%time_slice(i1)%profiles_1d%r_outboard,1)))
   IDSout%time_slice(i1)%profiles_1d%r_outboard = &
   IDSin%time_slice(i1)%profiles_1d%r_outboard
endif

! Copy time_slice/profiles_1d/rho_tor
if (associated(IDSin%time_slice(i1)%profiles_1d%rho_tor)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%rho_tor&
      (size(IDSin%time_slice(i1)%profiles_1d%rho_tor,1)))
   IDSout%time_slice(i1)%profiles_1d%rho_tor = &
   IDSin%time_slice(i1)%profiles_1d%rho_tor
endif

! Copy time_slice/profiles_1d/rho_tor_norm
if (associated(IDSin%time_slice(i1)%profiles_1d%rho_tor_norm)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%rho_tor_norm&
      (size(IDSin%time_slice(i1)%profiles_1d%rho_tor_norm,1)))
   IDSout%time_slice(i1)%profiles_1d%rho_tor_norm = &
   IDSin%time_slice(i1)%profiles_1d%rho_tor_norm
endif

! Copy time_slice/profiles_1d/dpsi_drho_tor
if (associated(IDSin%time_slice(i1)%profiles_1d%dpsi_drho_tor)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%dpsi_drho_tor&
      (size(IDSin%time_slice(i1)%profiles_1d%dpsi_drho_tor,1)))
   IDSout%time_slice(i1)%profiles_1d%dpsi_drho_tor = &
   IDSin%time_slice(i1)%profiles_1d%dpsi_drho_tor
endif

! Copy time_slice/profiles_1d/elongation
if (associated(IDSin%time_slice(i1)%profiles_1d%elongation)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%elongation&
      (size(IDSin%time_slice(i1)%profiles_1d%elongation,1)))
   IDSout%time_slice(i1)%profiles_1d%elongation = &
   IDSin%time_slice(i1)%profiles_1d%elongation
endif

! Copy time_slice/profiles_1d/triangularity_upper
if (associated(IDSin%time_slice(i1)%profiles_1d%triangularity_upper)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%triangularity_upper&
      (size(IDSin%time_slice(i1)%profiles_1d%triangularity_upper,1)))
   IDSout%time_slice(i1)%profiles_1d%triangularity_upper = &
   IDSin%time_slice(i1)%profiles_1d%triangularity_upper
endif

! Copy time_slice/profiles_1d/triangularity_lower
if (associated(IDSin%time_slice(i1)%profiles_1d%triangularity_lower)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%triangularity_lower&
      (size(IDSin%time_slice(i1)%profiles_1d%triangularity_lower,1)))
   IDSout%time_slice(i1)%profiles_1d%triangularity_lower = &
   IDSin%time_slice(i1)%profiles_1d%triangularity_lower
endif

! Copy time_slice/profiles_1d/volume
if (associated(IDSin%time_slice(i1)%profiles_1d%volume)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%volume&
      (size(IDSin%time_slice(i1)%profiles_1d%volume,1)))
   IDSout%time_slice(i1)%profiles_1d%volume = &
   IDSin%time_slice(i1)%profiles_1d%volume
endif

! Copy time_slice/profiles_1d/dvolume_dpsi
if (associated(IDSin%time_slice(i1)%profiles_1d%dvolume_dpsi)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%dvolume_dpsi&
      (size(IDSin%time_slice(i1)%profiles_1d%dvolume_dpsi,1)))
   IDSout%time_slice(i1)%profiles_1d%dvolume_dpsi = &
   IDSin%time_slice(i1)%profiles_1d%dvolume_dpsi
endif

! Copy time_slice/profiles_1d/dvolume_drho_tor
if (associated(IDSin%time_slice(i1)%profiles_1d%dvolume_drho_tor)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%dvolume_drho_tor&
      (size(IDSin%time_slice(i1)%profiles_1d%dvolume_drho_tor,1)))
   IDSout%time_slice(i1)%profiles_1d%dvolume_drho_tor = &
   IDSin%time_slice(i1)%profiles_1d%dvolume_drho_tor
endif

! Copy time_slice/profiles_1d/area
if (associated(IDSin%time_slice(i1)%profiles_1d%area)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%area&
      (size(IDSin%time_slice(i1)%profiles_1d%area,1)))
   IDSout%time_slice(i1)%profiles_1d%area = &
   IDSin%time_slice(i1)%profiles_1d%area
endif

! Copy time_slice/profiles_1d/darea_dpsi
if (associated(IDSin%time_slice(i1)%profiles_1d%darea_dpsi)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%darea_dpsi&
      (size(IDSin%time_slice(i1)%profiles_1d%darea_dpsi,1)))
   IDSout%time_slice(i1)%profiles_1d%darea_dpsi = &
   IDSin%time_slice(i1)%profiles_1d%darea_dpsi
endif

! Copy time_slice/profiles_1d/surface
if (associated(IDSin%time_slice(i1)%profiles_1d%surface)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%surface&
      (size(IDSin%time_slice(i1)%profiles_1d%surface,1)))
   IDSout%time_slice(i1)%profiles_1d%surface = &
   IDSin%time_slice(i1)%profiles_1d%surface
endif

! Copy time_slice/profiles_1d/trapped_fraction
if (associated(IDSin%time_slice(i1)%profiles_1d%trapped_fraction)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%trapped_fraction&
      (size(IDSin%time_slice(i1)%profiles_1d%trapped_fraction,1)))
   IDSout%time_slice(i1)%profiles_1d%trapped_fraction = &
   IDSin%time_slice(i1)%profiles_1d%trapped_fraction
endif

! Copy time_slice/profiles_1d/gm1
if (associated(IDSin%time_slice(i1)%profiles_1d%gm1)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%gm1&
      (size(IDSin%time_slice(i1)%profiles_1d%gm1,1)))
   IDSout%time_slice(i1)%profiles_1d%gm1 = &
   IDSin%time_slice(i1)%profiles_1d%gm1
endif

! Copy time_slice/profiles_1d/gm2
if (associated(IDSin%time_slice(i1)%profiles_1d%gm2)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%gm2&
      (size(IDSin%time_slice(i1)%profiles_1d%gm2,1)))
   IDSout%time_slice(i1)%profiles_1d%gm2 = &
   IDSin%time_slice(i1)%profiles_1d%gm2
endif

! Copy time_slice/profiles_1d/gm3
if (associated(IDSin%time_slice(i1)%profiles_1d%gm3)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%gm3&
      (size(IDSin%time_slice(i1)%profiles_1d%gm3,1)))
   IDSout%time_slice(i1)%profiles_1d%gm3 = &
   IDSin%time_slice(i1)%profiles_1d%gm3
endif

! Copy time_slice/profiles_1d/gm4
if (associated(IDSin%time_slice(i1)%profiles_1d%gm4)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%gm4&
      (size(IDSin%time_slice(i1)%profiles_1d%gm4,1)))
   IDSout%time_slice(i1)%profiles_1d%gm4 = &
   IDSin%time_slice(i1)%profiles_1d%gm4
endif

! Copy time_slice/profiles_1d/gm5
if (associated(IDSin%time_slice(i1)%profiles_1d%gm5)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%gm5&
      (size(IDSin%time_slice(i1)%profiles_1d%gm5,1)))
   IDSout%time_slice(i1)%profiles_1d%gm5 = &
   IDSin%time_slice(i1)%profiles_1d%gm5
endif

! Copy time_slice/profiles_1d/gm6
if (associated(IDSin%time_slice(i1)%profiles_1d%gm6)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%gm6&
      (size(IDSin%time_slice(i1)%profiles_1d%gm6,1)))
   IDSout%time_slice(i1)%profiles_1d%gm6 = &
   IDSin%time_slice(i1)%profiles_1d%gm6
endif

! Copy time_slice/profiles_1d/gm7
if (associated(IDSin%time_slice(i1)%profiles_1d%gm7)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%gm7&
      (size(IDSin%time_slice(i1)%profiles_1d%gm7,1)))
   IDSout%time_slice(i1)%profiles_1d%gm7 = &
   IDSin%time_slice(i1)%profiles_1d%gm7
endif

! Copy time_slice/profiles_1d/gm8
if (associated(IDSin%time_slice(i1)%profiles_1d%gm8)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%gm8&
      (size(IDSin%time_slice(i1)%profiles_1d%gm8,1)))
   IDSout%time_slice(i1)%profiles_1d%gm8 = &
   IDSin%time_slice(i1)%profiles_1d%gm8
endif

! Copy time_slice/profiles_1d/gm9
if (associated(IDSin%time_slice(i1)%profiles_1d%gm9)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%gm9&
      (size(IDSin%time_slice(i1)%profiles_1d%gm9,1)))
   IDSout%time_slice(i1)%profiles_1d%gm9 = &
   IDSin%time_slice(i1)%profiles_1d%gm9
endif

! Copy time_slice/profiles_1d/b_average
if (associated(IDSin%time_slice(i1)%profiles_1d%b_average)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%b_average&
      (size(IDSin%time_slice(i1)%profiles_1d%b_average,1)))
   IDSout%time_slice(i1)%profiles_1d%b_average = &
   IDSin%time_slice(i1)%profiles_1d%b_average
endif

! Copy time_slice/profiles_1d/b_min
if (associated(IDSin%time_slice(i1)%profiles_1d%b_min)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%b_min&
      (size(IDSin%time_slice(i1)%profiles_1d%b_min,1)))
   IDSout%time_slice(i1)%profiles_1d%b_min = &
   IDSin%time_slice(i1)%profiles_1d%b_min
endif

! Copy time_slice/profiles_1d/b_max
if (associated(IDSin%time_slice(i1)%profiles_1d%b_max)) then
   allocate(IDSout%time_slice(i1)%profiles_1d%b_max&
      (size(IDSin%time_slice(i1)%profiles_1d%b_max,1)))
   IDSout%time_slice(i1)%profiles_1d%b_max = &
   IDSin%time_slice(i1)%profiles_1d%b_max
endif

! Copy time_slice/profiles_2d
if (associated(IDSin%time_slice(i1)%profiles_2d)) then  
   allocate(IDSout%time_slice(i1)%profiles_2d(size(IDSin%time_slice(i1)%profiles_2d)))
   do i2 = 1,size(IDSin%time_slice(i1)%profiles_2d)
      
! Copy time_slice/profiles_2d/grid_type/name
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%grid_type%name)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%grid_type%name&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%grid_type%name,1)))
   IDSout%time_slice(i1)%profiles_2d(i2)%grid_type%name = &
   IDSin%time_slice(i1)%profiles_2d(i2)%grid_type%name
endif

! Copy time_slice/profiles_2d/grid_type/index
if (IDSin%time_slice(i1)%profiles_2d(i2)%grid_type%index/=-999999999)  then
   IDSout%time_slice(i1)%profiles_2d(i2)%grid_type%index = &
   IDSin%time_slice(i1)%profiles_2d(i2)%grid_type%index
endif   

! Copy time_slice/profiles_2d/grid_type/description
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%grid_type%description)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%grid_type%description&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%grid_type%description,1)))
   IDSout%time_slice(i1)%profiles_2d(i2)%grid_type%description = &
   IDSin%time_slice(i1)%profiles_2d(i2)%grid_type%description
endif

! Copy time_slice/profiles_2d/grid/dim1
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%grid%dim1)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%grid%dim1&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%grid%dim1,1)))
   IDSout%time_slice(i1)%profiles_2d(i2)%grid%dim1 = &
   IDSin%time_slice(i1)%profiles_2d(i2)%grid%dim1
endif

! Copy time_slice/profiles_2d/grid/dim2
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%grid%dim2)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%grid%dim2&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%grid%dim2,1)))
   IDSout%time_slice(i1)%profiles_2d(i2)%grid%dim2 = &
   IDSin%time_slice(i1)%profiles_2d(i2)%grid%dim2
endif

! Copy time_slice/profiles_2d/r
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%r)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%r&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%r,1), &
      size(IDSin%time_slice(i1)%profiles_2d(i2)%r,2)))
   IDSout%time_slice(i1)%profiles_2d(i2)%r = &
   IDSin%time_slice(i1)%profiles_2d(i2)%r
endif

! Copy time_slice/profiles_2d/z
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%z)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%z&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%z,1), &
      size(IDSin%time_slice(i1)%profiles_2d(i2)%z,2)))
   IDSout%time_slice(i1)%profiles_2d(i2)%z = &
   IDSin%time_slice(i1)%profiles_2d(i2)%z
endif

! Copy time_slice/profiles_2d/psi
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%psi)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%psi&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%psi,1), &
      size(IDSin%time_slice(i1)%profiles_2d(i2)%psi,2)))
   IDSout%time_slice(i1)%profiles_2d(i2)%psi = &
   IDSin%time_slice(i1)%profiles_2d(i2)%psi
endif

! Copy time_slice/profiles_2d/theta
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%theta)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%theta&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%theta,1), &
      size(IDSin%time_slice(i1)%profiles_2d(i2)%theta,2)))
   IDSout%time_slice(i1)%profiles_2d(i2)%theta = &
   IDSin%time_slice(i1)%profiles_2d(i2)%theta
endif

! Copy time_slice/profiles_2d/phi
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%phi)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%phi&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%phi,1), &
      size(IDSin%time_slice(i1)%profiles_2d(i2)%phi,2)))
   IDSout%time_slice(i1)%profiles_2d(i2)%phi = &
   IDSin%time_slice(i1)%profiles_2d(i2)%phi
endif

! Copy time_slice/profiles_2d/j_tor
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%j_tor)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%j_tor&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%j_tor,1), &
      size(IDSin%time_slice(i1)%profiles_2d(i2)%j_tor,2)))
   IDSout%time_slice(i1)%profiles_2d(i2)%j_tor = &
   IDSin%time_slice(i1)%profiles_2d(i2)%j_tor
endif

! Copy time_slice/profiles_2d/j_parallel
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%j_parallel)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%j_parallel&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%j_parallel,1), &
      size(IDSin%time_slice(i1)%profiles_2d(i2)%j_parallel,2)))
   IDSout%time_slice(i1)%profiles_2d(i2)%j_parallel = &
   IDSin%time_slice(i1)%profiles_2d(i2)%j_parallel
endif

! Copy time_slice/profiles_2d/b_r
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%b_r)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%b_r&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%b_r,1), &
      size(IDSin%time_slice(i1)%profiles_2d(i2)%b_r,2)))
   IDSout%time_slice(i1)%profiles_2d(i2)%b_r = &
   IDSin%time_slice(i1)%profiles_2d(i2)%b_r
endif

! Copy time_slice/profiles_2d/b_z
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%b_z)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%b_z&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%b_z,1), &
      size(IDSin%time_slice(i1)%profiles_2d(i2)%b_z,2)))
   IDSout%time_slice(i1)%profiles_2d(i2)%b_z = &
   IDSin%time_slice(i1)%profiles_2d(i2)%b_z
endif

! Copy time_slice/profiles_2d/b_tor
if (associated(IDSin%time_slice(i1)%profiles_2d(i2)%b_tor)) then
   allocate(IDSout%time_slice(i1)%profiles_2d(i2)%b_tor&
      (size(IDSin%time_slice(i1)%profiles_2d(i2)%b_tor,1), &
      size(IDSin%time_slice(i1)%profiles_2d(i2)%b_tor,2)))
   IDSout%time_slice(i1)%profiles_2d(i2)%b_tor = &
   IDSin%time_slice(i1)%profiles_2d(i2)%b_tor
endif

   enddo
endif
         
! Copy time_slice/coordinate_system/grid_type/name
if (associated(IDSin%time_slice(i1)%coordinate_system%grid_type%name)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%grid_type%name&
      (size(IDSin%time_slice(i1)%coordinate_system%grid_type%name,1)))
   IDSout%time_slice(i1)%coordinate_system%grid_type%name = &
   IDSin%time_slice(i1)%coordinate_system%grid_type%name
endif

! Copy time_slice/coordinate_system/grid_type/index
if (IDSin%time_slice(i1)%coordinate_system%grid_type%index/=-999999999)  then
   IDSout%time_slice(i1)%coordinate_system%grid_type%index = &
   IDSin%time_slice(i1)%coordinate_system%grid_type%index
endif   

! Copy time_slice/coordinate_system/grid_type/description
if (associated(IDSin%time_slice(i1)%coordinate_system%grid_type%description)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%grid_type%description&
      (size(IDSin%time_slice(i1)%coordinate_system%grid_type%description,1)))
   IDSout%time_slice(i1)%coordinate_system%grid_type%description = &
   IDSin%time_slice(i1)%coordinate_system%grid_type%description
endif

! Copy time_slice/coordinate_system/grid/dim1
if (associated(IDSin%time_slice(i1)%coordinate_system%grid%dim1)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%grid%dim1&
      (size(IDSin%time_slice(i1)%coordinate_system%grid%dim1,1)))
   IDSout%time_slice(i1)%coordinate_system%grid%dim1 = &
   IDSin%time_slice(i1)%coordinate_system%grid%dim1
endif

! Copy time_slice/coordinate_system/grid/dim2
if (associated(IDSin%time_slice(i1)%coordinate_system%grid%dim2)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%grid%dim2&
      (size(IDSin%time_slice(i1)%coordinate_system%grid%dim2,1)))
   IDSout%time_slice(i1)%coordinate_system%grid%dim2 = &
   IDSin%time_slice(i1)%coordinate_system%grid%dim2
endif

! Copy time_slice/coordinate_system/r
if (associated(IDSin%time_slice(i1)%coordinate_system%r)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%r&
      (size(IDSin%time_slice(i1)%coordinate_system%r,1), &
      size(IDSin%time_slice(i1)%coordinate_system%r,2)))
   IDSout%time_slice(i1)%coordinate_system%r = &
   IDSin%time_slice(i1)%coordinate_system%r
endif

! Copy time_slice/coordinate_system/z
if (associated(IDSin%time_slice(i1)%coordinate_system%z)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%z&
      (size(IDSin%time_slice(i1)%coordinate_system%z,1), &
      size(IDSin%time_slice(i1)%coordinate_system%z,2)))
   IDSout%time_slice(i1)%coordinate_system%z = &
   IDSin%time_slice(i1)%coordinate_system%z
endif

! Copy time_slice/coordinate_system/jacobian
if (associated(IDSin%time_slice(i1)%coordinate_system%jacobian)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%jacobian&
      (size(IDSin%time_slice(i1)%coordinate_system%jacobian,1), &
      size(IDSin%time_slice(i1)%coordinate_system%jacobian,2)))
   IDSout%time_slice(i1)%coordinate_system%jacobian = &
   IDSin%time_slice(i1)%coordinate_system%jacobian
endif

! Copy time_slice/coordinate_system/g_11
if (associated(IDSin%time_slice(i1)%coordinate_system%g_11)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%g_11&
      (size(IDSin%time_slice(i1)%coordinate_system%g_11,1), &
      size(IDSin%time_slice(i1)%coordinate_system%g_11,2)))
   IDSout%time_slice(i1)%coordinate_system%g_11 = &
   IDSin%time_slice(i1)%coordinate_system%g_11
endif

! Copy time_slice/coordinate_system/g_12
if (associated(IDSin%time_slice(i1)%coordinate_system%g_12)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%g_12&
      (size(IDSin%time_slice(i1)%coordinate_system%g_12,1), &
      size(IDSin%time_slice(i1)%coordinate_system%g_12,2)))
   IDSout%time_slice(i1)%coordinate_system%g_12 = &
   IDSin%time_slice(i1)%coordinate_system%g_12
endif

! Copy time_slice/coordinate_system/g_13
if (associated(IDSin%time_slice(i1)%coordinate_system%g_13)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%g_13&
      (size(IDSin%time_slice(i1)%coordinate_system%g_13,1), &
      size(IDSin%time_slice(i1)%coordinate_system%g_13,2)))
   IDSout%time_slice(i1)%coordinate_system%g_13 = &
   IDSin%time_slice(i1)%coordinate_system%g_13
endif

! Copy time_slice/coordinate_system/g_22
if (associated(IDSin%time_slice(i1)%coordinate_system%g_22)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%g_22&
      (size(IDSin%time_slice(i1)%coordinate_system%g_22,1), &
      size(IDSin%time_slice(i1)%coordinate_system%g_22,2)))
   IDSout%time_slice(i1)%coordinate_system%g_22 = &
   IDSin%time_slice(i1)%coordinate_system%g_22
endif

! Copy time_slice/coordinate_system/g_23
if (associated(IDSin%time_slice(i1)%coordinate_system%g_23)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%g_23&
      (size(IDSin%time_slice(i1)%coordinate_system%g_23,1), &
      size(IDSin%time_slice(i1)%coordinate_system%g_23,2)))
   IDSout%time_slice(i1)%coordinate_system%g_23 = &
   IDSin%time_slice(i1)%coordinate_system%g_23
endif

! Copy time_slice/coordinate_system/g_33
if (associated(IDSin%time_slice(i1)%coordinate_system%g_33)) then
   allocate(IDSout%time_slice(i1)%coordinate_system%g_33&
      (size(IDSin%time_slice(i1)%coordinate_system%g_33,1), &
      size(IDSin%time_slice(i1)%coordinate_system%g_33,2)))
   IDSout%time_slice(i1)%coordinate_system%g_33 = &
   IDSin%time_slice(i1)%coordinate_system%g_33
endif

! Copy time_slice/time
if (IDSin%time_slice(i1)%time.NE.-9.D40) then
   IDSout%time_slice(i1)%time = &
   IDSin%time_slice(i1)%time
endif

   enddo
endif
         
! Copy code/name
if (associated(IDSin%code%name)) then
   allocate(IDSout%code%name&
      (size(IDSin%code%name,1)))
   IDSout%code%name = &
   IDSin%code%name
endif

! Copy code/version
if (associated(IDSin%code%version)) then
   allocate(IDSout%code%version&
      (size(IDSin%code%version,1)))
   IDSout%code%version = &
   IDSin%code%version
endif

! Copy code/parameters
if (associated(IDSin%code%parameters)) then
   allocate(IDSout%code%parameters&
      (size(IDSin%code%parameters,1)))
   IDSout%code%parameters = &
   IDSin%code%parameters
endif

! Copy code/output_flag
if (associated(IDSin%code%output_flag)) then
   allocate(IDSout%code%output_flag&
      (size(IDSin%code%output_flag,1)))
   IDSout%code%output_flag = &
   IDSin%code%output_flag
endif

! Copy time
if (associated(IDSin%time)) then
   allocate(IDSout%time&
      (size(IDSin%time,1)))
   IDSout%time = &
   IDSin%time
endif


return
end subroutine ids_copy_equilibrium

end module equilibrium_ids_module
